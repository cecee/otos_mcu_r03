/*
 * dbg.h
 *
 */ 


#ifndef DBG_H_
#define DBG_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

// ASCII code definition
#define ASCII_BS                0x08    // Back space
#define ASCII_LF                0x0A    // Line feed
#define	ASCII_FF                0x0C    // Form feed
#define ASCII_CR                0x0D    // Carriage return
#define	ASCII_DEL               0x10    // delete
#define	ASCII_ESC               0x1B    // escape
#define	ASCII_CTRL_X            0x18    // control + x
#define ASCII_SP                0x20    // Space
// ANSI code definition
#define	ANSI_INIT			    "\033[0m\0"
#define	ANSI_BLINK			    "\033[5m\0"
#define ANSI_BLOCK			    "\033[1;30m\0"
#define	ANSI_RED			    "\033[1;31m\0"
#define	ANSI_GREEN			    "\033[1;32m\0"
#define	ANSI_YELLOW			    "\033[1;33m\0"
#define	ANSI_BLUE			    "\033[1;34m\0"
#define	ANSI_PURPLE			    "\033[1;35m\0"
#define	ANSI_CYAN			    "\033[1;36m\0"
#define	ANSI_WHITE			    "\033[1;37m\0"
#define	ANSI_DEFAULT		    "\033[1;39m\0"
#define	ANSI_B_BLOCK		    "\033[1;40m\0"

#define SET_ANSI_BLACK_RED            "\033[1;40;31m"
#define SET_ANSI_BLACK_GREEN          "\033[1;40;32m"
#define SET_ANSI_BLACK_YELLOW         "\033[1;40;33m"
#define SET_ANSI_BLACK_BLUE           "\033[1;40;34m"
#define SET_ANSI_BLACK_PINK           "\033[1;40;35m"
#define SET_ANSI_BLACK_BLUEGREEN      "\033[1;40;36m"
#define SET_ANSI_BLACK_WHITE          "\033[1;40;37m"

/****************************************************************************
*                                Functions
*****************************************************************************/

int DBG_Init(void);
void DBG(int nType, const char * format, ... );

#endif /* DBG_H_ */
