/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define DIP0_Pin GPIO_PIN_0
#define DIP0_GPIO_Port GPIOC
#define DIP1_Pin GPIO_PIN_1
#define DIP1_GPIO_Port GPIOC
#define DIP2_Pin GPIO_PIN_2
#define DIP2_GPIO_Port GPIOC
#define DIP3_Pin GPIO_PIN_3
#define DIP3_GPIO_Port GPIOC
#define SOL_D1p_Pin GPIO_PIN_7
#define SOL_D1p_GPIO_Port GPIOE
#define SOL_D1m_Pin GPIO_PIN_8
#define SOL_D1m_GPIO_Port GPIOE
#define SOL_D2p_Pin GPIO_PIN_9
#define SOL_D2p_GPIO_Port GPIOE
#define SOL_D2m_Pin GPIO_PIN_10
#define SOL_D2m_GPIO_Port GPIOE
#define SOL_B1p_Pin GPIO_PIN_11
#define SOL_B1p_GPIO_Port GPIOE
#define SOL_B1m_Pin GPIO_PIN_12
#define SOL_B1m_GPIO_Port GPIOE
#define SOL_B2p_Pin GPIO_PIN_13
#define SOL_B2p_GPIO_Port GPIOE
#define SOL_B2m_Pin GPIO_PIN_14
#define SOL_B2m_GPIO_Port GPIOE
#define CS_FND_Pin GPIO_PIN_12
#define CS_FND_GPIO_Port GPIOB
#define SD1_DET0_Pin GPIO_PIN_8
#define SD1_DET0_GPIO_Port GPIOD
#define SD1_DET1_Pin GPIO_PIN_9
#define SD1_DET1_GPIO_Port GPIOD
#define SD2_DET0_Pin GPIO_PIN_10
#define SD2_DET0_GPIO_Port GPIOD
#define SD2_DET1_Pin GPIO_PIN_11
#define SD2_DET1_GPIO_Port GPIOD
#define SB1_DET0_Pin GPIO_PIN_12
#define SB1_DET0_GPIO_Port GPIOD
#define SB1_DET1_Pin GPIO_PIN_13
#define SB1_DET1_GPIO_Port GPIOD
#define SB2_DET0_Pin GPIO_PIN_14
#define SB2_DET0_GPIO_Port GPIOD
#define SB2_DET1_Pin GPIO_PIN_15
#define SB2_DET1_GPIO_Port GPIOD
#define nHEATER_Pin GPIO_PIN_6
#define nHEATER_GPIO_Port GPIOC
#define nBMS_EN_Pin GPIO_PIN_7
#define nBMS_EN_GPIO_Port GPIOC
#define TXEN_Pin GPIO_PIN_8
#define TXEN_GPIO_Port GPIOA
#define NFC_TXD_Pin GPIO_PIN_12
#define NFC_TXD_GPIO_Port GPIOC
#define NFC_NRST_Pin GPIO_PIN_0
#define NFC_NRST_GPIO_Port GPIOD
#define NFC_RXD_Pin GPIO_PIN_2
#define NFC_RXD_GPIO_Port GPIOD
#define RESV_DOUT_Pin GPIO_PIN_4
#define RESV_DOUT_GPIO_Port GPIOD
#define RESV_DIN_Pin GPIO_PIN_5
#define RESV_DIN_GPIO_Port GPIOD
#define RL_CONT_Pin GPIO_PIN_6
#define RL_CONT_GPIO_Port GPIOD
#define FAN_CONT_Pin GPIO_PIN_7
#define FAN_CONT_GPIO_Port GPIOD
#define CS_FLASH_Pin GPIO_PIN_6
#define CS_FLASH_GPIO_Port GPIOB
#define FAN_FAIL_Pin GPIO_PIN_8
#define FAN_FAIL_GPIO_Port GPIOB
#define DC_SD_Pin GPIO_PIN_9
#define DC_SD_GPIO_Port GPIOB
#define AC_FAIL_Pin GPIO_PIN_0
#define AC_FAIL_GPIO_Port GPIOE
#define TEMP_FAIL_Pin GPIO_PIN_1
#define TEMP_FAIL_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
