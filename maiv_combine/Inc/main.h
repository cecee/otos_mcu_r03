/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define pLED0_Pin GPIO_PIN_0
#define pLED0_GPIO_Port GPIOC
#define pLED1_Pin GPIO_PIN_1
#define pLED1_GPIO_Port GPIOC
#define pLED2_Pin GPIO_PIN_2
#define pLED2_GPIO_Port GPIOC
#define pLED3_Pin GPIO_PIN_3
#define pLED3_GPIO_Port GPIOC
#define pPX_RESET_Pin GPIO_PIN_0
#define pPX_RESET_GPIO_Port GPIOB
#define pSEL_A0_Pin GPIO_PIN_1
#define pSEL_A0_GPIO_Port GPIOB
#define pSEL_A1_Pin GPIO_PIN_2
#define pSEL_A1_GPIO_Port GPIOB
#define pRELAY1_Pin GPIO_PIN_10
#define pRELAY1_GPIO_Port GPIOB
#define pRELAY2_Pin GPIO_PIN_11
#define pRELAY2_GPIO_Port GPIOB
#define pBAT_EN0_Pin GPIO_PIN_12
#define pBAT_EN0_GPIO_Port GPIOB
#define pBAT_EN1_Pin GPIO_PIN_13
#define pBAT_EN1_GPIO_Port GPIOB
#define pBAT_EN2_Pin GPIO_PIN_14
#define pBAT_EN2_GPIO_Port GPIOB
#define pBAT_EN3_Pin GPIO_PIN_15
#define pBAT_EN3_GPIO_Port GPIOB
#define pISBAT0_Pin GPIO_PIN_6
#define pISBAT0_GPIO_Port GPIOC
#define pISBAT1_Pin GPIO_PIN_7
#define pISBAT1_GPIO_Port GPIOC
#define pISBAT2_Pin GPIO_PIN_8
#define pISBAT2_GPIO_Port GPIOC
#define pISBAT3_Pin GPIO_PIN_9
#define pISBAT3_GPIO_Port GPIOC
#define TXEN_Pin GPIO_PIN_8
#define TXEN_GPIO_Port GPIOA
#define pSET_0_Pin GPIO_PIN_3
#define pSET_0_GPIO_Port GPIOB
#define pRESET_0_Pin GPIO_PIN_4
#define pRESET_0_GPIO_Port GPIOB
#define pSET_1_Pin GPIO_PIN_5
#define pSET_1_GPIO_Port GPIOB
#define pRESET_1_Pin GPIO_PIN_6
#define pRESET_1_GPIO_Port GPIOB
#define pAUX_A0_Pin GPIO_PIN_7
#define pAUX_A0_GPIO_Port GPIOB
#define pAUX_A1_Pin GPIO_PIN_8
#define pAUX_A1_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
