#ifndef HAL_FLASH_H_
#define HAL_FLASH_H_

typedef struct
{
    unsigned char a;
    unsigned char b;
    unsigned char c;

} TEST_STRUCT;

void flash_test_read();
void flash_test_write();

#endif