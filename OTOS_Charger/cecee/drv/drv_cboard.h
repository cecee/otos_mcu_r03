#ifndef DRV_CONSOLE_H_
#define DRV_CONSOLE_H_



////////////////////////////////////////
///  BATT EXIST
//   DISABLE 0.47V 이하
//   ENABLE  0.93V~1.24V
//   No Batt 1.55V 
//   1.40이상이 나오면 no bms로 정의
///////////////////////////////////////

#define NO_BATT_VOLTAGE 1500


typedef struct BIT_BUCKET
{
	uint8_t door: 1;
	uint8_t batt_exist: 1;
	uint8_t comm_type: 1;

}BIT_BUCKET;

typedef struct _CBOARD
{
  uint8_t id;
  uint32_t my_snd_id;
  uint32_t my_recv_id;
}CBOARD;


void CB_Init(void);
void CB_GetID(void);
void CB_BattCheck(void);
void CB_CommSelect(uint8_t select);
void CB_ADC_DAC_init(void);

//void CB_ChargeProcess(void);
void CB_AdcProcess(void);

void CB_ChargingStart(uint8_t *data);
//void CB_ChargingStop(void);

void Drv_console_TXprocess(void);
void Drv_console_RXprocess(uint8_t *buffer);

void CB_MotorTest(void);
void function_motor_test(void);
#endif