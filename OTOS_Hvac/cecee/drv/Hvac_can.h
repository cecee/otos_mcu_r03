#ifndef HVAC_CAN_H_
#define HVAC_CAN_H_

#define CAN_SND_ID  0x1CEC1000 //0x1CECxxyy xx:id yy:seq
#define CAN_RCV_ID  0x0CECEF10 //0x0CECEFxx xx:id

void Drv_CanTx(uint32_t can_id, uint8_t *data);  
void Drv_CanRx(void);
//void Drv_SetAlarmTemp(const uint8_t id, const uint8_t *data);
#endif