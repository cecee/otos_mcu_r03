/*
 * cbuffer.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "cbuffer.h"
#include "d_cecee.h"
#include <stdio.h>

/****************************************************************************
 *                                Defines
*****************************************************************************/

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/
QUEUE qUart1;
QUEUE qUart2;
QUEUE qUart3;
QUEUE qUart4;
QUEUE qUart5;

uint8_t qUart1_package=0;
uint8_t qUart1_buffer[128];
uint8_t qUart1_cnt=0;
uint8_t qUart1_sz=0xff;


uint8_t qUart2_package=0;
uint8_t qUart2_buffer[128];
uint8_t qUart2_cnt=0;
uint8_t qUart2_sz=0xff;

uint8_t qUart5_package=0;
uint8_t qUart5_buffer[128];
uint8_t qUart5_cnt=0;
uint8_t qUart5_sz=0xff;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

/////////////////////////////////////////////
///Queue
////////////////////////////////////////////

void init_qUart1(void){
  qUart1.front=0;
  qUart1.rear=0;
}

void init_qUart2(void){
  qUart2.front=0;
  qUart2.rear=0;
}

void init_qUart3(void){
  qUart3.front=0;
  qUart3.rear=0;
}

//void init_qUart4(void){
//  qUart4.front=0;
//  qUart4.rear=0;
//}

void init_qUart5(void){
  qUart5.front=0;
  qUart5.rear=0;
}

void init_queue(void){
  printf("##init_queue.........\r\n");
  qUart1.front=0;
  qUart1.rear=0;
  qUart2.front=0;
  qUart2.rear=0;
  qUart3.front=0;
  qUart3.rear=0;
  qUart4.front=0;
  qUart4.rear=0;
  qUart5.front=0;
  qUart5.rear=0;
}
  
int put(QUEUE q, int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((q.rear + 1) % MAX_SIZE == q.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",q.rear, q.front);
        return -1;
    }
    q.queue[ q.rear] = k;
    q.rear = (q.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", q.rear);
    return k;
}



int put_qUart1(int k){
    if ((qUart1.rear + 1) % MAX_SIZE == qUart1.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart1.rear, qUart1.front);
        return -1;
    }
    qUart1.queue[ qUart1.rear] = k;
    qUart1.rear = (qUart1.rear+1) % MAX_SIZE;
    return k;
}

int put_qUart2(int k){
   //printf("put_qUart2 [%x]\r\n",k);
    if ((qUart2.rear + 1) % MAX_SIZE == qUart2.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart2.rear, qUart2.front);
        return -1;
    }
    qUart2.queue[ qUart2.rear] = k;
    qUart2.rear = (qUart2.rear+1) % MAX_SIZE;
    return k;
}


int put_qUart3(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart3.rear + 1) % MAX_SIZE == qUart3.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart3.rear, qUart3.front);
        return -1;
    }

    qUart3.queue[ qUart3.rear] = k;
    qUart3.rear = (qUart3.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart4.rear);
    return k;
}

int put_qUart4(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart4.rear + 1) % MAX_SIZE == qUart4.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart4.rear, qUart4.front);
        return -1;
    }

    qUart4.queue[ qUart4.rear] = k;
    qUart4.rear = (qUart4.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart4.rear);
    return k;
}


int put_qUart5(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart5.rear + 1) % MAX_SIZE == qUart5.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart5.rear, qUart5.front);
        return -1;
    }

    qUart5.queue[ qUart5.rear] = k;
    qUart5.rear = (qUart5.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart5.rear);
    return k;
}

int get_qUart1(void)
{
    int i=0;
     if (qUart1.front == qUart1.rear){
        return -1;
    }
    i = qUart1.queue[qUart1.front];
   qUart1.front = (qUart1.front+1) % MAX_SIZE;
    return i;
}

int get_qUart2(void)
{
    int i=0;
     if (qUart2.front == qUart2.rear){
        return -1;
    }
    i = qUart2.queue[qUart2.front];
   qUart2.front = (qUart2.front+1) % MAX_SIZE;
    return i;
}

int get_qUart3(void)
{
    int i=0;
     if (qUart3.front == qUart3.rear){
        return -1;
    }
    i = qUart3.queue[qUart3.front];
   qUart3.front = (qUart3.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}

int get_qUart4(void)
{
    int i=0;
     if (qUart4.front == qUart4.rear){
        return -1;
    }
    i = qUart4.queue[qUart4.front];
   qUart4.front = (qUart4.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}

int get_qUart5(void)
{
    int i=0;
     if (qUart5.front == qUart5.rear){
        return -1;
    }
    i = qUart5.queue[qUart5.front];
   qUart5.front = (qUart5.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}


int GetSerialBuf_console(void)
{
	int  d;
	int end=0;
//	for(i=0;i<10;i++)
	for(;;)
	{
		d =get_qUart4();
		if(d==-1 )break;
			
		switch(d){
			case 0x6D:
				Drv_dbd_view();
				break;
			case 0x71:
				Drv_dbd_cinfo(0);
				break;	
		
		}	
   // if(d==0x6D)   Drv_dbd_view();
  	//	printf("GetSerialBuf  [%02X]\r\n", d);
   
		if(end) break;	
	}
  return 1;
}

/*
* [0]=STX 0xAA
* [1]=Protocol 
* [2]=Command
* [3]=HI Length
* [4]=LO Length
* [5]=....Data0
* ...
*[n]=CRC
*[n+1]=ETX 0xFF
*/
int GetSerialBuf_nfc(void)
{
	int  d;
	for(;;)
	{
		d =get_qUart2();
		if(d==-1 )break;
          if((qUart2_package==0) && (d==0xAA)) 
          {
            qUart2_package=1;
            qUart2_cnt=0;
            qUart2_sz=0xff;
             memset(qUart2_buffer,0,sizeof(qUart2_buffer));
          }
 		if(qUart2_package){
			qUart2_buffer[qUart2_cnt++]=d;
			if(qUart2_cnt==5) qUart2_sz=qUart2_buffer[4]+7;
               if((d==0xFF) && (qUart2_sz==qUart2_cnt) )
               {
                  qUart2_package=0;
                  if(qUart2_buffer[2]==0x01)//Command for only ID Data
                  {
                      //printf("## ETX GetSerialBuf_nfc qUart2_cnt[%d] sz[%d]\r\n",qUart2_cnt, qUart2_sz);
                      //for(i=0;i<qUart2_sz;i++) printf("## qUart2_buffer[%d] [%x]\r\n",i, qUart2_buffer[i]);
                       Drv_MessagePublic( NFC, qUart2_buffer, qUart2_sz);
                       
                    }
              }
          }
    }         
    return 1;
}

int GetSerialBuf_rk(void)
{
	int  d;
	for(;;)
	{
		d =get_qUart5();
		if(d==-1 )break;
          d=d&0xff;
          if((qUart5_package==0) && (d==0xFA)) 
          {
             qUart5_package=1;
             qUart5_cnt=0;
             qUart5_sz=0xff;
             memset(qUart5_buffer,0,sizeof(qUart5_buffer));
          }
 		if(qUart5_package){
			qUart5_buffer[qUart5_cnt++]=d;
 			if(qUart5_cnt==5) qUart5_sz=qUart5_buffer[4];
               if((d==0xFB) && (qUart5_sz==qUart5_cnt))
              {
                  qUart5_package=0;
                  {
                      Drv_MessageParse(qUart5_buffer, qUart5_sz);
                  }
                
              }
          }
    }         
    return 1;
}



int GetSerialBuf_fromM(void)
{
	int  d;
	for(;;)
	{
		d =get_qUart1();
		if(d==-1 )break;
		if((qUart1_package==0) && (d==0xcc))
    {
      
      printf("GetSerialBuf_fromM Find_headerr\r\n");
			qUart1_package=1;
			qUart1_cnt=0;
      qUart1_sz=0xff;
			memset(qUart1_buffer,0,sizeof(qUart1_buffer));
			
		}	
		if(qUart1_package){
			qUart1_buffer[qUart1_cnt++]=d;
			if(qUart1_cnt==2) qUart1_sz=qUart1_buffer[1];
      if(qUart1_sz<qUart1_cnt)
      {
        qUart1_package=0;
       printf("GetSerialBuf_fromM qUart1_cnt[%d] sz[%d]\r\n",qUart1_cnt, qUart1_sz);
       
      	memcpy(&BdCtrl, &qUart1_buffer, qUart1_sz );
      //	for(i=0;i<10;i++)	printf("GetSerialBuf_fromM [%d/%d] [%02X]\r\n",i,sz, qUart1_buffer[i]);
      }
      //if(end) break;	
    }
  }
  return 1;
}