#ifndef CECEE_H_
#define CECEE_H_
/*
#include "time.h"
#include "string.h"
#include "cecee_drv.h"
#include "cecee_util.h"
#include "cecee_hal.h"
#include "iwdg.h"
#include "../drv/drv_cboard.h"
#include "../drv/drv_sps194.h"
#include "../drv/drv_bms.h"
#include "../drv/drv_solenoid.h"
#include "../drv/drv_can.h"
#include "../drv/call_back.h"
//#include "../util/cbuffer.h"
#include "../hal/hal_adc.h"

#include "../hal/hal_spi.h"
*/

#include "../drv/call_back.h"
#include "../drv/drv_pixxi.h"
#include "../drv/drv_maiv.h"
#include "../drv/drv_bms.h"
#include "../hal/flash_if.h"
#include "../hal/hal_uart.h"
#include "string.h"
//#include "../hal/hal_can.h"

#include <stdint.h>

/*
* Define : Boolean
*/
#define FALSE		0
#define TRUE		1

/*
* Define : Return Value(Ok and Error)
*/
#define RET_OK		(0)
#define RET_ERR	(-1)

/*
* Define : Set Value(On/Off)
*/
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)

/*
* Define : Debug Message Type
*/
#define ERR			1
#define WARN	          2
#define MSG			3
#define PRT			4
#define ATC			5
#define ATR			6

/*
* Define : Filename
*/
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

extern MAIV_FLAG maiv_flag; 
extern MAIV_BMS_SOC maiv_bms;
extern STATUS_PIXXI pixxi;

extern uint8_t CAN_RCV_FLAG;
//extern uint8_t _101Cnt;
//extern uint8_t _102Cnt;
// For CAN1
//extern uint8_t can1_rxbuf[8];
//extern uint8_t bmsSQ;
//extern BMS_101 bms0101;
//extern BMS_102 bms0102;
//extern TX_A0A0 tx_a0a0;
//extern TX_A0A1 tx_a0a1;

#endif
