#include "main.h"
#include "stm32f1xx_hal.h"
#include "drv_solenoid.h"
#include "cecee.h"

//uint8_t exSolPos[4]={0,};
uint16_t err_timeout=0;

MOTOR motor[4];
const uint8_t MOTOR_TYPE[4]={0,1,0,1};
const uint8_t PC_LOCK[4]  ={DS_LOCK,   BS_LOCK,   DS_LOCK,   BS_LOCK};
const uint8_t PC_UNLOCK[4]={DS_UNLOCK, BS_UNLOCK, DS_UNLOCK, BS_UNLOCK};

uint16_t  m_tick[4]={0,};
//uint8_t check = 0;

void Drv_solenoid_init(void)
{
  uint8_t i;  
  memset(&gCBUP.bLockStatus,0,sizeof(gCBUP.bLockStatus));
  memset(&motor, 0, sizeof(motor));

   for(i=0; i<4 ;i++) {
    motor[i].b.ctrl=0;
    motor[i].b.ready=1;
    Drv_solenoid_set(i,_UNLOCK);
  }
}


void Drv_solenoid_tick(void)
{
  uint8_t i;
  for(i=0;i<4;i++){
    if(m_tick[i]>0) m_tick[i]--;
  }
}         
         
void Drv_solenoid_get_pos(void)
{
  motor[0].b.pos =((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_9)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_8))&0x03;
  motor[1].b.pos =((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_11)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_10))&0x03;
  motor[2].b.pos =((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_13)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_12))&0x03;
  motor[3].b.pos =((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_15)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14))&0x03;
  gCBUP.bLockStatus.D1_pos=motor[0].b.pos;
  gCBUP.bLockStatus.B1_pos=motor[1].b.pos;
  gCBUP.bLockStatus.D2_pos=motor[2].b.pos;
  gCBUP.bLockStatus.B2_pos=motor[3].b.pos;
}




void D1_ErrAction(){
	static uint8_t Try=0;
	static uint8_t rotateFlag=0;
	static uint8_t rotateTimeOut=0;
  
	uint8_t lock_pos = motor[0].b.pos;
	uint8_t release_tick = m_tick[0];
	uint8_t state_cmd = gCBUP.bLockStatus.D1_ctrl;
	
	if ( state_cmd &&  (lock_pos==DS_UNLOCK) &&  release_tick==0) {
		Try++;
		//printf("#### D1_ctrl  DS_LOCK ERROR!!  d1_pos [%d] [%d]\r\n", lock_pos, Try);
	}	
	 if(rotateFlag) rotateTimeOut++;
	 if((Try >= 50) && state_cmd ) {
			rotateFlag =1;
			err_timeout=0;
			rotateTimeOut = 0;
			Try=0;
			Drv_solenoid_rotate(0,  0);	   

	 }

	if(rotateFlag && rotateTimeOut >10 )  {
		 Drv_solenoid_set(0, state_cmd); //lock again
		 rotateFlag = 0;
		 Try =0; 
		 rotateTimeOut=0;
	}
}


void D2_ErrAction(){
	static uint8_t Try=0;
	static uint8_t rotateFlag=0;
	static uint8_t rotateTimeOut=0;
  
	uint8_t lock_pos = motor[2].b.pos;
	uint8_t release_tick = m_tick[2];
	uint8_t state_cmd = gCBUP.bLockStatus.D2_ctrl;
	
	if ( state_cmd &&  (lock_pos==DS_UNLOCK) &&  release_tick==0) {
		Try++;
		//printf("#### D2_ctrl  DS_LOCK ERROR!!  d2_pos [%d] [%d]\r\n", lock_pos, Try);
	}	
	 if(rotateFlag) rotateTimeOut++;
	 if((Try >= 50) && state_cmd ) {
			rotateFlag =1;
			err_timeout=0;
			rotateTimeOut = 0;
			Try=0;
			Drv_solenoid_rotate(0,  0);	   

	 }

	if(rotateFlag && rotateTimeOut >10 )  {
		 Drv_solenoid_set(0, state_cmd); //lock again
		 rotateFlag = 0;
		 Try =0; 
		 rotateTimeOut=0;
	}
}


void Drv_solenoid_release(void)
{
  uint8_t i;
  uint16_t tick;
  Drv_solenoid_tick();
  Drv_solenoid_get_pos();

   D1_ErrAction();
   D2_ErrAction();
  //printf("####-----Drv_solenoid_set pos[%d][%d][%d][%d]\r\n", motor[0].b.pos, motor[1].b.pos, motor[2].b.pos, motor[3].b.pos);
  for(i=0;i<4;i++)
  {
    tick = (MOTOR_TYPE[i] == DOOR_LOCK) ? DOOR_RELEASE_TIME : BATT_RELEASE_TIME;
    
    if(m_tick[i] > tick)
    {
		if( (motor[i].b.ctrl==1 && motor[i].b.pos==PC_LOCK[i]) || (motor[i].b.ctrl==0 && motor[i].b.pos==PC_UNLOCK[i]))  m_tick[i] = tick;
    }

  }

  Drv_solenoid_stop();

}


void Drv_solenoid_rotate(uint8_t pos, uint8_t ccw)
{
#ifdef DEBUG_MOTOR  
 // printf("Drv_solenoid_rotate motor[%02x] ccw[%d]\r\n",pos, ccw );
#endif  

  switch(pos)
  {
      case 0:
        HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
        HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET);   
         if(ccw){
            HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_SET); 
            //HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET); 
         }
        else{
            //HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET);           
            HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_SET); 
        }
        break;
        
        case 1:
          //printf("----SOL_D2p_Pin---  ccw[%02x] \r\n",ccw);
          HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET);          
          if(ccw){
             HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_SET); 
            //HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET);   
          }
          else{
            // HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_SET); 
          }          
          break;      
      case 2:
        HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_RESET); 
        HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_RESET);         
         if(ccw){
             HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_SET); 
          }
        else{
             HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_SET); 
        }       
        break;      

      case 3:
          HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_RESET);      
         if(ccw){
           HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_SET); 
         }
        else{
           HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_SET);           
        }         
        break;       
   }
}

void  maiv_lock_control(uint8_t mot, uint8_t lock) {
  // uint8_t  m_type = MOTOR_TYPE[mot];//check  BATT_LACK or DOOR_LOCK
   printf("####Maiv Lock mot(%02X) lock[%02X] \r\n",mot,  lock);
   
  switch(mot)
  {
      case 0:
        HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
        HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET);   
         if(lock==0){
            HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_SET); 
            //HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET); 
         }
        break;
        
        case 1:
          //printf("----SOL_D2p_Pin---  ccw[%02x] \r\n",ccw);
          HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET);          
          if(lock==0){
             HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_SET); 
            //HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET);   
          }
          break;      
      case 2:
		HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_RESET);         
		if(lock==0){
		   HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_SET); 
		}
        break;      
      case 3:
		HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_RESET);      
		if(lock==0){
		 HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_SET); 
		}
        break;       
   }
}

void Drv_solenoid_set(uint8_t mot, uint8_t lock)
{
  //motor[i].type
  uint8_t pos;
  uint8_t pc_lock, pc_unlock, m_type;
  lock=(lock)? 1:0;
  mot=mot&0x0f;
  pc_lock =   PC_LOCK[mot];
  pc_unlock = PC_UNLOCK[mot];
  m_type = MOTOR_TYPE[mot];
  gCBUP.chg_value.b.pBattOut= HAL_GPIO_ReadPin(GPIOB, pBATT_OUT_Pin)&0x01;
  
  err_timeout=0;//20.07.16
  
  //printf("Drv_solenoid_set pBattOut[%d]\r\n",gCBUP.chg_value.b.pBattOut);  

	switch(mot){
		case 0:  //Door문이 열려있을땐 무조건 unlock으로
			gCBUP.bLockStatus.D1_ctrl= lock;	
			///For led ++     
			if(gCBUP.bLockStatus.D1_ctrl) Drv_led_set(LED_LOCK,0xff,0);
			else Drv_led_set(LED_LOCK,0x00,0);     
			///For led -- 			
			if(gCBUP.chg_value.b.pBattOut) lock=0;
			break;
		case 1: 
			gCBUP.bLockStatus.B1_ctrl= lock;   
			break;
		case 2:  //Door문이 열려있을땐 무조건 unlock으로
			gCBUP.bLockStatus.D2_ctrl= lock;   
			if(gCBUP.chg_value.b.pBattOut) lock=0;
			break;   
		case 3:  
			gCBUP.bLockStatus.B2_ctrl= lock;   
			break;
	}
	motor[mot].b.ctrl=lock;
#ifndef MAIV_LOCK	
	if(!motor[mot].b.ready) return;
#endif	
	
	motor[mot].b.ready=0;
	m_tick[mot] = SOLENOID_TIMEOUT;	
#ifdef LOGOUT  
    printf("#### mot(%02X) lock[%02X]-1[%d] 0[%d] pos[%d]--m_tick[%d]\r\n",mot, lock, pc_lock, pc_unlock,  motor[mot].b.pos, m_tick[mot]);
#endif 
	Drv_solenoid_get_pos();
	pos = motor[mot].b.pos;
	
 #ifndef MAIV_LOCK 
  if( ((motor[mot].b.ctrl==1) && (pos==pc_lock)) || ((motor[mot].b.ctrl==0) && (pos==pc_unlock)))
  { 
    m_tick[mot] = 0; 
    return;
  }
#endif
    
 // printf("#### mot(%02X) lock[%02X]-1[%d] 0[%d] pos[%d]--m_tick[%d] \r\n",mot, lock, pc_lock, pc_unlock,  motor[mot].b.pos, m_tick[mot]);
#ifdef MAIV_LOCK
  
  maiv_lock_control(mot,  lock);
  
#else  
  if(lock) //Lock
  {
    
    if(pos==pc_lock){
		//if(m_type==BATT_LOCK)  Drv_solenoid_rotate(mot,_CCW); 
		//else Drv_solenoid_rotate(mot,_CW);  
    }
    else if(pos==pc_unlock){
		if(m_type==BATT_LOCK)  Drv_solenoid_rotate(mot,_CW); 
		else Drv_solenoid_rotate(mot,_CCW);
    }
    else{
		//if(m_type==BATT_LOCK)  Drv_solenoid_rotate(mot,_CW); 
		//else Drv_solenoid_rotate(mot,_CCW);
		Drv_solenoid_rotate(mot,_CW);
    }
    
  }
  else //unLock
  {
      if(pos==pc_lock){
		if(m_type==BATT_LOCK)  Drv_solenoid_rotate(mot,_CCW); 
		else Drv_solenoid_rotate(mot,_CW);  
	 }
	 else if(pos==pc_unlock){
		//if(m_type==BATT_LOCK)  Drv_solenoid_rotate(mot,_CCW); 
		//else Drv_solenoid_rotate(mot,_CW);  
	 } 
	 else{
		//if(m_type==BATT_LOCK)  Drv_solenoid_rotate(mot,_CCW); 
		//else Drv_solenoid_rotate(mot,_CW);  
		Drv_solenoid_rotate(mot,_CCW);
    }  
  
  }
#endif 
  
} 


void Drv_solenoid_stop()
{

  if( motor[0].b.ready != 0 && motor[1].b.ready != 0 && motor[2].b.ready !=0 &&  motor[3].b.ready != 0 ) return;
 // printf("Drv_solenoid_stop---[%x][%x][%x][%x]\r\n", m_tick[0], m_tick[1], m_tick[2], m_tick[3]);
  
  Drv_solenoid_get_pos();

  if( m_tick[0]==0  && motor[0].b.ready==0) {
      HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET);   
      gCBUP.bLockStatus.D1_err = ( (motor[0].b.ctrl==_LOCK && motor[0].b.pos==PC_LOCK[0]) || (motor[0].b.ctrl==_UNLOCK && motor[0].b.pos==PC_UNLOCK[0])   ) ? 0 : 1;     
       motor[0].b.ready=1;
     //  printf("----motor[0].b.ctr[%d] .D1_err[%d] \r\n",motor[0].b.ctrl, gCBUP.bLockStatus.D1_err);
  }
  
  if( m_tick[1]==0 && motor[1].b.ready==0)  {
     // printf("----SOL_D2p_Pin---  Drv_solenoid_stop \r\n");
      HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET);    
      gCBUP.bLockStatus.B1_err = ( (motor[1].b.ctrl==_LOCK && motor[1].b.pos==PC_LOCK[1]) || (motor[1].b.ctrl==_UNLOCK && motor[1].b.pos==PC_UNLOCK[1])   ) ? 0 : 1;        
      motor[1].b.ready=1;
     // printf("----motor[1].b.ctr[%d] .B1_err[%d] \r\n",motor[1].b.ctrl, gCBUP.bLockStatus.B1_err);
  }  
  
  if( m_tick[2]==0  && motor[2].b.ready==0) {
    
      HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_RESET);  
      gCBUP.bLockStatus.D2_err = ( (motor[2].b.ctrl==_LOCK && motor[2].b.pos==PC_LOCK[2]) || (motor[2].b.ctrl==_UNLOCK && motor[2].b.pos==PC_UNLOCK[2])   ) ? 0 : 1;    
      motor[2].b.ready=1;
     //  printf("----motor[2].b.ctr[%d] .D2_err[%d] \r\n",motor[2].b.ctrl, gCBUP.bLockStatus.D2_err);
  }

  if( m_tick[3]==0  && motor[3].b.ready==0) {
      HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_RESET); 
      gCBUP.bLockStatus.B2_err = ( (motor[3].b.ctrl==_LOCK && motor[3].b.pos==PC_LOCK[3]) || (motor[3].b.ctrl==_UNLOCK && motor[3].b.pos==PC_UNLOCK[3])   ) ? 0 : 1;        
      motor[3].b.ready=1;
      //printf("----motor[3].b.ctr[%d] .B2_err[%d] \r\n",motor[3].b.ctrl, gCBUP.bLockStatus.B2_err);
  }

}

void LOCK_Control(uint8_t *data)
{
  uint8_t val,mask,i;
  val=data[1];
  mask= data[2];
 // if(CHECK_BIT(mask,1)) Drv_solenoid_set(1, CHECK_BIT(val,1));
  
 #ifdef FUNCTION_MOTOR_TEST   
  if( gCBUP.bLockStatus.D1_err || gCBUP.bLockStatus.D2_err || gCBUP.bLockStatus.B1_err || gCBUP.bLockStatus.B2_err ) LOCK_ERR_CNT++;
  if( gCBUP.bLockStatus.D1_err ) LOCK_ERR_VAL= LOCK_ERR_VAL|0x01 ; else  LOCK_ERR_VAL=LOCK_ERR_VAL&0xFE;
  if( gCBUP.bLockStatus.D2_err ) LOCK_ERR_VAL= LOCK_ERR_VAL|0x02 ; else  LOCK_ERR_VAL=LOCK_ERR_VAL&0xFD;
  if( gCBUP.bLockStatus.B1_err ) LOCK_ERR_VAL= LOCK_ERR_VAL|0x04 ; else  LOCK_ERR_VAL=LOCK_ERR_VAL&0xFB;
  if( gCBUP.bLockStatus.B2_err ) LOCK_ERR_VAL= LOCK_ERR_VAL|0x08 ; else  LOCK_ERR_VAL=LOCK_ERR_VAL&0xF7;
#endif
  
  for(i=0;i<4;i++){
    if(CHECK_BIT(mask,i)) Drv_solenoid_set(i, CHECK_BIT(val,i));
  }
}


void Drv_solenoid_Control(uint8_t what)
{
  uint8_t lock;
  lock= (CHECK_BIT(what,0))? 1 : 0;
  Drv_solenoid_set(0, lock);
  
  lock= (CHECK_BIT(what,1))? 1 : 0;
  Drv_solenoid_set(1, lock);
  
  lock= (CHECK_BIT(what,2))? 1 : 0;
  Drv_solenoid_set(2, lock);

  lock= (CHECK_BIT(what,3))? 1 : 0;
  Drv_solenoid_set(3, lock);

}
