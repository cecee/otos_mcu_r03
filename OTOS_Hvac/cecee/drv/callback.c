//#include "main.h"
#include "stm32f1xx_hal.h"
#include "callback.h"
#include "extern_hvac.h"
#include "stm32f1xx_it.h"

uint16_t systick=0;
uint16_t sec_tick=0;
uint16_t sec_dht=0;
uint8_t HVAC_AUTO_FLAG=0;

uint8_t   t2_tick_dth=0;//100ms
uint8_t   t2_tick_adc=0;
uint8_t   t2_tick_acm=0;
uint8_t   acm_id=0;

uint16_t  t3_tick1=0;
uint16_t	t3_tick2=0;
uint16_t	t3_tick3=0;
uint16_t  th_start_tick=0;
uint8_t   th_start=1;

uint8_t   adc_mux=0;
uint8_t   DHT_READY_FLAG=0;
uint32_t gAdcValue[16];

void HAL_SYSTICK_Callback(void)
{
  // HAL_GPIO_TogglePin(GPIOB, pBAT_ENABLE_Pin);
  systick++;
  if(systick>1000)//1000ms
  {
    systick=0;
    sec_tick++;
    sec_dht++;
    // DBG(WARN,"HAL_SYSTICK_Callback sec_tick[%d]\n",sec_tick);
  }  
  if(sec_dht>5)//5sec마다
  {
    sec_dht=0;
    //Drv_DhtReadValue();
  }   
 }

void HAL_GPIO_EXTI_Callback(uint16_t  GPIO_pin)
{
   if(GPIO_pin==DHT11_S1_Pin){
       th_irq_buf[0][th_irq_cnt[0]++]=t3_tick1;
  	 t3_tick1=0;
    }
    else  if(GPIO_pin==DHT11_S2_Pin){
     th_irq_buf[1][th_irq_cnt[1]++]=t3_tick2;
  	 t3_tick2=0; 
   }
    else  if(GPIO_pin==DHT11_S3_Pin){
     th_irq_buf[2][th_irq_cnt[2]++]=t3_tick3;
  	 t3_tick3=0; 	
   }
   //printf("EXTI9_5_IRQHandler---GPIO_pin[%x]\r\n", GPIO_pin);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  
  if(htim->Instance==TIM2){//TIM2 100ms
    t2_tick_dth++;
    t2_tick_adc++;
    t2_tick_acm++;
    if(t2_tick_acm>5){//200ms
       t2_tick_acm=0;
        HVAC_SetACMonitor(acm_id);
        acm_id++;
        if(acm_id>3) acm_id=0;
    }
    else if(t2_tick_adc>5){//500ms
       t2_tick_adc=0;
       adc_mux = ADC_Process(adc_mux); 
       Hvac_adcMux(adc_mux);
       Hvac_gpio_getport();
    }
    else if(t2_tick_dth>50){
      t2_tick_dth=0;
      Drv_DhtReadValue();
      //HVAC_TempControl();
    }
  }
  else if(htim->Instance==TIM3){//TIM2 5us
      t3_tick1++;
      t3_tick2++;
      t3_tick3++;
      //HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_6);
      //printf("TIM3---th_start_tick[%d]\r\n", th_start_tick);
      if(th_start) th_start_tick++;
     // if(th_start_tick>4000) //20ms 대기 spec.18ms
      if(th_start_tick>4000) //20ms 대기 spec.18ms 
      {
        th_start_tick=0;
        th_start=0;
        Drv_DhtStartInterrup();
      }
  }
  
 // if (htim->Instance == TIM1) printf("htim1---\r\n");
 // if (htim->Instance == TIM2) printf("htim2---\r\n");
 // if (htim->Instance == TIM3) printf("htim3---\r\n");
}

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *hcan)
{
  //printf("Can RX INT [%x]\r\n", hcan->pRxMsg->ExtId); 
  HAL_CAN_Receive(hcan, CAN_FIFO0, 10);
  CAN_RCV_FLAG=1;
  if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) != HAL_OK)
  {
    Error_Handler();
  } 

}

uint8_t gCh1[2];
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    
  if (huart->Instance == USART3)
  {
    HVAC_PutAcmData(gCh1[0]);
    while(HAL_UART_Receive(&huart3, &gCh1[0], 1, 0)==HAL_OK){
        HVAC_PutAcmData(gCh1[0]);
    } 
    HAL_UART_Receive_DMA(&huart3, &gCh1[0], 1);
  }
}


uint8_t  ADC_Process(uint8_t mux)
{
  uint8_t i;
  uint16_t mV[5];
  int16_t tmp36[4]; 
  //TMP35 500mv offset 10mV/oC
  mV[0] = ( 2500 * (uint16_t)gAdcValue[0] )/4095;
  mV[1] = ( 2500 * (uint16_t)gAdcValue[1] )/4095;
  mV[2] = ( 2500 * (uint16_t)gAdcValue[2] )/4095; 
  mV[3] = ( 2500 * (uint16_t)gAdcValue[3] )/4095;
  
  
  tmp36[0] =(int16_t) ( mV[0]-500)/10;
  tmp36[1] =(int16_t) ( mV[1]-500)/10;
  tmp36[2] =(int16_t) ( mV[2]-500)/10;
  tmp36[3] =(int16_t) ( mV[3]-500)/10;
  for(i=0;i<4;i++) gStatus.tmp36[mux].tmp[i]=tmp36[i]&0xff;
  
  //0 1 4 5 6
 // DBG(WARN,"ADC_Process mux[%d] 0[%04x] 1[%04x] [%04x] [%04x] cds[%04d] \r\n",mux, tmp36[0], tmp36[1], tmp36[2],tmp36[3], gAdcValue[4]);
 // DBG(WARN,"ADC_Process mux[%d] 0[%04d] 1[%04d] [%04d] [%04d] 4[%04d] \r\n",mux, gAdcValue[0], gAdcValue[1], gAdcValue[2],gAdcValue[3], gAdcValue[4]);
  mux++;
  if(mux>2) mux=0;//0 1 2 
  return mux;

}


