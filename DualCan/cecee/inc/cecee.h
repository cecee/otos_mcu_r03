#ifndef CECEE_H_
#define CECEE_H_
/*
#include "time.h"
#include "string.h"
#include "cecee_drv.h"
#include "cecee_util.h"
#include "cecee_hal.h"
#include "iwdg.h"
#include "../drv/drv_cboard.h"
#include "../drv/drv_sps194.h"
#include "../drv/drv_bms.h"
#include "../drv/drv_as1107.h"
#include "../drv/drv_solenoid.h"
#include "../drv/drv_can.h"
#include "../drv/call_back.h"
#include "../drv/drv_sflash.h"
//#include "../util/cbuffer.h"
#include "../hal/hal_adc.h"
#include "../hal/hal_uart.h"
#include "../hal/hal_spi.h"
*/
#include "string.h"
#include "../hal/hal_can.h"

#include <stdint.h>

#define xKARA_BMS

#define BMS_ID_102	0x0102
#define BMS_ID_101	0x0101
#define SCT_ID_A0A0	0xA0A0
#define SCT_ID_A0A1	0xA0A1


/*
* Define : Boolean
*/
#define FALSE		0
#define TRUE		1

/*
* Define : Return Value(Ok and Error)
*/
#define RET_OK		(0)
#define RET_ERR	(-1)

/*
* Define : Set Value(On/Off)
*/
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)

/*
* Define : Debug Message Type
*/
#define ERR			1
#define WARN	          2
#define MSG			3
#define PRT			4
#define ATC			5
#define ATR			6

/*
* Define : Filename
*/
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))


//extern uint8_t _10msTimer1;
//extern uint8_t _100msTimer2;
//extern uint8_t bms_check_tick;

extern uint8_t CAN_RCV_FLAG;
extern uint8_t _101Cnt;
extern uint8_t _102Cnt;
// For CAN1
//extern uint8_t can1_rxbuf[8];
extern uint8_t bmsSQ;
extern BMS_101 bms0101;
extern BMS_102 bms0102;
extern TX_A0A0 tx_a0a0;
extern TX_A0A1 tx_a0a1;

#endif
