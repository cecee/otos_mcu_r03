#include "stm32f1xx_hal.h"
#include "Hvac_can.h"
#include "extern_hvac.h"

extern CAN_HandleTypeDef hcan1;
extern uint8_t TransmitMailbox1;


uint8_t cbd_rcv_buf[15][16][8]={0,};//can 15번 올라온다
uint8_t dbd_rcv_buf[4][16][8]={0,};//can 4번 올라온다

int Drv_dCan_init(void)
{
  Hal_CAN_Init();
  return 1;
}

void  Drv_CanRx(void)
{

  	uint32_t ExtId, snd_id;
  	uint8_t i,sZ;
  	uint8_t cmd;
  	uint8_t block_cnt,remain;
  	uint8_t block_buf[8];
     uint8_t can1_rxbuf[8];
    	uint8_t _buf[160];
      
  	ExtId=hcan1.pRxMsg->ExtId;
  	memcpy(can1_rxbuf, hcan1.pRxMsg->Data,8);
    
  	if (CAN_RCV_ID !=ExtId) return;	
     cmd = can1_rxbuf[0];

  	printf("CAN1_FromMASTER [0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x]\r\n",
            can1_rxbuf[0], can1_rxbuf[1], can1_rxbuf[2], can1_rxbuf[3], 
            can1_rxbuf[4], can1_rxbuf[5], can1_rxbuf[6], can1_rxbuf[7] );     
 	switch(cmd)
	{
		case 0x00://request
            printf("Drv_can1_rcv.. request\r\n");
            sZ=sizeof(gStatus);
            remain=sZ%8;
            block_cnt=sZ/8;
            block_cnt= (remain!=0)? (block_cnt+1) :(block_cnt);
            printf("block_cnt [%d]  gStatus sz[%d]\r\n",block_cnt, sZ);              
#if 0          
            gStatus.bFanCtrl.f1=1;
            gStatus.dth[0].tmp=0xa0aa;  gStatus.dth[0].humi=0xa1aa;
            gStatus.dth[1].tmp=0xb0aa;  gStatus.dth[1].humi=0xb1aa;
            gStatus.dth[2].tmp=0xc0aa;  gStatus.dth[2].humi=0xc1aa;
            for(i=0;i<3;i++){
              gStatus.tmp36[i].tmp[0]=0x80+i;
              gStatus.tmp36[i].tmp[1]=0x90+i;
              gStatus.tmp36[i].tmp[2]=0xA0+i;
              gStatus.tmp36[i].tmp[3]=0xB0+i;
            }
            gStatus.pwr.v=2200;
            gStatus.pwr.i= 1000;
            gStatus.pwr.p=33000;
            gStatus.bInput.ac_fail=1;
            gStatus.bInput.dc_fail=0;
            gStatus.bInput.shock=1;
           // gStatus.HeaterAlarm.b.alarm_heater=1;//tesy
           // gStatus.HeaterAlarm.alarm=0xAA55;//tesy
#endif           
            memcpy(&_buf[0],&gStatus, sZ ); 
  		  for(i=0;i<block_cnt;i++){
                snd_id = CAN_SND_ID + i;
                memset(&block_buf[0],0,8);
                memcpy(&block_buf[0], &_buf[i*8], 8);
#ifdef  CAN_UP_DEBUG
  		printf("Datag id[%x] =[%02x][%02x][%02x][%02x]-[%02x][%02x][%02x][%02x]\r\n",
          snd_id,block_buf[0],block_buf[1],block_buf[2],block_buf[3],block_buf[4],block_buf[5],block_buf[6],block_buf[7]);
#endif      
			 Drv_CanTx(snd_id, block_buf);
                HAL_Delay(10);
 			}     
            break;
          case 0x04://Relay control
              HVAC_Relay(can1_rxbuf);
              break;    
           case 0x05://FAN control
              HVAC_Fan(can1_rxbuf);
              break;             
		default:
			break;
	}

}


void Drv_CanTx(uint32_t can_id, uint8_t *data)
{
 	//DBG(WARN,"Drv_CanTx [%08X] [%x][%x]\n",can_id, data[0], data[1]);
	hcan1.pTxMsg->ExtId = can_id;
	memcpy(hcan1.pTxMsg->Data, data ,8);
     TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
	do
	{
		TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
	}
	while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );

}
