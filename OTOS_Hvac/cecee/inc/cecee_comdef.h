/*
 * cecee_comdef.h  
 *
 */ 
 /*
 * Define : Debug Message
 */
#ifndef CECEE_COMDEF_H_
#define CECEE_COMDEF_H_

#include "time.h"
#include "string.h"

#define  xCAN_UP_DEBUG
#define  HVAC_DEBUG
/*
* Define : Debug Message Type
*/
#define ERR			1
#define WARN	          2
#define MSG			3
#define PRT			4
#define ATC			5
#define ATR			6

/*
* Define : Boolean
*/
#define FALSE		0
#define TRUE		1

/*
* Define : Return Value(Ok and Error)
*/
#define RET_OK		(0)
#define RET_ERR		(-1)

/*
* Define : Set Value(On/Off)
*/
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)

/*
* Define : Filename
*/
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))


/*
* Maximum string size 
*/
#define MAX_SIZE_DEV_ID				16
#define MAX_SIZE_IMEI				15
#define MAX_SIZE_URL				128
#define MAX_SIZE_FOTA_ID			32
#define MAX_SIZE_FOTA_PW			32
#define MAX_SIZE_ADDR				128
#define MAX_SIZE_MSG				80
#define MAX_SIZE_SMS_PW				8
#define MAX_SIZE_SMS_SERIAL			16
#define MAX_SIZE_PHONE_NUM			41	// 3GPP �԰�
#define MAX_STR_SIZE_SMS_TEXT		640	// 3GPP �԰�
// Parsing tools
#define MAX_SIZE_PARSING_PARAM		32
#define MAX_CNT_PARSING_PARAM		10

/*
* Minimum string size 
*/
#define MIN_SIZE_SMS_PW				4

/************************************************************/
// define Enum
/************************************************************/

/*
* Protocol Message Type Index Number
*/
enum {
	CAN = 0,
	NFC,
	OTH
};

/*
* Protocol Message Pack Index Number
*/
enum {
	MSG_DEVICE_TYPE_NONE=0,
	MSG_DEVICE_TYPE_KIDS_PHONE,
	MSG_DEVICE_TYPE_GSM_TRAKCER,
	MSG_DEVICE_TYPE_EMOTO_PRO_3G,
	MSG_DEVICE_TYPE_MAX
};



/*
* Define : Modem Status
*/
typedef enum{
	MDM_STATUS_IDLE=0x00,
	MDM_STATUS_INIT_DONE,
	MDM_STATUS_CONNECT,
	MDM_STATUS_DISCONNECT,
	MDM_STATUS_NOSIGNAL,
	MDM_STATUS_MAX
}eMdmStatus;
/*
* Define : Modem Status
*/
typedef enum{
	GPS_STATUS_IDLE=0x00,
	GPS_STATUS_SEARCHING,
	GPS_STATUS_FIXED,
	GPS_STATUS_FAIL,
	GPS_STATUS_MAX
}eGpsStatus;

/*
* Define : GPIO External Interrupt
*/
typedef enum{
	GPIO_EXTI_ACC3X=0x00,
	GPIO_EXTI_RF,
	GPIO_EXTI_RI,
	GPIO_EXTI_ACC,
	GPIO_EXTI_MAX=0xff
}eGpioExtI;

/*
* Define : Motor bike operation state
*/
enum {
	BIKE_OPERATION_STATE_NONE	=0,
	BIKE_OPERATION_STATE_INIT,
	BIKE_OPERATION_STATE_INIT_DONE,
	BIKE_OPERATION_STATE_ACC_OFF,
	BIKE_OPERATION_STATE_ACC_ON,
	BIKE_OPERATION_STATE_MAX
};

/************************************************************/
// define Structure
/*
* Type Define : MessageQ command
*/
typedef struct MSG_
{
  unsigned int	Cmd;
  unsigned int	Msg[4];
} MSG_t;

#pragma pack(1)
/*
* String Parsing tools
*/
typedef struct _tParsing{
	unsigned char pcnt;
	char param[MAX_CNT_PARSING_PARAM][MAX_SIZE_PARSING_PARAM];
} tParsing;

#pragma pack()

#endif /* CECEE_COMDEF_H_ */

