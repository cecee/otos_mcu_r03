#include "main.h"
#include "stm32f1xx_hal.h"
#include "drv_bms.h"
#include "cecee.h"

uint8_t gBms_serch_flag=0;
uint8_t gBms_try=0;
uint8_t gBms_add=0xff;
uint8_t gBms_commType = 0;
uint8_t gBmsRequestCnt=0;
uint8_t bms_messge=0;


//BMS_VALUE bms_value;


void Drv_bms_init(void){
//	gBms_serch_flag=0;
	HAL_GPIO_WritePin(GPIOA, TXEN_Pin,GPIO_PIN_RESET); 
     memset(&gCBUP.bms_value,0,sizeof(gCBUP.bms_value));
	Drv_bms_restart();
}

void Drv_bms_restart(void)
{
  gBms_serch_flag=1;
  gBms_commType = Drv_bms_getCommType();
  HAL_GPIO_WritePin(GPIOA, TXEN_Pin,GPIO_PIN_RESET); //serial대기
  Drv_bms_enable(SET_OFF);
  HAL_Delay(100);
  Drv_bms_enable(SET_ON);
}

uint8_t Drv_bms_getCommType(void)
{
  uint8_t type=0; //RS485
  //set pCOM_TEST
	//HAL_GPIO_WritePin(GPIOA, TXEN_Pin,GPIO_PIN_RESET); 
  //HAL_Delay(10);
  //if(ADC[6]>8) type=1; //CAN
	return type;
  
}
/*
void Drv_bms_request(void)
{
#ifdef BMS_DBUG	
	printf("gBms_add[%x]\r\n",gBms_add);
#endif	
	if(gBms_add==0xff) {
		memset(&gCBUP.bms_value,0,sizeof(gCBUP.bms_value));
		Drv_bms_restart();
		return;
	}
	bms_messge=1;
}
*/

/*
void Drv_bms_monitor(void)
{
	
	uint8_t cell_cnt=20;
	uint8_t cell_temp_cnt=6;
	uint8_t none=0;
	if(bms_messge==0) return;
#ifdef BMS_ONE_COMMAND
		Drv_bms_cell_balance_control(1,3925);
		bms_messge=0;
#else
	switch(bms_messge){
		case 1:
			Drv_bms_station_request();
			bms_messge=0;
			return;
		case 2:
			Drv_bms_get( GET_CELL_VOLTAGE,  &cell_cnt, 2,0);
			bms_messge=3;
			return;		
		case 3:
			Drv_bms_get( GET_CELL_TEMPER,  &cell_temp_cnt, 2,0);
			bms_messge=4;
			return;		
		case 4:
		//	Drv_bms_get( GET_PACK_VOL_CUR,  &none, 1,0);
			bms_messge=5;
			return;		
		case 5:
			Drv_bms_get( GET_ALARM_STATUS,  &none, 1,0);
			bms_messge=6;
			return;		
		case 6:
		//	Drv_bms_get( GET_INFOR_SOC,  &none, 1,1);
			bms_messge=7;
			return;		
		case 7:
		//	Drv_bms_get( GET_INFOR_CYCLE,  &none, 1,1);
			bms_messge=8;
			return;			
		case 8:
			Drv_bms_get( GET_CELL_BALANCING_STATUS,  &none, 1,0);
			bms_messge=9;
			return;				
		case 9:
			bms_messge=0;
			return;					
	}
#endif
}

*/
void Drv_bms_enable(uint8_t value)
{
     GPIO_PinState pinState;
     
     pinState = (value)? GPIO_PIN_SET : GPIO_PIN_RESET;
 	HAL_GPIO_WritePin(GPIOB, pBAT_ENABLE_Pin, pinState);

}

uint8_t chk_crc(uint8_t *data)
{
	uint8_t i;
	uint32_t sum=0;
	uint8_t crc;
	uint8_t length=data[2]+2;
	uint8_t crc_pos=length+1;
	for(i=0;i<length;i++){
		sum=sum+data[i+1];
	}
	crc=sum&0xff;
//	printf("bms_buf data[2][%02x] crc_pos[%02X]\r\n",data[2], crc_pos);
//	printf("bms_buf crc_pos[%02x] crc[%02X]\r\n",data[crc_pos], crc);
	if(data[crc_pos]== crc) return 1;
	else return 0;
}

void put_bmsStationData(const uint8_t *data)
{
  uint8_t i;
  gBmsRequestCnt=0;
  int16_t temper=0;
  
  gCBUP.bms_value.pack_serial=data[4];
  gCBUP.bms_value.pack_capacity=data[5];
  gCBUP.bms_value.pack_parallel=data[6]; 
  gCBUP.bms_value.pack_year=data[7];
  gCBUP.bms_value.pack_month=data[8];   
  gCBUP.bms_value.pack_day=data[9]; 
  gCBUP.bms_value.pack_nation=data[10];
  gCBUP.bms_value.pack_serialNumber=BUILD_UINT16(data[12], data[11]);//LOW HIGH순서
  
  for(i=0;i<3;i++){
    gCBUP.bms_value.pack_userData.value[i]=data[i+13];
  } 
 // gCBUP.bms_value.pack_userData=data[12]<<16 + data[13]<<8 + data[14];
  
   for(i=0;i<6;i++){
    gCBUP.bms_value.pack_Counter.value[i]=data[i+16];
  } 
   
  for(i=0;i<20;i++){
    gCBUP.bms_value.cell_votage[i]=BUILD_UINT16(data[(i*2) + 23], data[(i*2) + 22]);
  }
 
  for(i=0;i<4;i++){
    gCBUP.bms_value.cell_temper[i]=BUILD_UINT16(data[(i*2) + 63], data[(i*2) + 62]);
    if(gCBUP.bms_value.cell_temper[i]>temper)  temper= gCBUP.bms_value.cell_temper[i]; //최고온도값응 대표값으로
  } 
  gCBUP.bms_value.pack_temper=temper;

  gCBUP.bms_value.pack_voltage = BUILD_UINT16(data[71], data[70]);
  gCBUP.bms_value.pack_current = BUILD_UINT16(data[73], data[72]);
  gCBUP.bms_value.pack_soc = BUILD_UINT16(data[75], data[74]);
 
  for(i=0;i<4;i++){
    gCBUP.bms_value.pack_err.value[i]=data[i+76];
  } 
   for(i=0;i<4;i++){
    gCBUP.bms_value.pack_cellBancing.value[i]=data[i+80];
  } 
#ifdef  BMS_DBUG 
//  printf("charging[%02d][%04d] pack_voltage[%d] current[%d] soc[%d]\r\n", 
 //       gCBUP.chg_value.charging_seg, gCBUP.chg_value.charging_time, gCBUP.bms_value.pack_voltage, gCBUP.bms_value.pack_current, gCBUP.bms_value.pack_soc);  
 // printf("pack_Counter[%02x][%02x][%02x][%02x][%02x][%02x]\r\n", data[16],data[17],data[18],data[19],data[20],data[21]);

#endif  
}

/*	
void put_bmsData(uint8_t *data, uint8_t id)
{
	uint8_t i,j;
	uint16_t t16;
	uint16_t tmp16[20];
	uint8_t sz=data[2]-1;
	uint8_t i_sz=sz/2;
	uint8_t j_sz=(sz/2)-1;

	switch(id){
		case RTN_PACK_VOL_CUR:
				gCBUP.bms_value.pack_voltage=BUILD_UINT16(data[5], data[4]);
				gCBUP.bms_value.pack_current=BUILD_UINT16(data[7], data[6]);
				//printf("vol[0x%04x] cur[0x%04x]\r\n",gCBUP.bms_value.pack_voltage,gCBUP.bms_value.pack_current);
			return;
		case RTN_ALARM_STATUS:
				gCBUP.bms_value.pack_err.value[0]=data[4];
				gCBUP.bms_value.pack_err.value[1]=data[5];
				gCBUP.bms_value.pack_err.value[2]=data[6];
				gCBUP.bms_value.pack_err.value[3]=data[7];
				//printf("pack_err[%04x] [%04x] [%04x] [%04x]\r\n",data[4],data[5],data[6],data[7]);
			return;			
		case RTN_INFOR_SOC:
				gCBUP.bms_value.pack_soc=BUILD_UINT16(data[5], data[4]);
				//printf("pack_soc[%04x]\r\n",gCBUP.bms_value.pack_soc);
			return;
		case RTN_INFOR_CYCLE:
				gCBUP.bms_value.pack_cycle=BUILD_UINT16(data[5], data[4]);
				//printf("pack_cycle[%04x]\r\n",gCBUP.bms_value.pack_cycle);
			return;
		case RTN_CELL_BALANCING_STATUS:
			gCBUP.bms_value.cell_bal_status[0]=data[4];
			gCBUP.bms_value.cell_bal_status[1]=data[5];
			gCBUP.bms_value.cell_bal_status[2]=data[6];
			gCBUP.bms_value.cell_bal_status[3]=data[7];
			//printf("cell_bal_status[%04x] [%04x] [%04x] [%04x]\r\n",data[4],data[5],data[6],data[7]);
			return;	
			
	}
	memset(tmp16,0,sizeof(tmp16));
	memcpy(tmp16,&data[4],sz);
	for(i=0, j=j_sz;i<i_sz;i++){
		t16=tmp16[j--];
		t16=SWAPBYTE_US(t16);
		switch(id){	
    		case RTN_CELL_VOLTAGE:
					gCBUP.bms_value.cell_votage[i]=t16;
					break;
    		case RTN_CELL_TEMPER:
					gCBUP.bms_value.cell_temper[i]=t16;
					break;	
		}
	}

	
	switch(id){	
		case RTN_CELL_VOLTAGE:
		//	for(i=0;i<i_sz;i++) printf("gCBUP.bms_value.cell_votage[%d][%04X]\r\n",i, gCBUP.bms_value.cell_votage[i]);
			break;
		case RTN_CELL_TEMPER:
		//	for(i=0;i<i_sz;i++) printf("gCBUP.bms_value.cell_temper[%d][%04X]\r\n",i, gCBUP.bms_value.cell_temper[i]);
			break;			
	}

}
*/

//new bms command 20cell짜리 한꺼번에 데이타 올라오는거
void Drv_bms_station_request(void)
{
         
    uint8_t data[6]={0x02, 0x00, 0x01, 0xc1,0xc2, 0x03};
    if(gBmsRequestCnt<5)  gBmsRequestCnt++;
    //memset(&gCBUP.bms_value,0,sizeof(gCBUP.bms_value));
    //Hal_Uart_BmsTxData(data,6);
    MX_USART_Bms_SendData(data,6);
    //HAL_Delay(10);
}

void Drv_bms_add_confirm(uint8_t add)
{
	uint8_t data[5]={0x3B,0x3F,add,0x4F,0x4B};
     MX_USART_Bms_SendData(data,5);
	//Hal_Uart_BmsTxData(data,5);
}

void Drv_bms_get( uint8_t command,  uint8_t *data, uint8_t length, uint8_t flg)
{
	uint8_t i;
	uint8_t sbuf[16];
	uint8_t crc=0;
	uint8_t sof=0x02;
	uint8_t eof=0x03;
	uint8_t data_len;
	uint8_t tx_len;
	
	data_len=length-1;
	memset(sbuf,0,sizeof(sbuf));
	sbuf[0]=sof;
	//sbuf[1]=gBms_add-0xAA;
	sbuf[1]=0x01;
		
	if(flg){
	 	tx_len=length+5;
	 	sbuf[2]=length;//rev
		sbuf[3]=command;
	//	sbuf[4]=command;
		
		for(i=0;i<data_len;i++)  sbuf[4+i]=data[i];
		for(i=1;i<tx_len-2;i++)  crc+=sbuf[i];	
	
	}
	else {
	 	tx_len=length+5;
		sbuf[2]=length;
		sbuf[3]=command;
		for(i=0;i<data_len;i++)  sbuf[4+i]=data[i];
		for(i=1;i<tx_len-2;i++)  crc+=sbuf[i];
		
	}

	crc&=0xff;
	
	sbuf[tx_len-2]=crc;
	sbuf[tx_len-1]=eof;
	
#ifdef BMS_ONE_COMMAND
for(i=0;i<10;i++){
	printf("sbuf[%d][%x]\r\n",i,sbuf[i]);
	}
#endif
	//Hal_Uart_BmsTxData(sbuf,tx_len);
    MX_USART_Bms_SendData(sbuf,tx_len);
	HAL_Delay(100);
}


void Drv_bms_cell_balance_control(uint8_t balance_on, uint16_t balance_voltage)
{
	uint8_t data[3];
	data[0]=(balance_on!=0) ? 0xA8:0x00; //byte3
	data[1]=HI_UINT16(balance_voltage);//byte2
	data[2]=LO_UINT16(balance_voltage);//byte1
	Drv_bms_get( SET_CELL_CONTROL,  &data[0], 4,0);
}