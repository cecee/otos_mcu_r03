/*
  drv_as1107.h
*/
#ifndef DRV_AS1107_H_
#define DRV_AS1107_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
#include <stdint.h>

#define TIME_BLINK 200

enum {
	LED_BATT=0,
	LED_CAN,
	LED_RS485,
	LED_LOCK,
	LED_CHGR,
	LED_CCV,
	LED_BP,
	LED_TMP,
};

typedef struct _LED_BLINK
{
  uint8_t blink;
  uint16_t tick;
}LED_BLINK;

typedef struct _LED_MONO
{
  uint8_t  blink;
  uint16_t tick;
}LED_MONO;

typedef struct _LED_RGB
{
  uint8_t  blink;
  uint16_t tick;
  uint16_t r;
  uint16_t g;
  uint16_t b;
}LED_RGB;

typedef struct tag_ledBit{
	uint8_t BATT: 1;
	uint8_t CAN: 1;
	uint8_t RS485: 1;
	uint8_t LOCK: 1;
	uint8_t CHGR: 1;
	uint8_t CCV: 1;
	uint8_t BP: 1;
	uint8_t TMP: 1;
}BIT_LED;

typedef struct tag_ui
{
	uint16_t voltage;
	uint16_t current;
 	uint8_t ledValue; 
}UI_FND;


/****************************************************************************
*                               function
*****************************************************************************/
void Drv_as1107_init(void);
void Drv_cat4109_init(void);

void writeRegister(uint8_t addr, uint8_t value);
void writeRegisters(uint16_t value);
void displayTest(void);
void noDisplayTest(void);
void Drv_display_fnd();
void Drv_display_cid();
void Drv_led_set(uint8_t led,  uint8_t timeout, uint8_t blink);
void Drv_led_release(void);
void Drv_led_tick(void);
//void display_voltage(uint16_t value);
void display_testfnd(void);
void display_decfnd(uint16_t lvalue, uint16_t rvalue, uint8_t dot);

void Drv_RGB_Blink(uint8_t on);
void Drv_SetRGB(uint8_t *data);
#endif