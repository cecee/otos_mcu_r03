/*
Hvac_dht11.h
*/
#ifndef HVAC_DHT11_H_
#define HVAC_DHT11_H_

void init_dth11(void);
void Drv_DhtStartInterrup(void);
void Drv_DhtReadValue(void);
#endif