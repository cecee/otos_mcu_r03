/*
 * dbg.c
 *
 * Created: 2018-01-08
 *  Author: SY.Shin
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
//#include "cmsis_os.h"

#include "cecee.h"
#include "dbg.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/
//static osSemaphoreId gMDbgSema=NULL;
static char pMsg[256];

//osSemaphoreDef(gMDbgSema);

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/


/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        DBG_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int DBG_Init(void)
{
/*
  gMDbgSema = osSemaphoreCreate(osSemaphore(gMDbgSema), 5);
	
	if(gMDbgSema == NULL)
	{
		printf("%s() semaphore create failed...\r\n",__FUNCTION__);
		
		return RET_ERR;
	} 
  */
	return RET_OK;
}
/*****************************************************************************
*  Name:        Dbg_Lock()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Dbg_Lock(void)
{
//	if(gMDbgSema != NULL)
//	{
//		if(osKernelRunning())osSemaphoreWait(gMDbgSema, 100/*osWaitForever*/);
//	}
//
	return 0;
}

/*****************************************************************************
*  Name:        Dbg_Unlock()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
void Dbg_Unlock(void)
{
//	if(gMDbgSema != NULL)
//	{
//		if(osKernelRunning())osSemaphoreRelease(gMDbgSema);
//	}
}

/*****************************************************************************
*  Name:        DBG()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
void DBG(int nType, const char* format, ...)
{
	va_list pArgument;
	unsigned char ssize=0;
	
	Dbg_Lock();

#if (DSPONE_DEBUG_LVL==DSP_NO_DEUBUG)
	nType = 0;
#elif (DSPONE_DEBUG_LVL==DSP_DEUBUG_ERR)
	if(ERR != nType)nType = 0;
#elif (DSPONE_DEBUG_LVL==DSP_DEUBUG_WARN)
	if((ERR != nType) && (WARN != nType))nType = 0;
#elif (DSPONE_DEBUG_LVL==DSP_DEUBUG_MSG)
#endif
	if (nType == 0 || NULL == format)
	{
		Dbg_Unlock();
		return;
	}

	memset(pMsg, 0x0, sizeof(pMsg));

	if(ERR==nType)
	{
		sprintf(pMsg, "%s[E]",ANSI_RED);
	}
	else if(WARN==nType)
	{
		sprintf(pMsg, "%s[W]",ANSI_YELLOW);
	}
	else if(MSG==nType)
	{
		sprintf(pMsg, "[M]");
	}
	else if(ATC==nType)
	{
		sprintf(pMsg, "%s[C]",ANSI_GREEN);
	}
	else if(ATR==nType)
	{
		sprintf(pMsg, "%s[R]",ANSI_GREEN);
	}

	ssize = strlen(pMsg);
	
	va_start(pArgument, format);
	{
		vsprintf(&pMsg[ssize], format, pArgument);
		pMsg[252] = '.';
		pMsg[253] = '.';
		pMsg[254] = '.';
		pMsg[255] = '\0';
		fprintf(stderr, pMsg);
	}
	va_end(pArgument);
	
	if(ERR==nType || WARN==nType || ATC==nType || ATR==nType)fprintf(stderr, "%s\r",ANSI_INIT);
	else if(MSG==nType)fprintf(stderr, "\r");

	Dbg_Unlock();

	return;
}
