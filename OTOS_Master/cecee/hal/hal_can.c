/*
 * hal_adc.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stm32f1xx_hal.h"
#include "can.h"
#include "hal_can.h"

#include "d_cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/
extern CAN_HandleTypeDef hcan1;
//extern void Error_Handler(void);

CanTxMsgTypeDef   TxMessage1;
CanRxMsgTypeDef   RxMessage1;
uint8_t TransmitMailbox1 = 0;

/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Hal_CAN_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/

int Hal_CAN_Init(void)
{
    

  
    hcan1.pTxMsg = &TxMessage1;
    hcan1.pRxMsg = &RxMessage1; 
    CAN_FilterConfTypeDef filtercfg;
    
    uint32_t FilterId   = 0x1cec0000;
    uint32_t FilterMask  = 0x1cec0000;   
    
    filtercfg.FilterNumber = 0;
    filtercfg.FilterMode = CAN_FILTERMODE_IDMASK;
    filtercfg.FilterScale = CAN_FILTERSCALE_32BIT;
    filtercfg.FilterIdHigh = (FilterId << 3) >> 16;                   /* 29비트 중 상위 2 바이트 */
    filtercfg.FilterIdLow = (0xFFFF & (FilterId << 3)) | (1 << 2);      /* 29비트 중 하위 2 바이트 + IDE */
    filtercfg.FilterMaskIdHigh = (FilterMask << 3) >> 16;
    filtercfg.FilterMaskIdLow = (0xFFFF & (FilterMask << 3)) | (1 << 2);
    filtercfg.FilterFIFOAssignment = CAN_FIFO0;
    filtercfg.FilterActivation = ENABLE;
    filtercfg.BankNumber = 0; 
 /*   
    filtercfg.FilterNumber = 0;
    filtercfg.FilterMode = CAN_FILTERMODE_IDMASK;
    filtercfg.FilterScale = CAN_FILTERSCALE_32BIT;
    filtercfg.FilterIdHigh = 0x0000;
    filtercfg.FilterIdLow = 0x0000;
    filtercfg.FilterMaskIdHigh = 0x0000;
    filtercfg.FilterMaskIdLow = 0x0000;
    filtercfg.FilterFIFOAssignment = 0;
    filtercfg.FilterActivation = ENABLE;
    filtercfg.BankNumber = 28;
 */   
    if(HAL_CAN_ConfigFilter(&hcan1, &filtercfg) != HAL_OK)
    {
     //   Error_Handler();
    }
    hcan1.pTxMsg->ExtId = FilterId;
    hcan1.pTxMsg->RTR = CAN_RTR_DATA;
    hcan1.pTxMsg->IDE = CAN_ID_EXT;
    hcan1.pTxMsg->DLC = 8;
    memset(hcan1.pTxMsg->Data,0x00,8);

	__HAL_CAN_ENABLE_IT(&hcan1,CAN_IT_FMP0);
	return RET_OK;
}

void Hal_CAN1_writer(uint32_t can_id, uint8_t *data)
{
  printf("@@@@@CAN1_Tx(%x)(0x%02X)\r\n",can_id, data[0]);
  hcan1.pTxMsg->ExtId = can_id;//0x0cecee00;
  hcan1.pTxMsg->RTR = CAN_RTR_DATA;
  hcan1.pTxMsg->IDE = CAN_ID_EXT;
  hcan1.pTxMsg->DLC = 8;
  memset(hcan1.pTxMsg->Data,0x00,8);
  memcpy(hcan1.pTxMsg->Data, data ,8);
  do
  {
    TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
  }
  while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );
	// __HAL_UNLOCK(&hcan1);	
}



