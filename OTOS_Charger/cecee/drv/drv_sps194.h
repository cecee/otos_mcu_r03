/*
  drv_sps194.h
*/
#ifndef DRV_SPS194_H_
#define DRV_SPS194_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
#include <stdint.h>

/****************************************************************************
*                                Defines
*****************************************************************************/
#define MAX_VOLTAGE		840  //80.0//68.0V
#define MAX_CURRENT		200//20.0A
#define STEP_VOLTAGE	5 //0.5V/10mS
#define STEP_CURRENT	1 //0.1A/10mS
#define ADC_FULL_SCALE	50 //5.0V *10

#define BATT_PRE_CHARGE_LOW	4500 
#define BATT_PRE_CHARGE_HIGH	7500 
#define BATT_CC_CHARGE_REF	8400 
#define BATT_CC_CHARGE_CURRENT 100 //10A 

#define STEP_KEEP	     0
#define STEP_NEXT	     1
#define STEP_QUIT	     2

enum {
	CC_MODE=0,
	CV_MODE,
	XX_MODE
};

/****************************************************************************
 *                               CHARGER STATUS STEP
*****************************************************************************/
 enum {
	STEP0_READY=0,
	STEP1_BALANCE_DISABLE,
	STEP2_BALANCE_ENABLE,
	STEP3_PRECHARGING,
	STEP4_CC_CHARGING,
	STEP5_CV_CHARGING,
	STEP6_,
	STEP7_,
	STEP8_,
	STEP9_,
	STEP10_,
	STEP16_
};

typedef struct tag_sps{
	uint32_t tick;	
	uint32_t process_cnt;
	uint16_t des_voltage;
	uint16_t des_current;
	uint16_t dac_voltage_value;
	uint16_t dac_current_value;
	uint16_t step_voltage;
	uint16_t step_current;
	uint8_t process_step;
}STATUS_SPS;

typedef struct tag_status_bit
{
    uint8_t charging: 1;
    uint8_t bms_err: 1;
    uint8_t charging_finish: 1;
    uint8_t batt_exist: 1;//x3
    uint8_t comm_fail: 1;
    uint8_t ac_fail: 1;
    uint8_t tmp_fail: 1;
    uint8_t fan_fail: 1;
    uint8_t dc_fail: 1;
    uint8_t load_balance: 1;	
    uint8_t relay: 1;	
    uint8_t dc_sd: 1;		
    uint8_t ctrl_fan: 1;//x12 //sps194 ������ FAN		
    uint8_t pBattIn: 1;//x13		
    uint8_t pBattOut: 1;//x14		
    uint8_t bms_enable: 1;//x15			
}SPS_STATUS_BIT;

typedef struct tag_sps_value
{
	uint16_t voltage_setting;
	uint16_t current_setting;  
	uint16_t voltage_outer;
	uint16_t voltage_sps;	
	uint16_t current_monitor;
	int16_t temperature_inner;//signed short
	int16_t rsev;//signed short
     uint8_t cid;
     uint8_t charging_seg;
     uint16_t  charging_time;
	uint16_t  cversion;
	SPS_STATUS_BIT b;
}SPS_VALUE;

/****************************************************************************
*                                Functions
*****************************************************************************/
int Drv_sps194_init(void);
void Drv_sps194_process(void);
int Drv_sps194_set_value(void);
//void Drv_sps194_writer_dac(uint16_t voltage, uint16_t current);
void Drv_sps194_writer_dac_value(uint16_t v_val, uint16_t i_val);

void SPS_Control(uint8_t *data);
void Drv_sps194_set_relay(uint8_t on);
void Drv_sps194_set_DcSD(uint8_t on);
void Drv_sps194_set_fan(uint8_t on);

//void Drv_sps194_set(void);
void Drv_sps194_step(void);
#endif