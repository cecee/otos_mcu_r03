#ifndef CECEE_H_
#define CECEE_H_

#include <stdint.h>
#include "cecee_comdef.h"
#include "d_cecee_util.h"
#include "../drv/Hvac_can.h"
#include "../drv/callback.h"
#include "../drv/Hvac_main.h"
#include "../drv/Hvac_gpio.h"
#include "../drv/Hvac_dht11.h"
#include "../drv/Hvac_power_detect.h"
#include "../hal/hal_can.h"
#include "../util/cbuffer.h"

#include "stm32f1xx_it.h"

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern ADC_HandleTypeDef hadc1;
extern UART_HandleTypeDef huart3;

extern uint8_t ONE_SEC_FLAG;
extern uint8_t CAN_RCV_FLAG;
extern uint8_t ONE_SEC_CNT;
extern uint8_t TEN_MIL_FLAG;
extern uint8_t HUN_MIL_FLAG;
extern uint8_t TEN_MIL_CNT;
extern uint8_t TICK_100M_CNT;
extern uint8_t CAN_TX_FLAG;

extern uint32_t gAdcValue[16];//adc
extern uint8_t gCh1[2];//uart

extern QUEUE qUart1;
extern QUEUE qUart3;
extern QUEUE qUart4;

extern uint16_t  t3_tick1;
extern uint16_t  t3_tick2;
extern uint16_t  t3_tick3;
extern uint8_t th_irq_cnt[3];
extern uint16_t th_irq_buf[3][60];
extern uint16_t th_start_tick;
extern uint8_t th_start;
//extern uint8_t th_ready;
extern uint8_t DHT_READY_FLAG;

extern STATUS gStatus;

#endif
