
#ifndef HAL_UART_H_
#define HAL_UART_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

/****************************************************************************
*                                Functions
*****************************************************************************/
int Hal_Uart_Init(void);
int Hal_Uart_DebugInit(void);
unsigned short Hal_Uart_SerialRxDataProcess(unsigned char rxdata);
//int Hal_Uart_BmsTxData(unsigned char *txdata, unsigned short txlen);
int Hal_Uart_ConsoleTxData(unsigned char *txdata, unsigned short txlen);

#endif /* HAL_UART_H_ */
