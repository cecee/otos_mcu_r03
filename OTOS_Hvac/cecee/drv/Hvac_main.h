/*
Hvac_main.h
*/
#ifndef HVAC_MAIN_H_
#define HVAC_MAIN_H_

#define TF_ALM_SET 70
#define TF_ALM_REL 60
#define TR_ALM_SET 70
#define TR_ALM_REL 60
#define VF_ON      35
#define VF_OFF     30
#define VR_ON      35
#define VR_OFF     30
#define Cn_HI_ON   40
#define Cn_HI_OFF  30
#define Cn_LO_ON   30
#define Cn_LO_OFF  25
#define Hn_ON      15
#define Hn_OFF     25
#define Hn_ALM_SET 80
#define Hn_ALM_REL 70

typedef struct _BIT_RELAY
{
    uint8_t ac_light: 1;
    uint8_t ac_heater1: 1;
    uint8_t ac_heater2: 1;
    uint8_t ac_heater3: 1;
}BIT_RELAY;

typedef struct _BIT_FAN
{
    uint8_t vf: 1;//r1 24v
    uint8_t vr: 1;//r2 24v
    uint8_t vs: 1;//r3 24v
    uint8_t c1: 1;//f1 12v
    uint8_t c2: 1;//f2 12v
    uint8_t c3: 1;//f3 12v
}BIT_FAN;


typedef struct _BIT_INPUT
{
    uint8_t ac_fail: 1;
    uint8_t dc_fail: 1;
    uint8_t bat_fail: 1;
    uint8_t ovc: 1;
    uint8_t door: 1;
    uint8_t slope1: 1;
    uint8_t slope2: 1;
    uint8_t shock: 1;    
}BIT_INPUT;


typedef struct _DHT11
{
    int16_t tmp;
    int16_t humi;
 }DHT11;

typedef struct _TMP36
{
    uint8_t tmp[4];
 }TMP36;

typedef struct _POWER_DETECT
{
    uint16_t v;
    uint16_t i;
    uint16_t p;
}POWER_DETECT;


typedef struct _HVAC_TMP
{
    uint8_t manual;
    uint8_t tf_alm_set;
    uint8_t tf_alm_rel;
    uint8_t tr_alm_set;
    uint8_t tr_alm_rel;
    uint8_t vf_on;
    uint8_t vf_off;
    uint8_t vr_on;
    uint8_t vr_off;
    uint8_t cn_hi_on;
    uint8_t cn_hi_off;
    uint8_t cn_lo_on;
    uint8_t cn_lo_off;
    uint8_t hn_on;
    uint8_t hn_off;
    uint8_t hn_alm_set;
    uint8_t hn_alm_rel;
}HVAC_TMP;

typedef struct _BIT_HEATER_ALARM
{
    uint8_t alarm_h1:1;
    uint8_t alarm_h2:1;
    uint8_t alarm_h3:1;
    uint8_t alarm_h4:1;
    uint8_t alarm_h5:1;
    uint8_t alarm_h6:1;
    uint8_t alarm_h7:1;
    uint8_t alarm_h8:1;
    uint8_t alarm_h9:1;
    uint8_t alarm_h10:1;
    uint8_t alarm_h11:1;
    uint8_t alarm_front:1;
    uint8_t alarm_rear:1;
    uint8_t alarm_heater:1;
}BIT_HEATER_ALARM;

typedef union _HEATER_ALARM
{
	uint16_t alarm;
	BIT_HEATER_ALARM b;
}HEATER_ALARM;

typedef struct _STATUS
{
    DHT11   dth[3];
    TMP36   tmp36[3]; 
    POWER_DETECT pwr;
    BIT_RELAY  bRelayCtrl;
    BIT_FAN    bFanCtrl;
    BIT_FAN    bFanAlarm;
    BIT_INPUT  bInput;
    //HVAC_TMP   hvacTmp;
    //HEATER_ALARM HeaterAlarm;
}STATUS;

void Drv_HvacInit();
void HVAC_TempControl();
void Drv_MessagePublic(uint8_t protocol, uint8_t *data, uint16_t length);
void Drv_MessageParse(uint8_t *data, uint16_t length);

void Drv_dbd_view(void);
//void Drv_dbd_TXprocess(uint8_t cid);
#endif