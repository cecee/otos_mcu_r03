/*
 * hal_uart.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "stm32f1xx_hal.h"
#include "usart.h"

#include "d_cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define HUART_TRACE(...)

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/



/*****************************************************************************
*                                Functions

/*****************************************************************************
*  Name:        Hal_Uart_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Uart_Init(void)
{
 	MX_USART_StartIT();
	//Hal_Uart_DebugInit();
  	//memset(bms_buf,0,sizeof(bms_buf));
   	//memset(console_buf,0,sizeof(console_buf));
   	// init_queue();
	return RET_OK;
}


/*****************************************************************************
*  Name:        Hal_Uart_SerialRxDataProcess()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Uart_TxtoRK(unsigned char *txdata, int txlen)
{
	HAL_StatusTypeDef state;
	if(txlen==0) return 0;
	state = HAL_UART_Transmit(&huart5, txdata, txlen, HAL_MAX_DELAY);
	if(state!=HAL_OK) return 1;
	else return 1;
}

