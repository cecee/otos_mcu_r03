#include "stm32f1xx_hal.h"
#include "Drv_dconsol.h"
#include "d_cecee.h"
//uint8_t update_ready;

BD_CONTROL BdCtrl;

void Drv_RK_Reset(void){
  HAL_GPIO_WritePin(GPIOB, PWR_EN_SYS_Pin,GPIO_PIN_SET); 
  HAL_Delay(10);
  HAL_GPIO_WritePin(GPIOB, RES_RKCPU_Pin,GPIO_PIN_RESET); 
 // HAL_GPIO_WritePin(GPIOB, PWR_EN_SYS_Pin,GPIO_PIN_RESET); 
  HAL_Delay(2);
  HAL_GPIO_WritePin(GPIOB, RES_RKCPU_Pin,GPIO_PIN_SET); 
  //HAL_GPIO_WritePin(GPIOB, PWR_EN_SYS_Pin,GPIO_PIN_SET); 
}

void Drv_NFC_Reset(void){
  HAL_GPIO_WritePin(GPIOC, nRST_NFC_Pin,GPIO_PIN_RESET); 
  HAL_Delay(2);
  HAL_GPIO_WritePin(GPIOC, nRST_NFC_Pin,GPIO_PIN_SET); 
}

void Drv_IOT_Reset(void){
  HAL_GPIO_WritePin(GPIOC, nRST_IOT_Pin,GPIO_PIN_RESET); 
  HAL_Delay(2);
  HAL_GPIO_WritePin(GPIOC, nRST_IOT_Pin,GPIO_PIN_SET); 
}

void Drv_MOD_Reset(void){
  HAL_GPIO_WritePin(GPIOC, nRST_MOD_Pin,GPIO_PIN_RESET); 
  HAL_Delay(2);
  HAL_GPIO_WritePin(GPIOC, nRST_MOD_Pin,GPIO_PIN_SET); 
}

void Drv_MOD_PWR(uint8_t on){
  if(on)  HAL_GPIO_WritePin(GPIOB, pM_PWR_Pin,GPIO_PIN_SET); 
  else  HAL_GPIO_WritePin(GPIOB, pM_PWR_Pin,GPIO_PIN_RESET); 
}

void Drv_IOT_PWR(uint8_t on){
  if(on)  HAL_GPIO_WritePin(GPIOE, pIOT_PWR_Pin,GPIO_PIN_SET); 
  else  HAL_GPIO_WritePin(GPIOE, pIOT_PWR_Pin,GPIO_PIN_RESET); 
}

void Drv_MessageParse(uint8_t *data, uint16_t length)
{
  uint8_t protocol =data[1];
//  uint8_t direction =data[2];
  uint32_t can_id=0;
  uint8_t can_data[8]={0,};
  switch(protocol)
  {
    case 0://can
      can_id= (data[8]<<24) + (data[7]<<16) +(data[6]<<8) +data[5];
      //printf("Drv_MessageParse can_id[%x]\r\n", can_id);
      if(can_id == 0xAAAAAAAA){
        printf("RECEIVED REBOOT COMMAND\r\n");
        Drv_RK_Reset();
      }
      memcpy(can_data,data+9,8);
      Drv_SendmsgToCbd(can_id, can_data);
      break;
    case 1://nfc
      break;  
    case 2: //other
      break;       
  }
  
  
}

void Drv_dbd_view(void){
   DBG(WARN,"==========================================================\n");
   DBG(MSG,"ac_heaterA[%d] ac_heaterA[%d]   ac_light[%d]   ac_monitor[%d]\n",db_up.dStatus.bRelay.ac_heaterA,db_up.dStatus.bRelay.ac_heaterB,db_up.dStatus.bRelay.ac_light,db_up.dStatus.bRelay.ac_monitor);
   DBG(MSG,"ctrlfan_f1[%d] ctrlfan_f2[%d] ctrlfan_f3[%d] ctrlfan_f4[%d]\n",db_up.dStatus.bFanCtrl.fan_f1,db_up.dStatus.bFanCtrl.fan_f2,db_up.dStatus.bFanCtrl.fan_f3,db_up.dStatus.bFanCtrl.fan_f4);
   DBG(MSG,"ctrlfan_r1[%d] ctrlfan_r2[%d] ctrlfan_r3[%d] ctrlfan_r4[%d]\n",db_up.dStatus.bFanCtrl.fan_r1,db_up.dStatus.bFanCtrl.fan_r2,db_up.dStatus.bFanCtrl.fan_r3,db_up.dStatus.bFanCtrl.fan_r4);
   DBG(MSG,"alarfan_f1[%d] alarfan_f2[%d] alarfan_f3[%d] alarfan_f4[%d]\n",db_up.dStatus.bFanAlarm.fan_f1,db_up.dStatus.bFanAlarm.fan_f2,db_up.dStatus.bFanAlarm.fan_f3,db_up.dStatus.bFanAlarm.fan_f4);
   DBG(MSG,"alarfan_r1[%d] alarfan_r2[%d] alarfan_r3[%d] alarfan_r4[%d]\n",db_up.dStatus.bFanAlarm.fan_r1,db_up.dStatus.bFanAlarm.fan_r2,db_up.dStatus.bFanAlarm.fan_r3,db_up.dStatus.bFanAlarm.fan_r4);
   DBG(MSG,"      dht1[%d]       dht2[%d]       dht3[%d]\n",db_up.dStatus.bSensor.dht1,db_up.dStatus.bSensor.dht2,db_up.dStatus.bSensor.dht3);
   DBG(MSG,"    shock1[%d]     shock2[%d]     slope1[%d]     slope2[%d]\n",db_up.dStatus.bSensor.shock1,db_up.dStatus.bSensor.shock2,db_up.dStatus.bSensor.slope1,db_up.dStatus.bSensor.slope2);
   DBG(MSG,"   ac_fail[%d]    dc_fail[%d]        ovc[%d]   bat_fail[%d]\n",db_up.dStatus.bPsuAlarm.ac_fail,db_up.dStatus.bPsuAlarm.dc_fail,db_up.dStatus.bPsuAlarm.ovc,db_up.dStatus.bPsuAlarm.buckup_bat_fail);
   DBG(MSG,"   Temper1[%d]    Temper2[%d]    Temper3[%d]  \n",db_up.dStatus.dht[0].temper,db_up.dStatus.dht[1].temper,db_up.dStatus.dht[2].temper);
   DBG(MSG,"   Humidi1[%d]    Humidi2[%d]    Humidi3[%d]  \n",db_up.dStatus.dht[0].humidi,db_up.dStatus.dht[1].humidi,db_up.dStatus.dht[2].humidi);
   DBG(MSG,"   RGB[%02X] [%02X] [%02X] [%02X] [%02X] [%02X]  \n",db_up.dStatus.buckRGB[0],db_up.dStatus.buckRGB[1],db_up.dStatus.buckRGB[2],db_up.dStatus.buckRGB[3],db_up.dStatus.buckRGB[4],db_up.dStatus.buckRGB[5]);
   DBG(MSG,"    Tmp36A[%d]    Tmp36B[%d]\n",db_up.dStatus.tmp36_0,db_up.dStatus.tmp36_1);
   DBG(MSG,"Cboard_Exist[%04X]\n",db_up.dStatus.cb_exist);
   DBG(MSG,"     CDS[%04X]    DIMMER[%04X]\n",db_up.dStatus.cds,db_up.dStatus.dimmer);
   DBG(MSG,"  main_V[%04d]    main_V[%04d] main_W[%04d]\n",db_up.dStatus.main_V,db_up.dStatus.main_A, db_up.dStatus.main_W);
   DBG(WARN,"==========================================================\n");
   DBG(MSG,"  0-voltage[%04d] 1-voltage[%04d] mood[%d]\n",BdCtrl.Ctrl_C[0].voltage_setting, BdCtrl.Ctrl_C[1].voltage_setting, BdCtrl.Ctrl_D.bCtrl.mood);
   
}

void Drv_dbd_cinfo(uint8_t cid)
{
	DBG(WARN,"==========================================================\n");
	DBG(MSG,"pack_voltage[%d] pack_current[%d]  pack_soc[%d]\n",CbData[cid].bms_value.pack_voltage,CbData[cid].bms_value.pack_current, CbData[cid].bms_value.pack_soc);

	
	DBG(WARN,"==========================================================\n");
}

/*
* Message send to RK
* STX|Protocol|Drection|HI Length|LO Length..
*/
void Drv_MessagePublic(uint8_t protocol, uint8_t *data, uint16_t length)
{
  uint8_t i;
  uint8_t sum=0;
  uint8_t buf[256]={0,};
  
  uint16_t sZ = length+7;
  
  //for(i=0;i<length;i++) printf("data [%03d][%X]\r\n",i,data[i]);
  buf[0]=0xFA;
  buf[1]=protocol;
  buf[2]=1;
  buf[3]= HI_UINT16(sZ);
  buf[4]= LO_UINT16(sZ);
  memcpy(&buf[5],data,length);
  buf[length+5]=0;
  buf[length+6]=0xFB;
  
  for(i=0;i<sZ;i++)sum=sum+buf[i];
  buf[length+5]=sum;
  Hal_Uart_TxtoRK(buf, sZ); 
  
  //if(protocol==CAN) DBG(WARN,"CAN seq[%02x] cid[%02x]\n", buf[5], buf[6]);
  //else for(i=0;i<sZ;i++) printf("Hal_Uart_TxtoRK [%03d][%X]\r\n",i,buf[i]);
 
}


void Drv_dbd_TXprocess(uint8_t cid)
{
  int i;
  uint8_t *data;
  uint8_t sum=0;
  uint16_t sz=0;
 // uint8_t test[16];
//update_ready=0;
 	db_up.sof='!';
  db_up.eof='@';
  db_up.size=sizeof(DB_UPLOAD);
  sz= db_up.size;
  
  data=(uint8_t *)malloc(sz+2);
  if(data==0){
    printf("[%d]malloc error!! \r\n",sz);
    return;
  }
 // printf("malloc --data[%x] sz[%d]\r\n",data,sz);
 // printf("BdCtrl --size[%d] \r\n",sizeof(BdCtrl));
 // printf("CB_UPLOAD --size[%d] \r\n",sizeof(CB_UPLOAD));
	//D_STATUS dStatus;
	//CB_UPLOAD CbData[1];
  
  
  for(i=0;i<sz;i++){
  	sum=(sum+data[i])&0xff;
    //printf("gCBUP --data[%d][%02X] \r\n",i, data[i]);
  } 
   db_up.crc=sum;
 //  db_up.dStatus.bRelay.ac_heaterA=1;
 //  db_up.dStatus.bRelay.ac_heaterB=1;
 //  db_up.dStatus.bFanCtrl.fan_f1=1;
 //  db_up.dStatus.bFanCtrl.fan_f4=1;
  // db_up.dStatus.bFanAlarm.fan_f2=0;
  // db_up.dStatus.bFanAlarm.fan_f3=0;
   
   //db_up.dStatus.bSensor.door=1;
   //db_up.dStatus.bSensor.dht3=1;
 //   DBG(WARN,"dht1[%d] dht2[%d] dht3[%d]\n",db_up.dStatus.bSensor.dht1,db_up.dStatus.bSensor.dht2,db_up.dStatus.bSensor.dht3);
    db_up.dStatus.buckRGB[0]=0xa0;
    db_up.dStatus.buckRGB[1]=0xa1;
    db_up.dStatus.buckRGB[2]=0xa2;
    db_up.dStatus.buckRGB[3]=0xa3;
    db_up.dStatus.buckRGB[4]=0xa4;
    db_up.dStatus.buckRGB[5]=0xa5;
//    db_up.dStatus.bSensor.dht2=1;
 //   db_up.dStatus.bSensor.dht3=1;
	db_up.dStatus.bPsuAlarm.buckup_bat_fail=0;
	db_up.dStatus.dimmer=4000;
	db_up.dStatus.main_W=3300;
	
	
	//CbData[0].bms_value.cell_votage[0]=1234;
	CbData[0].bms_value.pack_serial=0x1234;
	
	CbData[0].bms_value.pack_err.value[0]=0xe0;
	CbData[0].bms_value.pack_err.value[1]=0xe1;
	CbData[0].bms_value.pack_err.value[2]=0xe2;
	CbData[0].bms_value.pack_err.value[3]=0xe3;
	//CbData[0].bms_value.regacy_pack_err.value=0xefee;
	CbData[0].chg_value.voltage_setting=0xc001;
	
	//CbData[0].chg_value.b.fail_all=1;
	//CbData[0].sol_value.bSolCtrl.sol_type=1;
	//CbData[0].sol_value.bSolCtrl.bHTR=1;
	
	//CbData[0].sol_value.bSolPos.D1=0;
	//CbData[0].sol_value.bSolPos.D2=2;
	//CbData[0].sol_value.bSolPos.B1=3;
	//CbData[0].sol_value.bSolPos.B2=1;    
    
    memcpy(&db_up.CbData, &CbData[0], sizeof(db_up.CbData));
 //   printf("db_up.CbData --size[%d] \r\n",sizeof(db_up.CbData));
    memcpy(data,&db_up,sz);
    
  // data=(uint8_t *)&db_up; 
   //sum=db_up.crc;
   //printf("db_up.crc[%02X]\r\n",sum);
 //  printf(".......\r\n");
 //  printf("bFanAlarmF --[%d][%d][%d][%d] \r\n",db_up.dStatus.bFanAlarm.fan_f1,db_up.dStatus.bFanAlarm.fan_f2,db_up.dStatus.bFanAlarm.fan_f3,db_up.dStatus.bFanAlarm.fan_f4);
//   printf("bFanAlarmR--[%d][%d][%d][%d] \r\n",db_up.dStatus.bFanAlarm.fan_r1,db_up.dStatus.bFanAlarm.fan_r2,db_up.dStatus.bFanAlarm.fan_r3,db_up.dStatus.bFanAlarm.fan_r4);

// 	printf("sz[%d] data[0]->[%x][%x][%x][%x][%x][%x][%x]\r\n",sz,data[0],data[1],data[2],data[3],data[4],data[5],data[6]);
 //	printf("D_STATUS[%d] data[7]->[%x][%x][%x][%x][%x][%x][%x]\r\n",sizeof(D_STATUS),data[7],data[8],data[9],data[10],data[11],data[12],data[13]);
 //for(i=48;i<(48+32);i++)	printf("CbData[%d/%d] [%02X]\r\n",i,sizeof(db_up.CbData),data[i]);
	
#if 1
		Hal_Uart_TxtoRK(data, sz);
/*	
	test[0]=0x30;
		test[1]=0x31;
		test[2]=0x32;
		test[3]=0x33;
		test[4]=0x0a;
		test[5]=0x0d;
		Hal_Uart_TxtoM(test, 3);
  */
#endif
 	free(data);
//  update_ready=1;
}
