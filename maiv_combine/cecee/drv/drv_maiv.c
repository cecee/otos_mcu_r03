#include "stm32f1xx_hal.h"
#include "drv_maiv.h"
#include "cecee.h"
#include <stdlib.h>


MAIV_FLAG maiv_flag;  
void relay_on(uint8_t on);
void select_port(uint8_t port);
void maiv_batt_enable();

void maiv_init()
{
  uint32_t flash;
  maiv_batt_enable();
  
  memset(&maiv_flag, 0, sizeof(MAIV_FLAG));
  flash=read_flash_status();
  printf("maiv_init--flash [%08X]\r\n",flash);  
  //Drv_pixxi_BattVal(0);
  if(flash>=0x80){
    write_flash_status(0);
    relay_on(0);
  }
  else if((flash & 0xff)){
  	relay_on(1);
  }
  else{
  	relay_on(0);
  }

}

void maiv_main(void)
{
	// SOC below 30% LED OFF
  
	if(maiv_bms.exist[0]) HAL_GPIO_WritePin(GPIOC, pLED0_Pin,GPIO_PIN_RESET); 
	else HAL_GPIO_WritePin(GPIOC, pLED0_Pin,GPIO_PIN_SET);
	if(maiv_bms.exist[1]) HAL_GPIO_WritePin(GPIOC, pLED1_Pin,GPIO_PIN_RESET); 
	else HAL_GPIO_WritePin(GPIOC, pLED1_Pin,GPIO_PIN_SET); 
	if(maiv_bms.exist[2]) HAL_GPIO_WritePin(GPIOC, pLED2_Pin,GPIO_PIN_RESET); 
	else HAL_GPIO_WritePin(GPIOC, pLED2_Pin,GPIO_PIN_SET); 
	if(maiv_bms.exist[3]) HAL_GPIO_WritePin(GPIOC, pLED3_Pin,GPIO_PIN_RESET); 
	else HAL_GPIO_WritePin(GPIOC, pLED3_Pin,GPIO_PIN_SET);
#if 0 
	if(pixxi.sel_batt!=0){
		if(maiv_flag.relay_control_on==0){
			relay_on(1);
			//save EEPROM
		}
	}
	else {
		if(maiv_flag.relay_control_on!=0){
			relay_on(0);
			//save EEPROM
		}
	}
#endif	
	//Drv_pixxi_touch_get();
}

int bms_avr(){
  int sum=0;
  int avr;
  for(int i=0; i<4; i++){
    sum= sum + maiv_bms.soc[i];
    maiv_bms.exist[i]= (maiv_bms.soc[i]>30) ? 1:0;
  }
  avr= (sum*100) /400; //perent of total soc 
  return avr;
}

void maiv_sub(uint8_t seq){
  // printf("HAL_SYSTICK_Callback[%x]....\r\n",seq);
   int touch;
   int soc, avr;
   static int ex_avr=100;
   select_port(seq);
   //getBMS
   soc = Drv_BMS_GetSoc();
   maiv_bms.soc[seq]=soc;
   printf("batt [%x] soc[%d]....\r\n",seq, soc);
   //SOC average
   avr = bms_avr();
   if(ex_avr !=avr){
   	Drv_pixxi_BattVal(avr);
	ex_avr=avr;
   }
   //touch event
   touch = Drv_pixxi_touch_get();
   if(touch==1){
	printf("Press!![%x]....\r\n",touch);
	if(maiv_flag.relay_control_on){
		maiv_flag.relay_control_on=0;
	}
	else {
	  maiv_flag.relay_control_on=1;
	
	}
	//Drv_pixxi_selectBatt(maiv_flag.relay_control_on);
     relay_on(maiv_flag.relay_control_on);
   }

}


void maiv_batt_enable(){
  HAL_GPIO_WritePin(GPIOB, pBAT_EN0_Pin,GPIO_PIN_SET); 
  HAL_GPIO_WritePin(GPIOB, pBAT_EN1_Pin,GPIO_PIN_SET); 
  HAL_GPIO_WritePin(GPIOB, pBAT_EN2_Pin,GPIO_PIN_SET); 
  HAL_GPIO_WritePin(GPIOB, pBAT_EN3_Pin,GPIO_PIN_SET); 
}

void select_port(uint8_t port){
  switch(port){
	  case 0:
	    HAL_GPIO_WritePin(GPIOB, pSEL_A0_Pin,GPIO_PIN_RESET); 
	    HAL_GPIO_WritePin(GPIOB, pSEL_A1_Pin,GPIO_PIN_RESET); 
	    break;
	  case 1:
	    HAL_GPIO_WritePin(GPIOB, pSEL_A0_Pin,GPIO_PIN_SET); 
	    HAL_GPIO_WritePin(GPIOB, pSEL_A1_Pin,GPIO_PIN_RESET); 	    
	    break;   
	  case 2:
	    HAL_GPIO_WritePin(GPIOB, pSEL_A0_Pin,GPIO_PIN_RESET); 
	    HAL_GPIO_WritePin(GPIOB, pSEL_A1_Pin,GPIO_PIN_SET); 
	    break;
	  case 3:
	    HAL_GPIO_WritePin(GPIOB, pSEL_A0_Pin,GPIO_PIN_SET); 
	    HAL_GPIO_WritePin(GPIOB, pSEL_A1_Pin,GPIO_PIN_SET); 	    
	    break;      
  }
  HAL_Delay(2);
}

void relay_on(uint8_t on){
	maiv_flag.relay_control_on=on;
	Drv_pixxi_selectBatt(on);
 //pre relay  
	HAL_GPIO_WritePin(GPIOB, pRELAY1_Pin,GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, pRELAY2_Pin,GPIO_PIN_RESET); 
	HAL_Delay(10);
	if(on){
	 HAL_GPIO_WritePin(GPIOB, pRELAY1_Pin,GPIO_PIN_SET); 
	}
	else{
	 HAL_GPIO_WritePin(GPIOB, pRELAY2_Pin,GPIO_PIN_SET); 	 
	}

	HAL_Delay(20);
 //latch relay  
	HAL_GPIO_WritePin(GPIOB, pSET_0_Pin,GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, pRESET_0_Pin,GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, pSET_1_Pin,GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, pRESET_1_Pin,GPIO_PIN_RESET); 
  if(on){
 	HAL_GPIO_WritePin(GPIOB, pSET_0_Pin,GPIO_PIN_SET); 
     //HAL_GPIO_WritePin(GPIOB, pSET_1_Pin,GPIO_PIN_SET); 
	HAL_Delay(20);
     HAL_GPIO_WritePin(GPIOB, pSET_0_Pin,GPIO_PIN_RESET); 
     //HAL_GPIO_WritePin(GPIOB, pSET_1_Pin,GPIO_PIN_RESET); 
  }
  else{
    
   	//HAL_GPIO_WritePin(GPIOB, pRESET_0_Pin,GPIO_PIN_SET); 
     HAL_GPIO_WritePin(GPIOB, pRESET_1_Pin,GPIO_PIN_SET); 
	HAL_Delay(20);
     //HAL_GPIO_WritePin(GPIOB, pRESET_0_Pin,GPIO_PIN_RESET); 
     HAL_GPIO_WritePin(GPIOB, pRESET_1_Pin,GPIO_PIN_RESET);   
  }
  //save EEPROM
  write_flash_status(on);
}

