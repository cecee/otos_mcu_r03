#ifndef HVAC_GPIO_H_
#define HVAC_GPIO_H_

void Hvac_gpio_getport(void);
void Hvac_adcMux(uint8_t mux);


void CTRL_VFF(uint8_t on);
void CTRL_VFR(uint8_t on);
void CTRL_VFS(uint8_t on);
void CTRL_C1(uint8_t on);
void CTRL_C2(uint8_t on);
void CTRL_C3(uint8_t on);
void RELAY_Light(uint8_t on);
void RELAY_Heater1(uint8_t on);
void RELAY_Heater2(uint8_t on);
void RELAY_Heater3(uint8_t on);
void HVAC_Relay(uint8_t *data);
void HVAC_Fan(uint8_t *data);
#endif