/*
 * hal_adc.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "hal_adc.h"
#include "cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/
extern ADC_HandleTypeDef hadc1;
extern DAC_HandleTypeDef hdac;

/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Hal_ADC1_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/

int Hal_ADC_Init(void)
{
	Hal_Adc1_Init();
	return RET_OK;
}

/*****************************************************************************
*  Name:        Hal_Adc1_Init()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Adc1_Init(void)
{
	HAL_ADC_Start(&hadc1);
	Adc1_start();
	return RET_OK;
}

int Adc1_start(void)
{
	HAL_ADC_Start_DMA(&hadc1, gAdcValue, 16);
	return RET_OK;
}

int DAC_set_voltage( uint16_t value)
{
  HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, value);
  return RET_OK;
}

int DAC_set_current(uint16_t value)
{
  HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, value);
  return RET_OK;
}