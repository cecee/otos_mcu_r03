#ifndef HAL_ADC_H_
#define HAL_ADC_H_

int Hal_ADC_Init(void);
int Adc1_start(void);
int DAC_set_dimmer( uint16_t value);
void ADC_ConvTemperature(uint16_t ch0, uint16_t ch1, uint16_t cds);
#endif