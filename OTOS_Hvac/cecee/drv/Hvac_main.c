#include "stm32f1xx_hal.h"
#include "Hvac_main.h"
#include "extern_hvac.h"

STATUS gStatus;

void Drv_HvacInit()
{
  init_dth11();
  memset(&gStatus, 0,sizeof(gStatus));
  RELAY_Light(0);
  RELAY_Heater1(0);
  RELAY_Heater2(0);
  RELAY_Heater3(0); 
  CTRL_VFF(0);
  CTRL_VFR(0);
  CTRL_VFS(0);
  CTRL_C1(0);
  CTRL_C2(0);
  CTRL_C3(0);
  
 }


void HVAC_TempControl()
{
  int16_t tfh, tfl;
  int16_t trh;
  if(gStatus.dth[0].humi == -999 &&gStatus.dth[1].humi == -999 && gStatus.dth[2].humi == -999) return;
  if(gStatus.dth[0].humi == 0 &&gStatus.dth[1].humi == 0 && gStatus.dth[2].humi == 0) return;

#ifdef  HVAC_DEBUG 
   DBG(WARN,"dth[%03d/%03d][%03d/%03d][%03d/%03d]\r\n", gStatus.dth[0].tmp ,gStatus.dth[0].humi, gStatus.dth[1].tmp ,gStatus.dth[1].humi,gStatus.dth[2].tmp ,gStatus.dth[2].humi);
#endif

  if(gStatus.dth[0].tmp >= gStatus.dth[1].tmp){
    tfh=gStatus.dth[0].tmp/10;
    tfl=gStatus.dth[1].tmp/10;
  }
  else{
    tfh=gStatus.dth[1].tmp/10;
    tfl=gStatus.dth[0].tmp/10;  
  }
  trh = gStatus.dth[2].tmp;
}


void Drv_MessageParse(uint8_t *data, uint16_t length)
{
  uint8_t protocol =data[1];
//  uint8_t direction =data[2];
  uint32_t can_id=0;
  uint8_t can_data[8]={0,};
  switch(protocol)
  {
    case 0://can
      can_id= (data[8]<<24) + (data[7]<<16) +(data[6]<<8) +data[5];
      memcpy(can_data,data+9,8);
      //Drv_SendmsgToCbd(can_id, can_data);
      break;
    case 1://nfc
      break;  
    case 2: //other
      break;       
  }
}

void Drv_dbd_view(void){
  
   DBG(WARN,"==========================================================\n");
 /*
   DBG(MSG,"ac_heaterA[%d] ac_heaterA[%d]   ac_light[%d]   ac_monitor[%d]\n",db_up.dStatus.bRelay.ac_heaterA,db_up.dStatus.bRelay.ac_heaterB,db_up.dStatus.bRelay.ac_light,db_up.dStatus.bRelay.ac_monitor);
   DBG(MSG,"ctrlfan_f1[%d] ctrlfan_f2[%d] ctrlfan_f3[%d] ctrlfan_f4[%d]\n",db_up.dStatus.bFanCtrl.fan_f1,db_up.dStatus.bFanCtrl.fan_f2,db_up.dStatus.bFanCtrl.fan_f3,db_up.dStatus.bFanCtrl.fan_f4);
   DBG(MSG,"ctrlfan_r1[%d] ctrlfan_r2[%d] ctrlfan_r3[%d] ctrlfan_r4[%d]\n",db_up.dStatus.bFanCtrl.fan_r1,db_up.dStatus.bFanCtrl.fan_r2,db_up.dStatus.bFanCtrl.fan_r3,db_up.dStatus.bFanCtrl.fan_r4);
   DBG(MSG,"alarfan_f1[%d] alarfan_f2[%d] alarfan_f3[%d] alarfan_f4[%d]\n",db_up.dStatus.bFanAlarm.fan_f1,db_up.dStatus.bFanAlarm.fan_f2,db_up.dStatus.bFanAlarm.fan_f3,db_up.dStatus.bFanAlarm.fan_f4);
   DBG(MSG,"alarfan_r1[%d] alarfan_r2[%d] alarfan_r3[%d] alarfan_r4[%d]\n",db_up.dStatus.bFanAlarm.fan_r1,db_up.dStatus.bFanAlarm.fan_r2,db_up.dStatus.bFanAlarm.fan_r3,db_up.dStatus.bFanAlarm.fan_r4);
   DBG(MSG,"      dht1[%d]       dht2[%d]       dht3[%d]\n",db_up.dStatus.bSensor.dht1,db_up.dStatus.bSensor.dht2,db_up.dStatus.bSensor.dht3);
   DBG(MSG,"    shock1[%d]     shock2[%d]     slope1[%d]     slope2[%d]\n",db_up.dStatus.bSensor.shock1,db_up.dStatus.bSensor.shock2,db_up.dStatus.bSensor.slope1,db_up.dStatus.bSensor.slope2);
   DBG(MSG,"   ac_fail[%d]    dc_fail[%d]        ovc[%d]   bat_fail[%d]\n",db_up.dStatus.bPsuAlarm.ac_fail,db_up.dStatus.bPsuAlarm.dc_fail,db_up.dStatus.bPsuAlarm.ovc,db_up.dStatus.bPsuAlarm.buckup_bat_fail);
   DBG(MSG,"   Temper1[%d]    Temper2[%d]    Temper3[%d]  \n",db_up.dStatus.dht[0].temper,db_up.dStatus.dht[1].temper,db_up.dStatus.dht[2].temper);
   DBG(MSG,"   Humidi1[%d]    Humidi2[%d]    Humidi3[%d]  \n",db_up.dStatus.dht[0].humidi,db_up.dStatus.dht[1].humidi,db_up.dStatus.dht[2].humidi);
   DBG(MSG,"   RGB[%02X] [%02X] [%02X] [%02X] [%02X] [%02X]  \n",db_up.dStatus.buckRGB[0],db_up.dStatus.buckRGB[1],db_up.dStatus.buckRGB[2],db_up.dStatus.buckRGB[3],db_up.dStatus.buckRGB[4],db_up.dStatus.buckRGB[5]);
   DBG(MSG,"    Tmp36A[%d]    Tmp36B[%d]\n",db_up.dStatus.tmp36_0,db_up.dStatus.tmp36_1);
   DBG(MSG,"Cboard_Exist[%04X]\n",db_up.dStatus.cb_exist);
   DBG(MSG,"     CDS[%04X]    DIMMER[%04X]\n",db_up.dStatus.cds,db_up.dStatus.dimmer);
   DBG(MSG,"  main_V[%04d]    main_V[%04d] main_W[%04d]\n",db_up.dStatus.main_V,db_up.dStatus.main_A, db_up.dStatus.main_W);
   DBG(WARN,"==========================================================\n");
   //DBG(MSG,"  0-voltage[%04d] 1-voltage[%04d] mood[%d]\n",BdCtrl.Ctrl_C[0].voltage_setting, BdCtrl.Ctrl_C[1].voltage_setting, BdCtrl.Ctrl_D.bCtrl.mood);
 */  
}

/*
* Message send to RK
* STX|Protocol|Drection|HI Length|LO Length..
*/
void Drv_MessagePublic(uint8_t protocol, uint8_t *data, uint16_t length)
{
  uint8_t i;
  uint8_t sum=0;
  uint8_t buf[256]={0,};
  
  uint16_t sZ = length+7;
  
  //for(i=0;i<length;i++) printf("data [%03d][%X]\r\n",i,data[i]);
  buf[0]=0xFA;
  buf[1]=protocol;
  buf[2]=1;
  buf[3]= HI_UINT16(sZ);
  buf[4]= LO_UINT16(sZ);
  memcpy(&buf[5],data,length);
  buf[length+5]=0;
  buf[length+6]=0xFB;
  
  for(i=0;i<sZ;i++)sum=sum+buf[i];
  buf[length+5]=sum;

}



