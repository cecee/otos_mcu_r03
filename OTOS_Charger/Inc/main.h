/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define pCOM_TEST_Pin GPIO_PIN_5
#define pCOM_TEST_GPIO_Port GPIOE
#define pCOM_START_Pin GPIO_PIN_6
#define pCOM_START_GPIO_Port GPIOE
#define DIP0_Pin GPIO_PIN_0
#define DIP0_GPIO_Port GPIOC
#define DIP1_Pin GPIO_PIN_1
#define DIP1_GPIO_Port GPIOC
#define DIP2_Pin GPIO_PIN_2
#define DIP2_GPIO_Port GPIOC
#define DIP3_Pin GPIO_PIN_3
#define DIP3_GPIO_Port GPIOC
#define pBAT_ENABLE_Pin GPIO_PIN_0
#define pBAT_ENABLE_GPIO_Port GPIOB
#define pBATT_OUT_Pin GPIO_PIN_1
#define pBATT_OUT_GPIO_Port GPIOB
#define pBATT_IN_Pin GPIO_PIN_2
#define pBATT_IN_GPIO_Port GPIOB
#define SOL_D1p_Pin GPIO_PIN_7
#define SOL_D1p_GPIO_Port GPIOE
#define SOL_D1m_Pin GPIO_PIN_8
#define SOL_D1m_GPIO_Port GPIOE
#define SOL_D2p_Pin GPIO_PIN_9
#define SOL_D2p_GPIO_Port GPIOE
#define SOL_D2m_Pin GPIO_PIN_10
#define SOL_D2m_GPIO_Port GPIOE
#define SOL_B1p_Pin GPIO_PIN_11
#define SOL_B1p_GPIO_Port GPIOE
#define SOL_B1m_Pin GPIO_PIN_12
#define SOL_B1m_GPIO_Port GPIOE
#define SOL_B2p_Pin GPIO_PIN_13
#define SOL_B2p_GPIO_Port GPIOE
#define SOL_B2m_Pin GPIO_PIN_14
#define SOL_B2m_GPIO_Port GPIOE
#define pRY_SET_Pin GPIO_PIN_10
#define pRY_SET_GPIO_Port GPIOB
#define pRY_RESET_Pin GPIO_PIN_11
#define pRY_RESET_GPIO_Port GPIOB
#define CS_FND_Pin GPIO_PIN_12
#define CS_FND_GPIO_Port GPIOB
#define pSOL_DET0_Pin GPIO_PIN_8
#define pSOL_DET0_GPIO_Port GPIOD
#define pSOL_DET1_Pin GPIO_PIN_9
#define pSOL_DET1_GPIO_Port GPIOD
#define pSOL_DET2_Pin GPIO_PIN_10
#define pSOL_DET2_GPIO_Port GPIOD
#define pSOL_DET3_Pin GPIO_PIN_11
#define pSOL_DET3_GPIO_Port GPIOD
#define pSOL_DET4_Pin GPIO_PIN_12
#define pSOL_DET4_GPIO_Port GPIOD
#define pSOL_DET5_Pin GPIO_PIN_13
#define pSOL_DET5_GPIO_Port GPIOD
#define pSOL_DET6_Pin GPIO_PIN_14
#define pSOL_DET6_GPIO_Port GPIOD
#define pSOL_DET7_Pin GPIO_PIN_15
#define pSOL_DET7_GPIO_Port GPIOD
#define LED_PWM1_Pin GPIO_PIN_6
#define LED_PWM1_GPIO_Port GPIOC
#define LED_PWM2_Pin GPIO_PIN_7
#define LED_PWM2_GPIO_Port GPIOC
#define LED_PWM3_Pin GPIO_PIN_8
#define LED_PWM3_GPIO_Port GPIOC
#define LED_OE_Pin GPIO_PIN_9
#define LED_OE_GPIO_Port GPIOC
#define TXEN_Pin GPIO_PIN_8
#define TXEN_GPIO_Port GPIOA
#define NFC_TXD_Pin GPIO_PIN_12
#define NFC_TXD_GPIO_Port GPIOC
#define NFC_NRST_Pin GPIO_PIN_0
#define NFC_NRST_GPIO_Port GPIOD
#define NFC_RXD_Pin GPIO_PIN_2
#define NFC_RXD_GPIO_Port GPIOD
#define RESV_DOUT_Pin GPIO_PIN_4
#define RESV_DOUT_GPIO_Port GPIOD
#define RESV_DIN_Pin GPIO_PIN_5
#define RESV_DIN_GPIO_Port GPIOD
#define RL_CONT_Pin GPIO_PIN_6
#define RL_CONT_GPIO_Port GPIOD
#define FAN_CONT_Pin GPIO_PIN_7
#define FAN_CONT_GPIO_Port GPIOD
#define CS_FLASH_Pin GPIO_PIN_6
#define CS_FLASH_GPIO_Port GPIOB
#define FAN_FAIL_Pin GPIO_PIN_8
#define FAN_FAIL_GPIO_Port GPIOB
#define DC_SD_Pin GPIO_PIN_9
#define DC_SD_GPIO_Port GPIOB
#define AC_FAIL_Pin GPIO_PIN_0
#define AC_FAIL_GPIO_Port GPIOE
#define TEMP_FAIL_Pin GPIO_PIN_1
#define TEMP_FAIL_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
