#ifndef DRV_MAIV_H_
#define DRV_MAIV_H_
#include <stdint.h>

typedef struct _MAIV_FLAG
{
	uint8_t seq;
	uint8_t relay_control_on;
}MAIV_FLAG;

void maiv_init(void);
void maiv_main(void);
void maiv_sub(uint8_t seq);

#endif