/*

 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stm32f1xx_hal.h"
//#include "stm32f10x_flash.h"
#include "stm32f1xx_hal_flash.h"

#include "hal_flash.h"
#include "cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define StartAddr ((u32)0x0807F800)
//#define EndAddr ((u32)0x0807F8FF)

//#define StartAddr ((uint32_t)0x0801F800)
//#define EndAddr ((uint32_t)0x0801F8FF)

//#define StartAddr ((u32) 0x0800FC00)
//#define EndAddr ((u32) 0x0800FCFF)
/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                                Functions
******************************************************************************/
uint32_t StartAddr=0x0800FC00;

void flash_test_read()
{
 
    // 구조체 읽기:
    TEST_STRUCT t;

    memcpy( &t, (TEST_STRUCT* ) StartAddr, sizeof( t ));
 
    TEST_STRUCT x;
    memcpy( &x, &t, sizeof(t));
printf("a[%x]\r\n",x.a);
    // 이제 t.a, t.b, t.c 에 값이 들어있음.
}


void flash_test_write()
{
     // 구조체 쓰기
/*
  TEST_STRUCT w;
 
    w.a = 1;
    w.b = 2;
    w.c = 3;
   
 
    unsigned int * p_w = (unsigned int *)&w;
    int byte_align  = 0;
    if( (sizeof(w) % 4) > 0)
        byte_align = 1;  // 구조체가 32비트단위가 아닌경우 마지막 떨거지들에 대한처리.

    FLASH_Unlock();
    FLASH_ErasePage(StartAddr);

    for( int i=0; i< sizeof( w ) / 4 + byte_align ; i++)
    {
        FLASH_ProgramWord(StartAddr + (i*4) ,  p_w[i]  );
    }
    FLASH_Lock();

    */
    HAL_FLASH_Unlock();

    FLASH_PageErase( StartAddr );
    HAL_FLASH_Program( FLASH_TYPEPROGRAM_WORD, StartAddr, 1234);
    HAL_FLASH_Lock();
}

