/*
  drv_bms.h
*/
#ifndef DRV_BMS_H_
#define DRV_BMS_H_
#include <stdint.h>


#define	GET_CELL_VOLTAGE	 0xA1
#define	GET_CELL_TEMPER 	0xA2
#define	GET_PACK_VOL_CUR	0xA3
#define	PBA_CONTROL_COM	0xA4
#define	GET_ALARM_STATUS	0xA5
#define	GET_INFOR_SOC		0xB1
#define	GET_INFOR_CYCLE	0xB2
#define	SET_CLEAR_EEPROM	0xB3
#define	GET_CELL_BALANCING_STATUS	 0xA6
#define	SET_CELL_CONTROL		0xA8
#define	GET_PARAMETER		0xF1
#define	SET_PARAMETER		0xF2
	
#define	RTN_CELL_VOLTAGE 0x51
#define	RTN_CELL_TEMPER   0x52
#define	RTN_PACK_VOL_CUR   0x53
#define	RTN_ALARM_STATUS   0x55

#define	RTN_INFOR_SOC   0x61
#define	RTN_INFOR_CYCLE	0x71
#define	RTN_CELL_BALANCING_STATUS		0x56


typedef struct _MAIV_BMS_SOC{
	uint8_t soc[4];
	uint8_t exist[4];
	uint8_t avr;
}MAIV_BMS_SOC;


typedef struct  _BMS_ERR_BIT
{
	uint32_t uv_warning: 1;
	uint32_t uv_error: 1;
	uint32_t ov_warning: 1;		
	uint32_t ov_error: 1;
	uint32_t ot_warning: 1;
	uint32_t ot_error: 1;
	uint32_t ut_warning: 1;		
	uint32_t ut_error: 1;	
			
	uint32_t oc_warning_chg: 1;		
	uint32_t oc_error_chg: 1;     
	uint32_t oc_warning_dis: 1;
	uint32_t oc_error_dis: 1;	
	uint32_t comm_error: 1;	
	uint32_t by1bit5: 1;		
	uint32_t by1bit6: 1;		
	uint32_t by1bit7: 1;
		
	uint32_t pack_uv_warning: 1;		
	uint32_t pack_uv_protect: 1;     
	uint32_t pack_ov_warning: 1;
	uint32_t pack_ov_protect: 1;	
	uint32_t by2bit4: 1;	
	uint32_t by2bit5: 1;		
	uint32_t by2bit6: 1;		
	uint32_t by2bit7: 1;	
				
	uint32_t by3bit0: 1;		
	uint32_t by3bit1: 1;     
	uint32_t by3bit2: 1;
	uint32_t by3bit3: 1;	
	uint32_t by3bit4: 1;	
	uint32_t by3bit5: 1;		
	uint32_t warning: 1;		
	uint32_t error: 1;	
}BMS_ERR_BIT;



typedef union tag_bms_error_union
{
	uint8_t value[4];
	BMS_ERR_BIT b;
}BMS_ERR;

typedef union _CELL_BAL
{
	uint8_t value[4];
}CELL_BAL;

typedef union _USER_DATA
{
	uint8_t value[3];
}USER_DATA;

typedef union _PACK_COUNTER
{
	uint8_t value[6];
}PACK_COUNTER;

typedef struct tag_bms_value
{
  uint16_t cell_votage[20];//cell_votage[16];
  int16_t cell_temper[6];//"-"온도 있을수 있음
  uint16_t pack_voltage;
  int16_t pack_current; //"-"값 있음 전류방향에 따라서
  uint16_t pack_soc;//new: 2byte  old:1byte
  int16_t pack_temper;//(58)통신[60]MSB
  uint8_t pack_serial;//추가[62]
  uint8_t pack_parallel;//추가
  uint8_t pack_capacity;
  uint8_t pack_year;
  uint8_t pack_month;
  uint8_t pack_day;
  uint16_t pack_nation;
  uint16_t pack_serialNumber;//(68)통신[70-lsb][71-msb]
  uint16_t xxpack_cycle;//[72-lsb][73-msb]
  USER_DATA pack_userData;//[74-lsb][76-msb]	
  PACK_COUNTER pack_Counter;//[77]-[82]
  CELL_BAL pack_cellBancing;//83(MSB) .. 86(LSB)//추가	
  BMS_ERR pack_err; //[90-[93]
}BMS_VALUE;


/****************************************************************************
*                               Includes
*****************************************************************************/
#include "cecee.h"

/****************************************************************************
*                               function
*****************************************************************************/

void Drv_bms_init(void);
void Drv_bms_enable(uint8_t enable);
void Drv_bms_restart(void);

int  Drv_BMS_GetSoc(void);



//void Drv_bms_request(void);
void Drv_bms_station_request(void);
void Drv_bms_regacy_request(void);

//void Drv_bms_rcv(void);
void Drv_bms_get( uint8_t command,  uint8_t *data, uint8_t length, uint8_t flg);
void Drv_bms_add_confirm(uint8_t add);
//void Drv_bms_monitor(void);
void Drv_bms_cell_balance_control(uint8_t balance_on, uint16_t balance_voltage);
void put_bmsStationData(const uint8_t *data);
#endif