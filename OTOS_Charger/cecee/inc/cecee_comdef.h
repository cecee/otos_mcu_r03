/*
 * cecee_comdef.h
 *
 */ 
 /*
 * Define : Debug Message
 */
#ifndef CECEE_COMDEF_H_
#define CECEE_COMDEF_H_

#include "time.h"
#include "string.h"




#if 0

/*
* Maximum string size 
*/
#define MAX_SIZE_DEV_ID				16
#define MAX_SIZE_IMEI				15
#define MAX_SIZE_URL				128
#define MAX_SIZE_FOTA_ID			32
#define MAX_SIZE_FOTA_PW			32
#define MAX_SIZE_ADDR				128
#define MAX_SIZE_MSG				80
#define MAX_SIZE_SMS_PW				8
#define MAX_SIZE_SMS_SERIAL			16
#define MAX_SIZE_PHONE_NUM			41	// 3GPP �԰�
#define MAX_STR_SIZE_SMS_TEXT		640	// 3GPP �԰�
// Parsing tools
#define MAX_SIZE_PARSING_PARAM		32
#define MAX_CNT_PARSING_PARAM		10

/*
* Minimum string size 
*/
#define MIN_SIZE_SMS_PW				4

/************************************************************/
// define Enum
/************************************************************/

/************************************************************/
// define Structure
/*
* Type Define : MessageQ command
*/
typedef struct MSG_
{
  unsigned int	Cmd;
  unsigned int	Msg[4];
} MSG_t;

#pragma pack(1)
/*
* String Parsing tools
*/
typedef struct _tParsing{
	unsigned char pcnt;
	char param[MAX_CNT_PARSING_PARAM][MAX_SIZE_PARSING_PARAM];
} tParsing;

#pragma pack()

#endif

#endif /* CECEE_COMDEF_H_ */

