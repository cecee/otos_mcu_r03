/*
 * cecee_util.h
 *
 */

#ifndef CECEE_UTIL_H_
#define CECEE_UTIL_H_

#include "../util/dbg.h"

#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define set_bit(p,n) ((p) |= (1 << (n)))
#define clear_bit(p,n) ((p) &= (~(1) << (n)))           
#endif /* cecee_UTIL_H_ */
