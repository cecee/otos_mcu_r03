/*
 * cecee_cfg.h
 */ 
#define SUPPORT					1
#define NOT_SUPPORT				0

 
   
/*
* Define : Device Name
*/



#if 0

/*
* Define : Debug Level
*/
#define DSP_NO_DEUBUG			0
#define DSP_DEUBUG_WARN			1
#define DSP_DEUBUG_ERR			2
#define	DSP_DEUBUG_MSG			3

//#define DSPONE_DEBUG_LVL 		DSP_DEUBUG_MSG


/*
 * Define : Timezone
 */
#define TIMEZONE_VNM			28	// Vietnam (+7)
#define TIMEZONE_KOR			36	// Korea (+9)
#define DEV_TIMEZONE			TIMEZONE_KOR	// Korea (+9)

/*
* Define : Protocol Type
*/
//#define PROTOCOL_TYPE_ORI_EMOTO		0
//#define PROTOCOL_TYPE_GSM_TRACKER	1
//#define PROTOCOL_TYPE				PROTOCOL_TYPE_GSM_TRACKER

/*
* Define : Maximum size of tcp send buffer
*/
#if(PROTOCOL_TYPE==PROTOCOL_TYPE_GSM_TRACKER)
#define MAX_SIZE_TCP_SEND_BUF		400
#elif(PROTOCOL_TYPE==PROTOCOL_TYPE_ORI_EMOTO)
#define MAX_SIZE_TCP_SEND_BUF		350
#endif //#if(PROTOCOL_TYPE_GSM_TRACKER==PROTOCOL_TYPE_ORI_EMOTO)

/*
 * Define : Server Information
 */
#if(PROTOCOL_TYPE==PROTOCOL_TYPE_GSM_TRACKER)
#define EMOTO_SERVER_IP			""
#define EMOTO_SERVER_PORT		0
#elif(PROTOCOL_TYPE==PROTOCOL_TYPE_ORI_EMOTO)
#define EMOTO_SERVER_IP			""
#define EMOTO_SERVER_PORT		0
#endif //#if(PROTOCOL_TYPE_GSM_TRACKER==PROTOCOL_TYPE_ORI_EMOTO)

/*
 * Define : NTP Server Information
 */
//#define NTP_SERVER_DNS			"pool.ntp.org"
//#define NTP_SERVER_PORT			123

/*
 * Define : GPS XTRA Information
 */
//#define GPS_XTRA_SERVER_URL		"http://xtrapath1.izatcloud.net/xtra2.bin"
//#define GPS_XTRA_FILE_PATH		"RAM:xtra2.bin"

/*
 * Define : TCP Connection Information
 */
//#define TCP_CON_INFO_CONTEXT_TYPE_IPV4		1
//#define TCP_CON_INFO_APN_NAME				"web.sktelecom.com"
//#define TCP_CON_INFO_USER_NAME				""
//#define TCP_CON_INFO_PASSWORD				""
//#define TCP_CON_INFO_AUTH_NONE				0
//#define TCP_CON_INFO_AUTH_PAP				1
//#define TCP_CON_INFO_AUTH_CHAP				2

/*
* Define : 3x Accelerometer
*/
//#define I2C_CON						0
//#define SPI_CON						1
//#define ACC3X_DEFAULT_GLEVEL		3
//#define ACC3X_CONTROL_IF			I2C_CON

/*
* Define : Internal EEP ROM
*/
#define INTERNAL_EEPROM				NOT_SUPPORT

/*
* Define : SMS Protocol whether or not to use
*/
//#define SMS_PROTOCOL_USAGE			SET_OFF

/*
* Define : GPS AUTO Start
*/
//#define GPS_AUTO_START_FLAG			SET_ON

/*
 * Define : EMOTO Protocol Device ID
 */
//#define E_PROTOCOL_DEV_ID			0

#endif