#include "stm32f1xx_hal.h"
#include "drv_pixxi.h"
#include "cecee.h"

/****************************************************************************
 *                                Defines
 *****************************************************************************/
 #define BMS_SERCH_TIMEOUT 100*5  //5sec
 
/*****************************************************************************
*                           Global Variables
******************************************************************************/
STATUS_PIXXI pixxi;
extern UART_HandleTypeDef huart2;
long TimeLimit4D=5;
int pixxi_command(unsigned char *txdata, unsigned short len);
int pixxi_set_touch_region(void);
int pixxi_touch_set(void);
int pixxi_GetAck(void);
int gfx_Cls(void);
int gfx_RectangleFilled(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);
int gfx_LedDigit(uint16_t x, uint16_t y, uint16_t size, uint16_t oncolor, uint16_t offcolor, uint16_t value);
int gfx_putstr(uint8_t *str, uint16_t line, uint16_t column ,uint16_t mul, uint16_t FGcolour, uint16_t BGcolour);

int Drv_pixxi_init(void)
{
	printf("pixxi init...\r\n");
	//reset pixxi
	//HAL_GPIO_WritePin(GPIOB, pPX_RESET_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, pPX_RESET_Pin,GPIO_PIN_RESET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOB, pPX_RESET_Pin,GPIO_PIN_SET);
	memset(&pixxi, 0, sizeof(STATUS_PIXXI));
	HAL_Delay(10000);
	
	gfx_Cls();
	gfx_RectangleFilled(0,0,240,240,BLACK);//07e0 
	gfx_RectangleFilled(0,180,240,240,RED);//07e0 	
	gfx_putstr("MaiV",3,10, 3, WHITE, BLACK);
	gfx_putstr("%",3,7, 3, SKYBLUE, BLACK);
	
	//Drv_pixxi_BattVal(100,0);
	Drv_pixxi_selectBatt(0);
	pixxi_touch_set();
	pixxi_set_touch_region();
	return RET_OK;
}


int Drv_pixxi_BattVal(uint8_t percent)
{
	uint8_t d100;
	uint8_t d10;
	uint8_t d1;
	gfx_RectangleFilled(0,80,160,170,BLACK);//07e0 
	d100=percent/100;
	d10=percent/10;
	d1=percent%10;
	if(d100){
	  	gfx_LedDigit(20, 100, 3, SKYBLUE, BLACK,1);
		gfx_LedDigit(70, 100, 3, SKYBLUE, BLACK,0);
		gfx_LedDigit(120, 100, 3, SKYBLUE, BLACK,0);
	}
	else{
		gfx_LedDigit(70, 100, 3, SKYBLUE, BLACK,d10);
		gfx_LedDigit(120, 100, 3, SKYBLUE, BLACK,d1);
	}
}


int Drv_pixxi_selectBatt(uint8_t select)
{
  	printf("Drv_pixxi_selectBatt[%d]\r\n",select);
	if(select) {//aux
   		gfx_RectangleFilled(0,180,240,240,STEELBLUE);//07e0 
		gfx_putstr("Aux",8,6, 2, WHITE,STEELBLUE);
	}
	else {//main
 		gfx_RectangleFilled(0,180,240,240,RED);//07e0 
		gfx_putstr("Main",8,6, 2, WHITE,RED);
	}
}



int Drv_pixxi_touch_get(void){
//cmd(MSB), cmd(LSB), mode(MSB), mode(LSB)
//0xFF, 0x37, 0x00, 0x01  
  uint8_t data[4]={0xFF, 0x37, 0x00, 0x00};
  uint8_t len=sizeof(data);
  HAL_StatusTypeDef state;
  int read=0 ;
  int size;
  unsigned char buffer[32];
  unsigned long timeA ;
   
  pixxi_command(data, len);
  
  timeA = HAL_GetTick();
  memset(&buffer,0,sizeof(buffer));
  while ((read != 1) && ((HAL_GetTick() - timeA) < TimeLimit4D))
  {
    state = HAL_UART_Receive(&huart2,buffer,10,50);
    if(state==HAL_OK) read = 1 ;
  }

  size = strlen(buffer);
 		
  if (buffer[0] != 6)
  {
    return  RET_ERR; 
  }
  //printf("Drv_pixxi_touch_get[%d]-[%x][%x][%x][%x]\r\n",size,buffer[0],buffer[1],buffer[2],buffer[3]);
  return buffer[2];
}


int pixxi_command(unsigned char *txdata, unsigned short len)
{
	HAL_StatusTypeDef state;
	if(len==0) return RET_ERR;
	state = HAL_UART_Transmit(&huart2, txdata, len, HAL_MAX_DELAY);
	if(state!=HAL_OK) return RET_ERR;
	else return RET_OK;
}

int gfx_Cls(){
  uint8_t data[10]={0xFF, 0xCD};
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;

}

int pixxi_set_touch_region(void){
//Byte Stream:
//cmd(MSB), cmd(LSB), line(MSB), line(LSB), column(MSB), column(LSB)
//0xFF, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x00, 0x64 
  uint16_t x0,x1;
  uint16_t y0,y1;
  uint8_t data[10];
  x0=0; y0=0;
  x1=240, y1=240;
    
  data[0]=0xFF;
  data[1]=0x39;
  data[2]=HI_UINT16(x0);
  data[3]=LO_UINT16(x0);
  data[4]=HI_UINT16(y0);
  data[5]=LO_UINT16(y0); 
  data[6]=HI_UINT16(x1);
  data[7]=LO_UINT16(x1);
  data[8]=HI_UINT16(y1);
  data[9]=LO_UINT16(y1); 
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;  
}


int pixxi_touch_set(void){
//cmd(MSB), cmd(LSB), mode(MSB), mode(LSB)
//0xFF, 0x38, 0x00, 0x00  
  uint8_t data[4]={0xFF, 0x38, 0x00, 0x00};
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;
}


int pixxi_GetAck(void)
{
  HAL_StatusTypeDef state;
  int read ;
  int size;
  unsigned char buffer[32];
  unsigned long timeA ;

  read    = 0 ;
 // HAL_StatusTypeDef HAL_UART_Receive(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);
  timeA = HAL_GetTick();
  memset(&buffer,0,sizeof(buffer));
 #if 1   
  while ((read != 1) && ((HAL_GetTick() - timeA) < TimeLimit4D))
  {
    state = HAL_UART_Receive(&huart2,buffer,10,50);
    if(state==HAL_OK) read = 1 ;
  }
#endif  
  size = strlen(buffer);
  //printf("HAL_UART_Receive[%d]-[%x][%x][%x][%x]\r\n",size,buffer[0],buffer[1],buffer[2],buffer[3]);
		
  if (read == 0)
  {
    //Error4D = RET_ERR ;
    return  RET_ERR; 
  }
  else if (buffer[0] != 6)
  {
    return  RET_ERR; 
  }
		
  return  RET_OK; 
}


int gfx_RectangleFilled(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color){
//Byte Stream:
//cmd(MSB), cmd(LSB), x1(MSB), x1(LSB), y1(MSB), y1(LSB), x2(MSB), x2(LSB), y2(MSB), y2(LSB), colour(MSB), colour(LSB)
//0xFF, 0xC4, 0x00, 0x32, 0x00, 0x3C, 0x00, 0x5A, 0x00, 0x64, 0x07, 0xE0
  uint8_t data[12];//={0xFF, 0xC4, 0x00, 0x32, 0x00, 0x3C, 0x00, 0x5A, 0x00, 0x64, 0x07, 0xE0};
  data[0]=0xFF;
  data[1]=0xC4;
  data[2]=HI_UINT16(x0);
  data[3]=LO_UINT16(x0);
  data[4]=HI_UINT16(y0);
  data[5]=LO_UINT16(y0); 
  data[6]=HI_UINT16(x1);
  data[7]=LO_UINT16(x1);
  data[8]=HI_UINT16(y1);
  data[9]=LO_UINT16(y1); 
  data[10]=HI_UINT16(color);
  data[11]=LO_UINT16(color); 
  
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
 // HAL_Delay(100);
  pixxi_GetAck();
  return RET_OK;

}

int gfx_LedDigit(uint16_t x, uint16_t y, uint16_t size, uint16_t oncolor, uint16_t offcolor, uint16_t value)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), x(MSB), x(LSB), y(MSB), y(LSB), digitSize(MSB), digitSize(LSB), onColour(MSB), onColour (LSB), offColour(MSB), offColour (LSB), value(MSB), value(LSB)
//0xFE, 0xC1, 0x00, 0x0A, 0x00, 0x0A, 0x00, 0x01, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x09
  uint8_t data[14];
  data[0]=0xFE;
  data[1]=0xC1;
  data[2]=HI_UINT16(x);
  data[3]=LO_UINT16(x);
  data[4]=HI_UINT16(y);
  data[5]=LO_UINT16(y); 
  data[6]=HI_UINT16(size);
  data[7]=LO_UINT16(size);
  data[8]=HI_UINT16(oncolor);
  data[9]=LO_UINT16(oncolor); 
  data[10]=HI_UINT16(offcolor);
  data[11]=LO_UINT16(offcolor); 
  data[12]=HI_UINT16(value);
  data[13]=LO_UINT16(value); 
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
 // HAL_Delay(100);
  pixxi_GetAck();
  return RET_OK;
}


int gfx_txt_MoveCursor(uint16_t line, uint16_t column)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), line(MSB), line(LSB), column(MSB), column(LSB)
//0xFF, 0xE9, 0x00, 0x05, 0x00, 0x03
  uint8_t data[6];
  data[0]=0xFF;
  data[1]=0xE9;
  data[2]=HI_UINT16(line);
  data[3]=LO_UINT16(line);
  data[4]=HI_UINT16(column);
  data[5]=LO_UINT16(column); 
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;
}

int gfx_txt_FGcolour(uint16_t color)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), colour(MSB), colour(LSB)
//0xFF, 0xE7, 0x00, 0x10
  uint8_t data[4];
  data[0]=0xFF;
  data[1]=0xE7;
  data[2]=HI_UINT16(color);
  data[3]=LO_UINT16(color);
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;
}

int gfx_txt_BGcolour(uint16_t color)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), colour(MSB), colour(LSB)
//0xFF, 0xE6, 0xF8, 0x00
  uint8_t data[4];
  data[0]=0xFF;
  data[1]=0xE6;
  data[2]=HI_UINT16(color);
  data[3]=LO_UINT16(color);
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;
}

int gfx_txt_Width(uint16_t multiple)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), multiplier(MSB), multiplier (LSB)
//0xFF, 0xE4, 0x00, 0x05
  uint8_t data[4];
  data[0]=0xFF;
  data[1]=0xE4;
  data[2]=HI_UINT16(multiple);
  data[3]=LO_UINT16(multiple);
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;
}

int gfx_txt_Height(uint16_t multiple)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), multiplier(MSB), multiplier (LSB)
//0xFF, 0xE3, 0x00, 0x02
  uint8_t data[4];
  data[0]=0xFF;
  data[1]=0xE3;
  data[2]=HI_UINT16(multiple);
  data[3]=LO_UINT16(multiple);
  uint8_t len=sizeof(data);
  pixxi_command(data, len);
  pixxi_GetAck();
  return RET_OK;
}



int gfx_putstr(uint8_t *str, uint16_t line, uint16_t column ,uint16_t mul, uint16_t FGcolour, uint16_t BGcolour)
{
//Byte Stream:
//cmd(MSB), cmd(LSB), char0, char1, char2, ��, charN, NULL
//0x00, 0x18, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x00
  uint8_t len;
  uint8_t data[32]={0,};
  len = strlen(str);
  
  gfx_txt_MoveCursor(line, column);
  gfx_txt_FGcolour(FGcolour);
  gfx_txt_BGcolour(BGcolour);
  gfx_txt_Width(mul);
  gfx_txt_Height(mul);
  
  memcpy(&data[2],str,len);
  data[0]=0x00;
  data[1]=0x18;

  //printf("gfx_putstr[%d]-[%x][%x][%x][%x]-[%x][%x][%x][%x]\r\n",len,data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
  //uint8_t len=sizeof(data);
  pixxi_command(data, len+3);
  pixxi_GetAck();
  return RET_OK;
}






