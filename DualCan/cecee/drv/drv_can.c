//#include "main.h"
#include "stm32f1xx_hal.h"
#include "drv_can.h"
#include "cecee.h"


extern CAN_HandleTypeDef hcan1;
extern uint8_t TransmitMailbox1;

CBD_CTRL_CMD cbd_ctrl_from_dbd;

uint8_t can1_rxbuf[8];

int Drv_can1_init(void)
{
	Hal_CAN_Init();
	return RET_OK;
}


void Drv_can1_request(uint8_t what)
{
  uint32_t reqId;
  uint8_t can_buf[8];
  reqId = cbd.my_snd_id + 0xAA;
  switch(what){
  	case 0:
		memset(can_buf,0xAA,8);
		break;
  	case 1:
		memset(can_buf,0,8);
		can_buf[1]=gCBUP.chg_value.b.batt_exist;
		can_buf[2]=HI_UINT16(gCBUP.bms_value.pack_soc);
		can_buf[3]=LO_UINT16(gCBUP.bms_value.pack_soc);
		can_buf[4]=HI_UINT16(gCBUP.bms_value.pack_temper);
		can_buf[5]=LO_UINT16(gCBUP.bms_value.pack_temper);
		break;		
  }
  can_buf[0]=what;
  Drv_can1_transmit(reqId, can_buf); 
}

int Drv_can1_rcv(void)
{

  	uint32_t ExtId, snd_id;
  	uint8_t i,sZ;
  	uint8_t cmd;
  	uint8_t block_cnt,remain;
  	uint8_t block_buf[8];
  	uint8_t _buf[160];
     uint16_t voltage, current;
     
  	//printf("Drv_can1_rcv  myboard_id[%x] \r\n", myboard_id);
  	ExtId=hcan1.pRxMsg->ExtId;
  	memcpy(can1_rxbuf,hcan1.pRxMsg->Data,8);
    
  	//printf("Drv_can1_rcv  ExtId[%x] cbd.my_recv_id[%x]\r\n", ExtId, cbd.my_recv_id);
 
	if(cbd.my_recv_id !=ExtId) return 0;	
			
     Drv_led_set(LED_CAN, 50, 0);//rcv can1 indicate
	cmd=ExtId & 0x0f;
#ifdef  CAN_UP_DEBUG
  	printf("CAN1_FromMASTER [0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x]  \r\n",
            can1_rxbuf[0], can1_rxbuf[1], can1_rxbuf[2], can1_rxbuf[3], 
            can1_rxbuf[4], can1_rxbuf[5], can1_rxbuf[6], can1_rxbuf[7] 
            );
#endif    
     cmd = can1_rxbuf[0];
 	switch(cmd)
	{
		case 0x00://request
			sZ=sizeof(gCBUP);
			remain=sZ%8;
			block_cnt=sZ/8;
			block_cnt= (remain!=0)? (block_cnt+1) :(block_cnt);
           // for(i=0;i<20;i++)gCBUP.bms_value.cell_votage[i]=i;
           // gCBUP.rgb=0xAABBCCDD;
            	memcpy(&_buf[0],&gCBUP, sZ ); 
  		  for(i=0;i<block_cnt;i++){
                snd_id=cbd.my_snd_id +i;
                memset(&block_buf[0],0,8);
                memcpy(&block_buf[0], &_buf[i*8], 8);
#ifdef  CAN_UP_DEBUG
  		printf("Datag id[%x] =[%02x][%02x][%02x][%02x]-[%02x][%02x][%02x][%02x]\r\n",
          snd_id,block_buf[0],block_buf[1],block_buf[2],block_buf[3],block_buf[4],block_buf[5],block_buf[6],block_buf[7]);
#endif      
			 Drv_can1_transmit(snd_id, block_buf); 
                HAL_Delay(5);
			}          
			break;
          case 0x01://charging control
#ifdef  LOGOUT		  
              printf("Drv_can1_rcv.. charging \r\n");
#endif
              CB_ChargingStart(can1_rxbuf);
               break;
          case 0x02://Lock control
              //printf("Drv_can1_rcv.. Lock control what[%d]\r\n", can1_rxbuf[2]);
              LOCK_Control(can1_rxbuf);
              //Drv_solenoid_Control(can1_rxbuf[2]);
               break; 
           case 0x03://RGB control
              //Drv_SetRGB(can1_rxbuf[1], can1_rxbuf[2], can1_rxbuf[3], can1_rxbuf[4]);
              Drv_SetRGB(can1_rxbuf);
              break;              
           case 0x06://SPS control
              SPS_Control(can1_rxbuf);
             // Drv_sps194_set_fan(can1_rxbuf[1]);
              break;             
		default:
			break;
	}

	return 1;
}


void Drv_can1_transmit(uint32_t can_id, uint8_t *data)
{
 	hcan1.pTxMsg->ExtId=can_id;
 	memcpy(hcan1.pTxMsg->Data, data ,8);
 	do
	{
		TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
	}
	while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );
}