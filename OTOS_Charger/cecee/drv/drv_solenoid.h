/*
  drv_solenoid.h
*/
#ifndef DRV_SOLENOID_H_
#define DRV_SOLENOID_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
 *                               SOLENOID STATUS
*****************************************************************************/

#define _LOCK    1
#define _UNLOCK  0
#define _CW   0
#define _CCW  1

#define DOOR_LOCK   0
#define BATT_LOCK   1

#define DOOR_RELEASE_TIME 16//10//8 //5 10ms
#define BATT_RELEASE_TIME 3//3 // 10ms
#define SOLENOID_TIMEOUT 200     // dlock 1sec100�� ȸ��

#define BS_READY  0x00
#define BS_LOCK   0x01
#define BS_UNLOCK 0x02
#define BS_ING    0x03

#define DS_READY  0x00
#define DS_LOCK   0x02
#define DS_UNLOCK 0x01
#define DS_ING    0x03
/*
#define DS_READY  0x00
#define DS_LOCK   0x02
#define DS_UNLOCK 0x01
#define DS_ING    0x03
*/
///MOTOR 0,2 Door
///MOTOR 1,3 Batt
typedef struct _BIT_MOTOR_STATUS{
	uint8_t D1_ctrl: 1;
	uint8_t B1_ctrl: 1;
	uint8_t D2_ctrl: 1;
	uint8_t B2_ctrl: 1;
 	uint8_t D1_err: 1;
	uint8_t B1_err: 1;
	uint8_t D2_err: 1;
	uint8_t B2_err: 1;
 	uint8_t D1_pos: 2;
	uint8_t B1_pos: 2;
	uint8_t D2_pos: 2;
	uint8_t B2_pos: 2;    
}BIT_MOTOR_STATUS;

typedef struct _BIT_MOTOR{
	uint8_t ctrl: 1;
	uint8_t ready: 1;
	uint8_t pos: 2;
}BIT_MOTOR;

typedef struct _MOTOR{
  BIT_MOTOR b;
}MOTOR;


void Drv_solenoid_tick(void);
void Drv_solenoid_init(void);
void Drv_solenoid_get_pos(void);
void Drv_solenoid_rotate(uint8_t pos, uint8_t ccw);
void Drv_solenoid_set(uint8_t mot, uint8_t lock);
void Drv_solenoid_release(void);
void Drv_solenoid_stop(void);
void Drv_solenoid_Control(uint8_t what);
void LOCK_Control(uint8_t *data);
#endif