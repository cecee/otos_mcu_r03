/*
Drv_dht11.h
*/
#ifndef DRV_DHT11_H_
#define DRV_DHT11_H_

void init_dth11(void);
void Drv_DhtStartInterrup(void);
void Drv_DhtReadValue(void);
#endif