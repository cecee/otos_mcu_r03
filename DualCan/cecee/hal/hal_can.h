#ifndef HAL_CAN_H_
#define HAL_CAN_H_


typedef struct  _BMS_ACTIVE
{
	uint8_t bmsActive: 1;
	uint8_t bmsReady: 1;		
	uint8_t bmsCellBalancingState: 1;
	uint8_t bmsChargingCompleted: 1;
	uint8_t bmsForceChargingStop: 1;
	uint8_t bmsWarning: 1;		
	uint8_t bmsFault: 1;
	uint8_t bmsRelayControlStatus: 1;
}BMS_ACTIVE;	

typedef union _UBMS_ACTIVE
{
	uint8_t active;
	BMS_ACTIVE b;
}UBMS_ACTIVE;

typedef struct  _BMS_101
{
	uint8_t bmsHeaterBeat;
	uint8_t bmsProtocolVersion;
	UBMS_ACTIVE uBmsActive;
	uint8_t bmsSOC;
	uint16_t bmsRequestCurrent;
	uint8_t bmsTemperature;
	uint8_t bms07;
}BMS_101;


typedef struct  _BMS_102
{
	uint16_t bmsMaximumBatteryVoltage;
	int16_t bmsMaximumBatteryCurrent;
	uint16_t bmsPresentVoltage;
	int16_t bmsPresentCurrent;
}BMS_102;


typedef struct  _bCHARING_CONDITION
{
	uint8_t chargingCondition: 1;
	uint8_t speedLimit: 1;		
	uint8_t bit2: 1;
	uint8_t bit3: 1;
	uint8_t bit4: 1;
	uint8_t bit5: 1;		
	uint8_t bit6: 1;
	uint8_t bit7: 1;
}bCHARING_CONDITION;

typedef union _UCHARING_CONDITION
{
	uint8_t byteCondition;
	bCHARING_CONDITION b;
}UCHARING_CONDITION;

typedef struct  _TX_A0A0
{
	uint8_t version;
	UCHARING_CONDITION condition;
	uint16_t packVoltage;
	int16_t packCurrent;
	uint8_t soc;
	int8_t temp;
}TX_A0A0;


typedef struct  _RELAY_FAIL
{
	uint8_t dischargingRelayFail: 1;
	uint8_t chargingRelayFail: 1;		
	uint8_t bit2: 1;
	uint8_t bit3: 1;
	uint8_t bit4: 1;
	uint8_t bit5: 1;		
	uint8_t bit6: 1;
	uint8_t bit7: 1;
}RELAY_FAIL;

typedef struct  _TX_A0A1
{
	uint8_t byte0;
	uint8_t overVoltage;
	RELAY_FAIL relayFail;
	uint8_t byte3;
	uint8_t byte4;
	uint8_t byte5;
	uint8_t byte6;
	uint8_t byte7;
}TX_A0A1;


int Hal_CAN1_Init(void);
int Hal_CAN2_Init(void);

void Hal_CAN1_writer(uint32_t can_id, uint8_t *data);
void BMS_Transmit(uint32_t std_id);
void canTransTestScooter(uint32_t id);
int bms_recv(void);
#endif