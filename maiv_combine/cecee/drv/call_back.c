#include "stm32f1xx_it.h"
#include "call_back.h"
#include "cecee.h"

uint8_t bms_check_tick=0;
uint32_t _100msTick=0;
uint8_t ONE_SEC_FLAG=0;
uint8_t _10msTimer1=0;
uint8_t _100msTimer2=0;

extern uint8_t bms_buf[128];
extern uint8_t bms_data_cnt;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{

  if(htim->Instance==TIM1){//TIM1 10ms
#if 0     
	if(_10msTimer1==0)_10msTimer1=1;
//TEN_MIL_FLAG=1;
    Drv_sps194_process();
    Drv_led_release();
    Drv_solenoid_release();  
#endif
  }
  else if(htim->Instance==TIM2){//TIM2 100ms

#if 0 
    period_100ms();
    if(bms_check_tick++ > 10){//1sec
      bms_check_tick=0;
      CB_BattCheck();
    }
    else if(_100msTick%10==0) {

    }
#endif   
  }  

}


void period_100ms(void)
{

}

#define BMS_START   0x02
#define BMS_EOF     0x03
uint8_t bms_packet=0;

void GetSerialBuf(uint8_t d)
{
  if(bms_packet==0)
  {
   if(d==BMS_START){
     memset(bms_buf,0,sizeof(bms_buf));
     bms_data_cnt=0;
     bms_packet=1;
#ifdef BMS_DBUG	
 //printf("find bms_data_cnt[%d][%02x]\r\n",bms_data_cnt, d);
#endif          
   }
 }
 if(bms_packet){
  // printf("find bms_data_cnt[%d][%02x]\r\n",bms_data_cnt, d);
   bms_buf[bms_data_cnt++]=d;
   if(bms_data_cnt==86 && d==BMS_EOF) 
   {
#ifdef BMS_DBUG	
 //printf("====>find bms_data_cnt[%d][%02x]\r\n",bms_data_cnt, d);
#endif 
      //Drv_led_set(LED_RS485, 10, 0);//rcv can1 indicate blink
      put_bmsStationData(bms_buf);
      memset(bms_buf,0,sizeof(bms_buf));
      bms_packet=0;
    }
 }
 if(bms_data_cnt>88){
    memset(bms_buf,0,sizeof(bms_buf));
    bms_packet=0;
 }       
}

