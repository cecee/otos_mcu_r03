#include "main.h"
#include "stm32f1xx_hal.h"
#include "drv_cboard.h"
#include "cecee.h"
#include "dac.h"
#include <stdlib.h>

uint8_t gConsole_serch_flag=1;
uint8_t charger_ctrl_mode=1;
uint16_t VBexist;
uint32_t gAdcValue[16];
uint8_t bms_frame=0;
uint8_t fsz=0xff;

uint16_t LOCK_TRY_CNT=0;
uint16_t LOCK_ERR_CNT=0;
uint16_t LOCK_ERR_VAL=0;

CB_UPLOAD gCBUP;  
CBOARD cbd;

void CB_Init()
{
  CB_GetID();
  Hal_Uart_Init();
  Hal_Spi_Init();
  Hal_CAN_Init();
  
  CB_CommSelect(1);//select 485
  Drv_sps194_set_relay(0); //relay off 
  Drv_bms_init();//bms init
  Drv_solenoid_init();//solenoid init
  Drv_Sflash_Init();
  Drv_as1107_init();
  Drv_sps194_init();
  
  CB_ADC_DAC_init();
  Drv_bms_enable(1); 

}

void CB_ADC_DAC_init(void)
{
  Hal_ADC_Init();
  HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
  HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
}

void CB_GetID(void)
{
   uint8_t dip_value=0;
   if( HAL_GPIO_ReadPin(GPIOC, DIP0_Pin))  dip_value=dip_value|0x01; else dip_value=dip_value&~0x01;
   if( HAL_GPIO_ReadPin(GPIOC, DIP1_Pin))  dip_value=dip_value|0x02; else dip_value=dip_value&~0x02;
   if( HAL_GPIO_ReadPin(GPIOC, DIP2_Pin))  dip_value=dip_value|0x04; else dip_value=dip_value&~0x04;
   if( HAL_GPIO_ReadPin(GPIOC, DIP3_Pin))  dip_value=dip_value|0x08; else dip_value=dip_value&~0x08;
   cbd.id=dip_value&0xFF;
   gCBUP.chg_value.cid=dip_value & 0x0F;
   gCBUP.chg_value.cversion = FW_VERSION;
   cbd.my_recv_id = CAN_RCV_ID + cbd.id;
   cbd.my_snd_id  = CAN_SND_ID + ((cbd.id<<8)&0xff00);  
}

void CB_BattCheck(void)
{
  
//  Drv_bms_enable(1);
//1 sec 주기로 동작  
   static uint8_t bms_enable_tick=0;
   static uint8_t exStatus=0;
   
    VBexist =( 2500 * (uint16_t)gAdcValue[3] )/4095;//10mV

    if(VBexist<500)gCBUP.chg_value.b.bms_enable=0;
    else if (VBexist > 500 && VBexist<1500) gCBUP.chg_value.b.bms_enable=1;
    gCBUP.chg_value.b.batt_exist = (VBexist > NO_BATT_VOLTAGE) ? 0 : 1;
    gCBUP.chg_value.b.comm_fail = (gBmsRequestCnt>3) ? 1 : 0;
    gCBUP.chg_value.b.pBattIn = HAL_GPIO_ReadPin(GPIOB, pBATT_IN_Pin)&0x01;
    gCBUP.chg_value.b.pBattOut= HAL_GPIO_ReadPin(GPIOB, pBATT_OUT_Pin)&0x01;
    
    if(gCBUP.chg_value.b.comm_fail || (gCBUP.chg_value.b.batt_exist==0)) memset(&gCBUP.bms_value,0,sizeof(gCBUP.bms_value));
#ifdef LOGOUT  
    printf("VBexist[%d][%d] [%d][%d]- SQ[%d] SS[%d] Cfinish[%d] pBattOut[%d] pos[%d][%d][%d][%d]-R[%d]\r\n", 
        VBexist, gCBUP.chg_value.b.batt_exist, gCBUP.chg_value.voltage_sps, gCBUP.chg_value.current_monitor, 
        gCBUP.chg_value.charging_seg, gCBUP.chg_value.charging_time, gCBUP.chg_value.b.charging_finish,
        gCBUP.chg_value.b.pBattOut, motor[0].b.pos, motor[1].b.pos, motor[2].b.pos, motor[3].b.pos, gCBUP.chg_value.b.relay );
#endif    
    if(gCBUP.chg_value.b.batt_exist) {
      Drv_led_set(LED_BATT,0xff,0);
       if(gCBUP.chg_value.b.comm_fail){
	    if(bms_enable_tick==0){
	    		Drv_bms_enable(0);
	    }
	    else if(bms_enable_tick>3){
	    		bms_enable_tick=0;
			Drv_bms_enable(1); 
	    }
  		bms_enable_tick++;
      }
	 
    }
    else {
	 Drv_led_set(LED_BATT,0x00,0); 
	 bms_enable_tick=0;
    }
    //comm_fail으로 밧데리 들어온거로 인식함  
    if(exStatus != gCBUP.chg_value.b.batt_exist){
    		exStatus = gCBUP.chg_value.b.batt_exist;
		Drv_can1_request(1);
    }
//   if(gCBUP.bLockStatus.D1_ctrl) Drv_led_set(LED_LOCK,0xff,0);
//    else Drv_led_set(LED_LOCK,0x00,0); 

    Drv_bms_station_request();  
 }

void CB_CommSelect(uint8_t select)
{
  HAL_GPIO_WritePin(GPIOB, pRY_SET_Pin,GPIO_PIN_RESET);   
  HAL_GPIO_WritePin(GPIOB, pRY_RESET_Pin,GPIO_PIN_RESET);  
  if(select){
    HAL_GPIO_WritePin(GPIOB, pRY_SET_Pin,GPIO_PIN_SET); 
    HAL_Delay(10);
    HAL_GPIO_WritePin(GPIOB, pRY_SET_Pin,GPIO_PIN_RESET); 	
  }
  else 
  {
    HAL_GPIO_WritePin(GPIOB, pRY_RESET_Pin,GPIO_PIN_SET); 
    HAL_Delay(10);
    HAL_GPIO_WritePin(GPIOB, pRY_RESET_Pin,GPIO_PIN_RESET); 	  
  }
}


void CB_ChargingStart(uint8_t *data)
{
	uint8_t batt_err=0;
	uint8_t cmd_charging = data[1]&0x01;
	gCBUP.chg_value.voltage_setting = BUILD_UINT16(data[3], data[2]);//lo,hi순서
	gCBUP.chg_value.current_setting = BUILD_UINT16(data[5], data[4]);//lo,hi순서
 
#ifdef FUNCTION_CHARGE_TEST  
	 cmd_charging =1;
	gCBUP.chg_value.voltage_setting =840;
	gCBUP.chg_value.current_setting = 100;
	gCBUP.chg_value.b.charging = cmd_charging;
	gCBUP.chg_value.charging_time=0;
	gCBUP.chg_value.b.charging_finish=0; 
	Drv_led_set(LED_CHGR,0xff,0);//LED_CHGR ON
	gCBUP.chg_value.charging_seg = 1; 
	Drv_sps194_set_relay(1); 
#else
	if(gCBUP.chg_value.b.charging == cmd_charging) return;
	gCBUP.chg_value.b.charging = cmd_charging;
	gCBUP.chg_value.charging_time=0;
	//gCBUP.chg_value.charging_seg = STEP0_READY;   
	if(cmd_charging && gCBUP.chg_value.b.batt_exist) {
		gCBUP.chg_value.b.charging_finish=0;      
		Drv_led_set(LED_CHGR,0xff,0);//LED_CHGR ON
		Drv_sps194_set_relay(1); 
  		batt_err = (gCBUP.bms_value.pack_voltage < BATT_PRE_CHARGE_LOW) ? 1:0;
		gCBUP.chg_value.b.bms_err = batt_err;
		if(batt_err)  {
		 	//Drv_sps194_set_relay(0);  
			Drv_led_set(LED_CHGR,50,1);//LED_CHGR BLINK
		}
	}
	else{ //charging stop
		Drv_led_set(LED_CHGR,0,0);//LED_CHGR OFF
		gCBUP.chg_value.b.charging_finish=1;
		Drv_sps194_set_relay(0);
	}
#endif	
	printf("CB_ChargingStart--p[%d] c[%d]-[%d][%d]\r\n",gCBUP.chg_value.b.charging, cmd_charging,gCBUP.chg_value.voltage_setting, gCBUP.chg_value.current_setting);
	
	Drv_sps194_set_value();//0xfff-->68.8V
}

#ifdef FUNCTION_MOTOR_TEST  
  uint8_t lock_val=0;
  uint8_t lock2=0,lock4=0;
  uint8_t exkey2=0,exkey4=0;
  
  void CB_MotorTest(void)  
  {
  gCBUP.chg_value.b.pBattIn = HAL_GPIO_ReadPin(GPIOB, pBATT_IN_Pin)&0x01;
  gCBUP.chg_value.b.pBattOut= HAL_GPIO_ReadPin(GPIOB, pBATT_OUT_Pin)&0x01;
  //printf("pBattIn[%d] pBattOut[%d]\r\n",gCBUP.chg_value.b.pBattIn, gCBUP.chg_value.b.pBattOut);
   if(exkey2 && gCBUP.chg_value.b.pBattOut==0) {
    exkey2=0;
    lock2++;
    lock2 %=2;
    if(lock2) lock_val = lock_val|0x02;
    else lock_val = lock_val&0xFD;
    //printf("lock2[%d]lock_val[%x] \r\n",lock2, lock_val);
    Drv_solenoid_Control(lock_val);
  }
  else if(exkey2==0 && gCBUP.chg_value.b.pBattOut){
   exkey2=1;
  }
  
  if(exkey4 && gCBUP.chg_value.b.pBattIn==0) {
    exkey4=0;
    lock4++;
    lock4 %=2;
    if(lock4) lock_val = lock_val|0x08;
    else lock_val = lock_val&0xF7;
   // printf("lock2[%d]lock_val[%x] \r\n",lock4, lock_val);
    Drv_solenoid_Control(lock_val);
  }
  else if(exkey4==0 && gCBUP.chg_value.b.pBattIn){
   exkey4=1;
  }

}
#endif
/*
adc0->gAdcValue[0]--V1_MONITOR
adc1->gAdcValue[1]--V2_MONITOR
adc2->gAdcValue[2]--C1_MONITOR
adc3->gAdcValue[3]--BMS_V12 DETECTOR
//adc6->gAdcValue[4]--TEMPER From sps194
//adc7->gAdcValue[5]--TEMPER From TMP36
//adc8->gAdcValue[6]--RSEV From sps194
//adc9->gAdcValue[7]--BMS_GND(current) DETECTOR
adc3->gAdcValue[4]--comm type detect  
adc6->gAdcValue[5]--TEMPER From TMP36

adc14->gAdcValue[14]--TEMPER From sps194
adc15->gAdcValue[15]--RSEV From sps194
*/
void CB_AdcProcess(void)
{
  uint16_t mv;
  gCBUP.chg_value.voltage_sps = ( 40 * ADC_FULL_SCALE * (uint16_t)gAdcValue[0] )/4095;
  gCBUP.chg_value.voltage_outer=( 40 * ADC_FULL_SCALE * (uint16_t)gAdcValue[1] )/4095;
  gCBUP.chg_value.current_monitor=( 10 * ADC_FULL_SCALE * (uint16_t)gAdcValue[2] )/4095; //org10
  
 /////////////////////////////////////  
 ////     LM45 10mV/도 
 ////////////////////////////////////
    mv=(2500*(uint16_t)gAdcValue[14])/4095;//LM35
   gCBUP.chg_value.temperature_inner=mv*2;//0.0V offset
 
 //////////////////////////////////////  
 ////    RSEV  unit 10mV
 //////////////////////////////////////
  mv=(2500*(uint16_t)gAdcValue[15])/4095;//LM35
  gCBUP.chg_value.rsev= mv*2; 
  
  ui.voltage=gCBUP.chg_value.voltage_outer;
  ui.current=gCBUP.chg_value.current_monitor;
}

void Drv_console_RXprocess(uint8_t *buffer)
{
	//uint8_t i;
	char val1[3];
	char val2[3];
     char HexVal_R[4]={0,};
     char HexVal_G[4]={0,};
     char HexVal_B[4]={0,};
     
     uint8_t id;
     int r,g,b,blink;
	uint16_t int_val1, int_val2;
     if(buffer[8]!='@')return;
     
    if(buffer[1]=='L' || buffer[1]=='l')
    {
      HexVal_R[0]=buffer[2];
      HexVal_R[1]=buffer[3];
      HexVal_G[0]=buffer[4];
      HexVal_G[1]=buffer[5];      
      HexVal_B[0]=buffer[6];
      HexVal_B[1]=buffer[7];
      sscanf(HexVal_R, "%x", &r);
      sscanf(HexVal_G, "%x", &g);
      sscanf(HexVal_B, "%x", &b);
      if(buffer[1]=='L')blink=1;
      else blink=0;
      r=r*4; b=b*4; g=g*4;
      //printf("Drv_console_RXprocess [%d][%d][%d]\r\n",r,g,b);
      //!L0A0AFF@
    }
    else
    {
        val1[0]=buffer[2];
        val1[1]=buffer[3];
        val1[2]=buffer[4];
        val2[0]=buffer[5];
        val2[1]=buffer[6];
        val2[2]=buffer[7];
        int_val1= atoi(val1);
        id = int_val1;
        int_val2 = atoi(val2);    
    }
    
    switch(buffer[1])
    {
      case 'A':
          charger_ctrl_mode=1;
          break;
      case 'V':
        //!V840200@
          printf("VVV int_val1[%d] int_val2[%d]\r\n",int_val1,int_val2);
        //  CB_ChargingStart(int_val1,int_val2);
          break;
      case 'L':
      case 'l':  
          //Drv_SetRGB(r,g,b,blink);
          break;          
      case 'S':
        int_val2= int_val2& 0x01;
        printf("!S--->id[%d] int_val2[%d]\r\n", id, int_val2);
        switch(id)
        {
            case 0:
            case 1:             
                Drv_solenoid_Control(int_val2);
                break;
              case 5:
                Drv_sps194_set_relay(int_val2);
                break; 
              case 6:
                Drv_sps194_set_DcSD(int_val2);
                break; 
             case 7:
                Drv_sps194_set_fan(int_val2);
                break;  
         }
        break;		
    }
	// printf("voltage[%d] current[%d]-\r\n", voltage, current);
}

void Drv_console_TXprocess()
{
 //  CB_UPLOAD
 	uint8_t sz;
 	uint8_t *consol_data;
 	sz=sizeof(gCBUP);
//     sz_bms=sizeof(gCBUP.bms_value);
 #if 0
    gCBUP.bms_value.cell_votage[0]=3900;
    gCBUP.bms_value.cell_votage[1]=3901;
    gCBUP.bms_value.cell_votage[2]=3902;
    gCBUP.bms_value.cell_votage[17]=3917;
    gCBUP.bms_value.cell_votage[18]=3918;
    gCBUP.bms_value.cell_votage[19]=3919;
    gCBUP.bms_value.pack_temper=0xb0b1;
    gCBUP.bms_value.pack_serial=0x62;
    gCBUP.bms_value.pack_parallel=0x63;
#endif     
 	consol_data=malloc(sz+4);
 	memcpy(consol_data+2, &gCBUP,sz);
 	consol_data[0]='!';
 	consol_data[1]=sz;
 	consol_data[sz+2]='@';
#if 0  
  offset=90;
  for(i=0;i<24;i++){
  	printf("consol_data[%d] [%x]\r\n", i+offset, consol_data[i+offset]);
  }
#endif
 	
#ifdef WIN_UI
     //printf("sizeof consol_data[%d]\r\n",sz+3);
   Hal_Uart_ConsoleTxData(consol_data, sz+3);
   //memset(&gCBUP.bms_value,0,sizeof(gCBUP.bms_value));
#endif
 	free(consol_data);
}

void function_motor_test(void)
{
  static uint8_t lock=0;
  uint8_t data[8]={0,};
  data[2]=0x0F;//mask
  LOCK_TRY_CNT++;
#ifdef LOGOUT    
  printf("function_motor_test[%d] cnt[%d] err[%d]\r\n",lock, LOCK_TRY_CNT, LOCK_ERR_CNT);
#endif
  
  if(lock){
    data[1]=0x0f;   lock=0;
    
  }
  else {
     data[1]=0x00;   lock=1;
    
  }
   LOCK_Control(data);


}
