#ifndef CECEE_H_
#define CECEE_H_

#include "time.h"
#include "string.h"
#include "cecee_drv.h"
#include "cecee_util.h"
#include "cecee_hal.h"
#include "iwdg.h"

#include "../drv/drv_cboard.h"
#include "../drv/drv_sps194.h"
#include "../drv/drv_bms.h"
#include "../drv/drv_as1107.h"
#include "../drv/drv_solenoid.h"
#include "../drv/drv_can.h"
#include "../drv/call_back.h"
#include "../drv/drv_sflash.h"
//#include "../util/cbuffer.h"
#include "../hal/hal_adc.h"
#include "../hal/hal_uart.h"
#include "../hal/hal_spi.h"
#include "../hal/hal_can.h"

#include <stdint.h>

/*
* Define : Cbd Version
*/
#define DEV_NAME			"OTOS-CB"
#define HW_VER_REV_03		0x0003
#define FW_VERSION			0xAB27 //2020.11.27
#define MAIV_LOCK			//2020.11.27
/*
* Define : Debug 
*/
#define xCONSOL_DEBUG  
#define xLOGOUT
#define xWIN_UI 
#define xBMS_ONE_COMMAND
#define xBMS_DBUG
#define xCAN_UP_DEBUG
#define xDEBUG_MOTOR
#define xFUNCTION_MOTOR_TEST
#define xFUNCTION_CHARGE_TEST

/*
* Define : Boolean
*/
#define FALSE		0
#define TRUE		1

/*
* Define : Return Value(Ok and Error)
*/
#define RET_OK		(0)
#define RET_ERR	(-1)

/*
* Define : Set Value(On/Off)
*/
#define SET_ON		(1)
#define SET_OFF	(0)
#define SET_ON_OFF	(2)

/*
* Define : Debug Message Type
*/
#define ERR			1
#define WARN	          2
#define MSG			3
#define PRT			4
#define ATC			5
#define ATR			6

/*
* Define : Filename
*/
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))

typedef struct tag_upload
{
	BMS_VALUE bms_value;
	SPS_VALUE chg_value;
     BIT_MOTOR_STATUS bLockStatus;
     uint32_t rgb;
}CB_UPLOAD;

extern CB_UPLOAD gCBUP;
extern CBOARD cbd;

extern uint8_t _10msTimer1;
extern uint8_t _100msTimer2;
extern uint8_t bms_check_tick;

extern uint8_t CAN_RCV_FLAG, ONE_SEC_FLAG;
extern uint16_t LOCK_TRY_CNT, LOCK_ERR_CNT, LOCK_ERR_VAL;
extern uint32_t _100msTick;
extern UI_FND ui;
extern uint32_t gAdcValue[16];//adc

// For BMS
extern uint8_t BMS_RCV_FLAG;
extern uint8_t CONSOLE_RCV_FLAG;

extern uint8_t gConsole_serch_flag;
extern uint8_t gBms_serch_flag;

extern uint8_t gBms_try;
extern uint8_t gBms_add;
extern uint8_t bms_buf[128];
extern uint8_t console_buf[32];
extern uint8_t bmsAddBuf[16];
extern  uint8_t bms_data_cnt;
extern uint8_t bms_messge;

extern uint16_t VBexist;
extern uint8_t gBmsRequestCnt;

// For CAN1
//extern uint32_t myboard_id;
extern uint8_t can1_rxbuf[8];
extern CBD_CTRL_CMD cbd_ctrl_from_dbd;
//LED
extern LED_BLINK led_blink;
extern MOTOR motor[4];
extern uint16_t m_tick[4];
extern const uint8_t PC_LOCK[4];
extern const uint8_t PC_UNLOCK[4];
#endif
