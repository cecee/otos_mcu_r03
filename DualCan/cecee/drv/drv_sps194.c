#include "stm32f1xx_hal.h"
#include "drv_sps194.h"
#include "cecee.h"

/****************************************************************************
 *                                Defines
 *****************************************************************************/
 #define BMS_SERCH_TIMEOUT 100*5  //5sec
 
/*****************************************************************************
*                           Global Variables
******************************************************************************/
STATUS_SPS status_sps;

//uint8_t step_process=0;
uint16_t step_period[4];

//extern uint16_t SetVoltage;
//extern uint16_t SetCV_Current;
//extern uint16_t SetCC_Current;

void sps194_adjVC(void);
void  mStepCntUP(void);
void  mChargingStart(void);
uint16_t  GetVotageValue(uint16_t voltage);
uint16_t  GetCurrentValue(uint16_t current);
/*****************************************************************************
*  Name:        init_sps194()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_sps194_init(void)
{
	memset(&status_sps, 0, sizeof(STATUS_SPS));
     //gCBUP.chg_value.b.charging=0;
	gCBUP.chg_value.b.charging_finish=1;
	Drv_sps194_set_relay(SET_OFF);
     Drv_sps194_set_DcSD(SET_OFF);
	return RET_OK;
}


/*****************************************************************************
*  Name:        Drv_sps194_set_value()
*  Description:  
*  Returns  : 
******************************************************************************/
int Drv_sps194_set_value()
{
	status_sps.tick=0;
	status_sps.step_voltage =gCBUP.chg_value.voltage_outer;
	status_sps.step_current =gCBUP.chg_value.current_monitor;
	status_sps.des_voltage= gCBUP.chg_value.voltage_setting; 
	status_sps.des_current=  gCBUP.chg_value.current_setting;  
	
	status_sps.dac_voltage_value  = GetVotageValue(gCBUP.chg_value.voltage_setting);
	status_sps.dac_current_value   = GetCurrentValue(gCBUP.chg_value.current_setting);
#ifdef LOGOUT  	
	printf("dac_volt[%d], dac_cur[%d]\r\n",status_sps.dac_voltage_value, status_sps.dac_current_value);
	//Drv_sps194_writer_dac_value(status_sps.dac_voltage_value, status_sps.dac_current_value);
#endif
	//Drv_sps194_set_relay(SET_ON);
	return RET_OK;
}

#if 1
void sps194_adjVC(void)
{

    uint16_t monitor_vlotage, des_voltage;
    uint16_t monitor_current, des_current;
    signed int step_voltage, step_current ;
    signed int diff_voltage, diff_current;
	
	monitor_vlotage=gCBUP.chg_value.voltage_sps;
	monitor_current= gCBUP.chg_value.current_monitor;
	
	des_voltage=status_sps.des_voltage;
	des_current =status_sps.des_current;
	
	step_voltage=status_sps.step_voltage;
	step_current=status_sps.step_current;
	
	diff_voltage=des_voltage - monitor_vlotage;
	diff_current= des_current - monitor_current;

	if(diff_voltage>5){
		step_voltage=step_voltage+5;
	}
	else	if(diff_voltage<(-5)){
		step_voltage=step_voltage-5;
	}
	else {
		if(diff_voltage>0)step_voltage++;
			if(diff_voltage<0)step_voltage--;
	}
	
	if(step_voltage<0)step_voltage=0;
	//else if(step_voltage>4095)step_voltage=4095;
	else if(step_voltage > status_sps.dac_voltage_value)step_voltage = status_sps.dac_voltage_value;
	status_sps.step_voltage=step_voltage;			
			
	if(diff_current>5){
		step_current=step_current+5;
	}
	else	if(diff_voltage<(-5)){
		step_current=step_current-5;
	}
	else {
		if(diff_current>0)step_current++;
		if(diff_current<0)step_current--;
	}		
			
	if(step_current<0)step_current=0;
	//else if(step_current>4095)step_current=4095;
	else if(step_current>status_sps.dac_current_value)step_current = status_sps.dac_current_value;
	status_sps.step_current=step_current;		

     status_sps.tick++;
	Drv_sps194_writer_dac_value(status_sps.step_voltage, status_sps.step_current);
}
#endif
void mStepCntUP(void)
{
	step_period[0]++;
	if(step_period[0]>=10){
		step_period[0]=0;
		step_period[1]++;
	}
}

void  mChargingStart(void)
{
	gCBUP.chg_value.charging_time=0;  
	Drv_sps194_set_value();
	Drv_sps194_set_relay(1);
}

uint8_t mGetBalanceStatus(uint8_t timeout_sec)
{
	uint8_t  r=0;
	if(gCBUP.chg_value.charging_time > timeout_sec) 	return 2;
     if(gCBUP.bms_value.pack_cellBancing.value[0] + gCBUP.bms_value.pack_cellBancing.value[1]  + gCBUP.bms_value.pack_cellBancing.value[2] + gCBUP.bms_value.pack_cellBancing.value[3]) r=1;
	return r;
}

uint8_t mPrecharging(uint16_t timeout_sec)
{
//
//pack voltage 와   gCBUP.chg_value.voltage_outer가 다르면?
//붛량밧데리... 또는 단자 접속 불량
    uint16_t pack_voltage = gCBUP.bms_value.pack_voltage;
    if(gCBUP.chg_value.charging_time > timeout_sec) {
          Drv_sps194_set_relay(0);
		return STEP_NEXT;
	}
    
     if(pack_voltage >= BATT_PRE_CHARGE_HIGH )
    {	
        Drv_sps194_set_relay(0);
        return STEP_NEXT;
     }
    
      if(pack_voltage <= BATT_PRE_CHARGE_LOW )//밧데리불량
	{
       //50V이하경우 
       Drv_sps194_set_relay(0);//relay off
       gCBUP.chg_value.b.bms_err=1;
          return STEP_QUIT;
     }
     else if((pack_voltage > BATT_PRE_CHARGE_LOW) &&  (pack_voltage < BATT_PRE_CHARGE_HIGH))
	{
       //50V~75V인경우 pre-charging
       if(gCBUP.chg_value.charging_time==10) {
           Drv_sps194_set_value();
           Drv_sps194_set_relay(1);
       }
        return  STEP_KEEP;
    }
    return STEP_KEEP;
}

uint8_t mCCcharging(uint16_t timeout_sec)
{
  uint16_t cc_detect_current = ( gCBUP.chg_value.current_setting*90) /100;//- gCBUP.bms_value.pack_current;
  if(gCBUP.chg_value.charging_time <2 ) Drv_sps194_set_value();
  if(gCBUP.chg_value.charging_time > timeout_sec)     return STEP_NEXT;
  //if(gCBUP.bms_value.pack_voltage>=BATT_CC_CHARGE_REF)    return STEP_NEXT;
  if(gCBUP.bms_value.pack_current <= cc_detect_current)    return STEP_NEXT;
  if(gCBUP.bms_value.pack_soc>=1000)    return STEP_NEXT;
  return STEP_KEEP;
  
}

uint8_t mCVcharging(uint16_t timeout_sec)
{
	if(gCBUP.chg_value.charging_time > timeout_sec) 	return STEP_NEXT;
     if(gCBUP.bms_value.pack_soc>=1000) return STEP_NEXT;
     return STEP_KEEP;
}

void Drv_sps194_step(void)
{
// 100ms마다 들어옴
//  STEP_KEEP	     0
//  STEP_NEXT	     1
//  STEP_QUIT	     2
     uint16_t ret;
	switch(gCBUP.chg_value.charging_seg)
	{
		case STEP0_READY:
		  	gCBUP.chg_value.charging_time=0;   
			if(gCBUP.chg_value.b.batt_exist && (gCBUP.chg_value.b.charging_finish ==0) && gCBUP.chg_value.b.charging) {
				mChargingStart();        
				gCBUP.chg_value.charging_seg = STEP5_CV_CHARGING;
			}
			break;
/*			
		case STEP1_BALANCE_DISABLE:
			ret=mGetBalanceStatus(10);//10sec
              // printf("mGetBalanceStatus----ret[%d] time[%d]\r\n",ret,  gCBUP.chg_value.charging_time);
                 switch(ret){
                        case 0: //balance ok
                          gCBUP.chg_value.charging_time=0;
                          gCBUP.chg_value.charging_seg = STEP2_BALANCE_ENABLE;
                          break;
                       case 1: //balance ...ing
                         // gCBUP.chg_value.charging_time=0;
                         // gCBUP.chg_value.charging_seg = STEP2_BALANCE_ENABLE;
                        break;    
                        case 2: //time_out
                          //memset(step_period,0,sizeof(step_period));
                          gCBUP.chg_value.charging_time=0;
                          gCBUP.chg_value.charging_seg = STEP0_READY;
                        break;                        
               }
              
			break;
 	  case STEP2_BALANCE_ENABLE:	
                   ret=mGetBalanceStatus(10);
                   switch(ret){
                      case 0: //balance Ok!!
                          gCBUP.chg_value.charging_time=0;
                          gCBUP.chg_value.charging_seg = STEP3_PRECHARGING;
                        break;
                       case 1: //balance ...ing
                         // gCBUP.chg_value.charging_time=0;
                         // gCBUP.chg_value.charging_seg = STEP3_PRECHARGING;
                          break;    
                        case 2: //time_out
                          gCBUP.chg_value.charging_time=0;
                          gCBUP.chg_value.charging_seg = STEP0_READY;
                          break;                        
                 }           
                 break;	
	  case STEP3_PRECHARGING:	//50V ~75V  낮으면 밧게리 불량
	  		ret=mPrecharging(1800);//30분대기 30*60
              // ret=mPrecharging(30);//30분대기 30*60
                switch(ret){
                        case  STEP_NEXT: //75V 이상인경우
                          gCBUP.chg_value.charging_time=0;
                          gCBUP.chg_value.charging_seg = STEP4_CC_CHARGING;
                          break;
                      case STEP_QUIT: 
                          gCBUP.chg_value.charging_time=0;
                          gCBUP.chg_value.charging_seg = STEP0_READY;
                          break;  
                      case STEP_KEEP: break;    
                 }               
			break;
	 	case STEP4_CC_CHARGING:	//10A 
	 		ret=mCCcharging(7200);//120*60 120분대기 CC구간 Time out
                if(ret==STEP_NEXT) {
				 gCBUP.chg_value.charging_time=0;
                    gCBUP.chg_value.charging_seg = STEP5_CV_CHARGING;
			}	
	 		break;
*/			 	
	 	case STEP5_CV_CHARGING:	
	 		ret = mCVcharging(240*60);//240분(max 4시간)대기 CV구간 Time out
              // printf("Drv_sps194_step----[%d] time[%d] ret[%d]\r\n",gCBUP.chg_value.charging_seg,  gCBUP.chg_value.charging_time, ret);
  	  		if(ret==STEP_NEXT) {
			//finish charging
			   printf("Drv_sps194_step----finished charging\r\n");
                  gCBUP.chg_value.charging_time=0;
			   //gCBUP.chg_value.b.charging = 0;
                  gCBUP.chg_value.b.charging_finish=1;
                  Drv_sps194_set_relay(0);
                  gCBUP.chg_value.charging_seg = STEP0_READY;
			}   
	 		break;	  		
	  		
	}
}
/*****************************************************************************
*  Name:        Drv_sps194_process()
*  Description:  10ms마다 한번씩 들어옴
*  Arguments:  
*  Returns  : 
******************************************************************************/
void Drv_sps194_process(void)
{
	sps194_adjVC();
}

uint16_t  GetVotageValue(uint16_t voltage)
{
	uint16_t v_val;
	if(voltage<415) voltage=415;
	v_val =  ((voltage*10)-4150)/85;// Vin=(Vout-41.5)/8.5 //41.5V 보다 낮은전압은???
	v_val = (4095*v_val)/ADC_FULL_SCALE;
	if(v_val>=4095) v_val=4095;
	return v_val;
}

uint16_t  GetCurrentValue(uint16_t current)
{
  	uint16_t  i_val;
	if(current<10) current=10;//current 1.0A미만 Not support
	i_val= ((current*100)+3750)/475;// Vin=(Iout+3.75)/4.75 //1A 보다 낮은전류는???	
	i_val=(4095*i_val)/ADC_FULL_SCALE;
	if(i_val>=4095) i_val=4095;	
	return i_val;
}


void Drv_sps194_writer_dac_value(uint16_t v_val, uint16_t i_val)
{
     DAC_set_voltage(v_val);
	DAC_set_current(i_val);
}

void Drv_sps194_off()
{
	memset(&gCBUP, 0, sizeof(gCBUP));
}

void SPS_Control(uint8_t *data)
{
  uint8_t val,mask;
  val=data[1];
  mask= data[2];
  //printf("SPS_Control mask[%x] val[%x]\r\n", mask, val);
  if(CHECK_BIT(mask,0))  Drv_sps194_set_relay(CHECK_BIT(val,0));
  if(CHECK_BIT(mask,1))  Drv_sps194_set_DcSD(CHECK_BIT(val,1));
  if(CHECK_BIT(mask,2))  Drv_sps194_set_fan(CHECK_BIT(val,2));
  if(CHECK_BIT(mask,3)) {
    if(CHECK_BIT(val,3)){
    //bms Drv_bms_restart
	 Drv_bms_restart();
    }
  }
}



void Drv_sps194_set_relay(uint8_t on)
{
    on=(on)? 1:0;
    gCBUP.chg_value.b.relay=on;
    if(gCBUP.chg_value.b.relay) HAL_GPIO_WritePin(GPIOD, RL_CONT_Pin,GPIO_PIN_SET); 
    else	HAL_GPIO_WritePin(GPIOD, RL_CONT_Pin,GPIO_PIN_RESET); 				
}

void Drv_sps194_set_DcSD(uint8_t on)
{
  on=(on)? 1:0;
  gCBUP.chg_value.b.dc_sd=on;	
  if(on) HAL_GPIO_WritePin(GPIOB, DC_SD_Pin,GPIO_PIN_SET); 
  else	  HAL_GPIO_WritePin(GPIOB, DC_SD_Pin,GPIO_PIN_RESET); 	
}


void Drv_sps194_set_fan(uint8_t on)
{
  on=(on)? 1:0;
  gCBUP.chg_value.b.ctrl_fan=on;	
  if(on) HAL_GPIO_WritePin(GPIOD, FAN_CONT_Pin,GPIO_PIN_SET); 
  else	  HAL_GPIO_WritePin(GPIOD, FAN_CONT_Pin,GPIO_PIN_RESET); 	
}

void Drv_sps194_set(void)
{
  Drv_sps194_set_DcSD(1);
  Drv_sps194_set_fan(1);
}
	