#include "stm32f1xx_hal.h"
#include "Drv_dcan.h"
#include "d_cecee.h"

extern CAN_HandleTypeDef hcan1;
extern uint8_t TransmitMailbox1;

DB_UPLOAD db_up;
//CBD_CTRL_CMD CCtrlCmd[12];
CBD_DATA CbData[16];

uint8_t cbd_rcv_buf[15][16][8]={0,};//can 15번 올라온다
uint8_t dbd_rcv_buf[4][16][8]={0,};//can 4번 올라온다

int Drv_dCan_init(void)
{
  Hal_CAN_Init();
  memset(&db_up,0,sizeof(db_up));
  memset(&CbData,0,sizeof(CbData));

  printf("DB_UPLOAD --size[%d] \r\n",sizeof(DB_UPLOAD));
  printf("D_STATUS --size[%d] \r\n",sizeof(D_STATUS));
  printf("CBD_DATA --size[%d] \r\n",sizeof(CBD_DATA));
  printf("db_up --[%d][%d][%d][%d] \r\n",db_up.dStatus.bFanAlarm.fan_f1,db_up.dStatus.bFanAlarm.fan_f2,db_up.dStatus.bFanAlarm.fan_f3,db_up.dStatus.bFanAlarm.fan_f4);
  return 1;
}

void Drv_SendmsgToCbd(uint32_t can_id, uint8_t *data)
{
 	hcan1.pTxMsg->ExtId=can_id;
	memcpy(hcan1.pTxMsg->Data, data ,8);
     TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
	do
	{
		TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
	}
	while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );
    //DBG(WARN,"Drv_SendmsgToCbd [%08X] [%x][%x]\n",can_id, data[0], data[1]);
}

void Drv_CanRx()
{
    uint8_t msg_buf[16]={0,};
    uint32_t extid;
    extid=hcan1.pRxMsg->ExtId;
    printf(",,,%04x\r\n",extid);
    if((extid&0xFFFF0000)!=0x1CEC0000) return;

    memcpy(&msg_buf[0], &extid, 4);//cid, seq포함
    memcpy(&msg_buf[4], &hcan1.pRxMsg->Data,8);
    Drv_MessagePublic(CAN, msg_buf,12);
}

