/*
 * drv_sflash.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "string.h"
#include "stm32f1xx_hal.h"
#include "spi.h"
#include "cecee.h"
#include "drv_sflash.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define SF_TRACE(...)
#define SF_TRACE(...)				DBG(__VA_ARGS__)

/****************************************************************************
 *                             Data types
*****************************************************************************/
typedef enum {
	WREN		= 0x06,    
	WRDI		= 0x04,    
	WRSR		= 0x01,   
	RDID		= 0x9F,    
	RDSR		= 0x05,    
	READ		= 0x03,    
	FAST_READ	= 0x0B,
	DREAD		= 0x3B,  
	RES			= 0xAB,  
	REMS		= 0x90, 
	SE			= 0x20,
	BE			= 0x52,
	CE			= 0x60,
	PP			= 0x02,
	DP			= 0xB9,
	RDP			= 0xAB,
	
	MX25L1006E_OPCODE_END
}s_op_type;

typedef struct {
	s_op_type opcode;
	unsigned char addr[3];
	unsigned char dummy;
} s_proto_type;

typedef struct {
	s_proto_type cmd;
	unsigned char c_size;
} cmd_info_type;

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/
unsigned int sflash_addr = SPI_OTA_ACCESS_START;
static unsigned char cmd_buf[6];
static cmd_info_type *cmd_info;

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/


/*****************************************************************************
*                                Functions
******************************************************************************/
void fn_make_cmd_header(s_op_type op_code, unsigned char* p_cmd)
{
	unsigned char tmp_addr[3];
	
	tmp_addr[0] = (unsigned char)((sflash_addr & 0x00ff0000) >> 16);
	tmp_addr[1] = (unsigned char)((sflash_addr & 0x0000ff00) >> 8);
	tmp_addr[2] = (unsigned char)(sflash_addr & 0x000000ff);

	cmd_info = (cmd_info_type *)p_cmd;

	cmd_info->cmd.opcode = op_code;
	memcpy(cmd_info->cmd.addr, tmp_addr, sizeof(tmp_addr));

	switch(op_code)
	{
		case CE:
		case WREN:
		case RDID:
			cmd_info->c_size = 1;
			break;
		case FAST_READ:
			cmd_info->cmd.dummy = 0xff;
			cmd_info->c_size = 5;	
			break;
		case PP:
			cmd_info->c_size = 4;
			break;
		case SE:
			cmd_info->c_size = 4;
		default:
			break;
	}
}

void fn_Serial_Flash_Data_Write_Enable(void)
{
	cmd_info = (cmd_info_type *)cmd_buf;
	
	Hal_Spi1_CS(SET_ON);
	
	fn_make_cmd_header(WREN, cmd_buf);
	//SF_TRACE(MSG,"fn_Serial_Flash_Data_Write_Enable op[%02x] add[%02X][%02X][%02X] size[%02X]\n",WREN, cmd_info->cmd.addr[0], cmd_info->cmd.addr[1],cmd_info->cmd.addr[2],cmd_info->c_size);	

	if(Hal_Spi1_Write(cmd_buf, cmd_info->c_size)==RET_ERR)
		while(1) SF_TRACE(ERR,"Sflash WE ERR\n");

	Hal_Spi1_CS(SET_OFF);
}

void fn_Serial_Flash_ID_Read(void)
{
	cmd_info = (cmd_info_type *)cmd_buf;
	Hal_Spi1_CS(SET_ON);
	fn_make_cmd_header(RDID, cmd_buf);
	Hal_Spi1_Write(cmd_buf, cmd_info->c_size);
	memset(cmd_buf, 0x00, sizeof(cmd_buf));
	Hal_Spi1_Read(cmd_buf, 4);
	Hal_Spi1_CS(SET_OFF);
	SF_TRACE(MSG,"Serial Flash Init : MID:%02X, Type:%02X, Den:%02X\n", cmd_buf[0], cmd_buf[1], cmd_buf[2]);
}

int fn_Serial_Flash_Erase(void)
{	
	int ret=RET_OK;
	
	if (((sflash_addr <= (SPI_OTA_ACCESS_START-SPI_QUEUE_SIZE))/* && (event3.bits.sflash_fdata_access == 2)*/) || (sflash_addr >= SPI_OTA_ACCESS_START))		
	{
		cmd_info = (cmd_info_type *)cmd_buf;

		fn_Serial_Flash_Data_Write_Enable();
			
		Hal_Spi1_CS(SET_ON);;

		fn_make_cmd_header(SE, cmd_buf);

		ret = Hal_Spi1_Write(cmd_buf, cmd_info->c_size);

		Hal_Spi1_CS(SET_OFF);
	}
	else
	{
		ret = RET_ERR;
		SF_TRACE(ERR,"Requested area[0x%x ~ 0x%x] is fdata area. so, i can't write in this area.\n\r", sflash_addr, sflash_addr+SPI_QUEUE_SIZE-1);
	}

	return ret;
}

void fn_Serial_Flash_ChipErase(void)
{
	if (((sflash_addr <= (SPI_OTA_ACCESS_START-SPI_QUEUE_SIZE)) /*&& (event3.bits.sflash_fdata_access == 2)*/) || (sflash_addr >= SPI_OTA_ACCESS_START))		
	{
		cmd_info = (cmd_info_type *)cmd_buf;

		fn_Serial_Flash_Data_Write_Enable();
			
		Hal_Spi1_CS(SET_ON);;

		fn_make_cmd_header(CE, cmd_buf);

		Hal_Spi1_Write(cmd_buf, cmd_info->c_size);

		Hal_Spi1_CS(SET_OFF);
	}
	else
	{
		SF_TRACE(ERR,"Requested area[0x%x ~ 0x%x] is fdata area. so, i can't write in this area.\n\r", sflash_addr, sflash_addr+SPI_QUEUE_SIZE-1);
	}
}

int fn_Serial_Flash_Data_Write(unsigned char *pdata, unsigned short size)
{
	if (((sflash_addr <= (SPI_OTA_ACCESS_START-SPI_QUEUE_SIZE)) /*&& (event3.bits.sflash_fdata_access == 2)*/) || (sflash_addr >= SPI_OTA_ACCESS_START))		
	{
		cmd_info = (cmd_info_type *)cmd_buf;
		fn_Serial_Flash_Data_Write_Enable();
		Hal_Spi1_CS(SET_ON);

		fn_make_cmd_header(PP, cmd_buf);
		
		
		if(Hal_Spi1_Write(cmd_buf, cmd_info->c_size)!=RET_OK)
		{
			Hal_Spi1_CS(SET_OFF);
			SF_TRACE(ERR,"Sflash WR ERR_1\r\n");
			
			return RET_ERR;
		}

		if(Hal_Spi1_Write(pdata, size)!=RET_OK)
		{
			Hal_Spi1_CS(SET_OFF);
			SF_TRACE(ERR,"Sflash WR ERR_2\r\n");
			return RET_ERR;
		}

		Hal_Spi1_CS(SET_OFF);

		return RET_OK;
	}
	else
	{
		SF_TRACE(ERR,"Requested area[0x%x ~ 0x%x] is fdata area. so, i can't write in this area.\n\r", sflash_addr, sflash_addr+SPI_QUEUE_SIZE-1);
		return RET_ERR;
	}
}

void fn_Serial_Flash_Data_Read(unsigned char *pdata, unsigned short size)
{
	unsigned short i;

	cmd_info = (cmd_info_type *)cmd_buf;	
	
	fn_Serial_Flash_Data_Write_Enable();

	Hal_Spi1_CS(SET_ON);;

	fn_make_cmd_header(FAST_READ, cmd_buf);

	Hal_Spi1_Write(cmd_buf, cmd_info->c_size);

	for(i=0; i<size; i++)
	{
		Hal_Spi1_Read(&pdata[i], 1);
	}

	Hal_Spi1_CS(SET_OFF);
}

int ExFLASH_If_Erase(unsigned int destination, unsigned int length)
{
  unsigned char i = 0;
  unsigned char endSec;
  int ret=RET_OK;
  
  endSec = (length/SPI_QUEUE_SIZE)+((length%SPI_QUEUE_SIZE)?1:0);
  
  for (i = 0; i < endSec; i++)
  {
  	sflash_addr = destination + SPI_QUEUE_SIZE*i;
//	printf(" - Erase current addr: 0x%x\r\n", sflash_addr);	
	ret = fn_Serial_Flash_Erase();
	if (ret != RET_OK)
		return ret;
  }  

  return ret;
}

int ExFLASH_If_Write(unsigned int destination, unsigned char *p_source, unsigned int length)
{
  unsigned char i = 0;
  unsigned char endSec;
  unsigned char wrBuff[SPI_QUEUE_SIZE];
  //char rdMagic[12];
  int ret=RET_OK;

  endSec = (length/SPI_SECTOR_SIZE)+((length%SPI_SECTOR_SIZE)?1:0);  
  
  for (i = 0; i < endSec; i++)
  {
  	sflash_addr = destination + SPI_SECTOR_SIZE*i;
	//printf(" - Erase current addr: 0x%x endSec[%d]\r\n", sflash_addr, endSec);
    ret = fn_Serial_Flash_Erase();	
	if (ret != RET_OK)
	{
		SF_TRACE(ERR," - Erase fail, current addr: 0x%x\r\n", sflash_addr);	
		return ret;
	}
	HAL_Delay(50);
  }

  endSec = (length/SPI_QUEUE_SIZE)+((length%SPI_QUEUE_SIZE)?1:0);

  for (i = 0; i < endSec; i++)
  {  	
  	memset(wrBuff, 0, sizeof(wrBuff));
  	sflash_addr = destination + SPI_QUEUE_SIZE*i;
	//printf(" - Write current addr: 0x%x endSec[%d]\r\n", sflash_addr,endSec);
	
	memcpy(&wrBuff[0], &p_source[i*SPI_QUEUE_SIZE], SPI_QUEUE_SIZE);

    ret = fn_Serial_Flash_Data_Write(wrBuff, SPI_QUEUE_SIZE);
  	if (ret != RET_OK)
	{
		SF_TRACE(ERR," - Write fail, current addr: 0x%x\r\n", sflash_addr);   
		return ret;
	}
#if 0
    Hexdump_Addr(wrBuff, sflash_addr, SPI_QUEUE_SIZE);
#else
	//Drv_Delay(100);
  HAL_Delay(100);
#endif	
  }
#if 0
  if ((destination == SPI_FDATA_ACCESS_START) || (destination == (SPI_FDATA_ACCESS_START+SPI_SECTOR_SIZE)))
  	ret = ExFLASH_MagicCode_Compare(destination, rdMagic);
#endif
  return ret;
}

void ExFLASH_If_Read(unsigned int destination, unsigned char *p_source, unsigned int length)
{
  unsigned char i = 0;
  unsigned char endSec;
  unsigned char rdBuff[SPI_QUEUE_SIZE];
  
  endSec = (length/SPI_QUEUE_SIZE)+((length%SPI_QUEUE_SIZE)?1:0);
  
  for (i = 0; i < endSec; i++)
  {
  	memset(rdBuff, 0, sizeof(rdBuff));
  	sflash_addr = destination + SPI_QUEUE_SIZE*i;
	
	fn_Serial_Flash_Data_Read(rdBuff, SPI_QUEUE_SIZE);

	memcpy(&p_source[i*SPI_QUEUE_SIZE], &rdBuff[0], SPI_QUEUE_SIZE);
	//printf(" - Read current addr: 0x%x\r\n", sflash_addr);
#if 0
	Hexdump_Addr(rdBuff, SPI_QUEUE_SIZE, sflash_addr);
	printf("\r\n");
#else
	//Drv_Delay(100);
  HAL_Delay(100);
#endif
  }
}
#if 0
int ExFLASH_MagicCode_Compare(unsigned int destination, char *rdMagic)
{
	int ret;
	unsigned char rdDataBuf[SPI_QUEUE_SIZE];

	memset(rdDataBuf, 0, sizeof(rdDataBuf));
	ExFLASH_If_Read(destination, rdDataBuf, SPI_QUEUE_SIZE);
	memcpy(rdMagic, &rdDataBuf[4], 12);

	if (!(strcmp(rdMagic, g_magic)))
	{	
		ret = RET_OK;
	}
	else
	{
		SF_TRACE(ERR," - Magic code comparing is fail.[0x%x, %s]\r\n", destination, rdMagic);
		ret = RET_ERR;
	}

	return ret;
}
#endif
/*****************************************************************************
*  Name:        Drv_Sflash_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Sflash_Init(void)
{
	fn_Serial_Flash_ID_Read();
	return RET_OK;
}
