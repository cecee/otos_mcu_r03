#include "stm32f1xx_hal.h"
#include "Hvac_gpio.h"
#include "extern_hvac.h"

void Hvac_gpio_getport(void)
{
  gStatus.bInput.ac_fail = HAL_GPIO_ReadPin(GPIOC, AC_FAIL_Pin);
  gStatus.bInput.dc_fail = HAL_GPIO_ReadPin(GPIOC, DC_FAIL_Pin);
  gStatus.bInput.bat_fail = HAL_GPIO_ReadPin(GPIOC, BAT_FAIL_Pin);
  gStatus.bInput.door = HAL_GPIO_ReadPin(GPIOA, pDoor_Pin);
  gStatus.bInput.ovc = HAL_GPIO_ReadPin(GPIOC, OVC_Pin);
  gStatus.bInput.shock = HAL_GPIO_ReadPin(GPIOD, pSHOCK1_Pin);
  gStatus.bInput.slope1 = HAL_GPIO_ReadPin(pSLOPE1_GPIO_Port, pSLOPE1_Pin);
  gStatus.bInput.slope2 = HAL_GPIO_ReadPin(pSLOPE2_GPIO_Port, pSLOPE2_Pin);
  //12v fan
  gStatus.bFanAlarm.c1 = HAL_GPIO_ReadPin(FAN_F1_AL_GPIO_Port, FAN_F1_AL_Pin);
  gStatus.bFanAlarm.c2 = HAL_GPIO_ReadPin(FAN_F2_AL_GPIO_Port, FAN_F2_AL_Pin);
  gStatus.bFanAlarm.c3 = HAL_GPIO_ReadPin(FAN_F3_AL_GPIO_Port, FAN_F3_AL_Pin);
 //24v fan
  gStatus.bFanAlarm.vf = HAL_GPIO_ReadPin(FAN_R1_AL_GPIO_Port, FAN_R1_AL_Pin);
  gStatus.bFanAlarm.vr = HAL_GPIO_ReadPin(FAN_R2_AL_GPIO_Port, FAN_R2_AL_Pin);
  gStatus.bFanAlarm.vs = HAL_GPIO_ReadPin(FAN_R3_AL_GPIO_Port, FAN_R3_AL_Pin);
  //printf("slope1[%d] slope2[%d]\r\n", gStatus.bInput.slope1, gStatus.bInput.slope2);
}

void Hvac_adcMux(uint8_t mux)
{
  //printf("mux[%d]\r\n", mux);
  switch(mux){
    case 0:
        HAL_GPIO_WritePin(pADC_SEL_A0_GPIO_Port, pADC_SEL_A0_Pin,GPIO_PIN_RESET);   
        HAL_GPIO_WritePin(pADC_SEL_A1_GPIO_Port, pADC_SEL_A1_Pin,GPIO_PIN_RESET);
      break;
    case 1:
        HAL_GPIO_WritePin(pADC_SEL_A0_GPIO_Port, pADC_SEL_A0_Pin,GPIO_PIN_SET);   
        HAL_GPIO_WritePin(pADC_SEL_A1_GPIO_Port, pADC_SEL_A1_Pin,GPIO_PIN_RESET);
      break;    
    case 2:
        HAL_GPIO_WritePin(pADC_SEL_A0_GPIO_Port, pADC_SEL_A0_Pin,GPIO_PIN_RESET);   
        HAL_GPIO_WritePin(pADC_SEL_A1_GPIO_Port, pADC_SEL_A1_Pin,GPIO_PIN_SET);
      break;
    case 3:
        HAL_GPIO_WritePin(pADC_SEL_A0_GPIO_Port, pADC_SEL_A0_Pin,GPIO_PIN_SET);   
        HAL_GPIO_WritePin(pADC_SEL_A1_GPIO_Port, pADC_SEL_A1_Pin,GPIO_PIN_SET);
       break;  
  }
}

void CTRL_VFF(uint8_t on)
{
  on=(on) ? 1:0;
  gStatus.bFanCtrl.vf=on;
  if(on)HAL_GPIO_WritePin(pFAN_R1__GPIO_Port, pFAN_R1__Pin,GPIO_PIN_SET);   
  else HAL_GPIO_WritePin(pFAN_R1__GPIO_Port, pFAN_R1__Pin,GPIO_PIN_RESET);  
}

void CTRL_VFR(uint8_t on)
{
  on=(on) ? 1:0;
  gStatus.bFanCtrl.vr=on;
  if(on)HAL_GPIO_WritePin(pFAN_R2__GPIO_Port, pFAN_R2__Pin,GPIO_PIN_SET);   
  else HAL_GPIO_WritePin(pFAN_R2__GPIO_Port, pFAN_R2__Pin,GPIO_PIN_RESET);  
}

void CTRL_VFS(uint8_t on)//spare
{
  on=(on) ? 1:0;
  gStatus.bFanCtrl.vs=on;
  if(on)HAL_GPIO_WritePin(pFAN_R3__GPIO_Port, pFAN_R3__Pin,GPIO_PIN_SET);   
  else HAL_GPIO_WritePin(pFAN_R3__GPIO_Port, pFAN_R3__Pin,GPIO_PIN_RESET);  
}

void CTRL_C1(uint8_t on)
{
  on=(on) ? 1:0;
  //printf("CTRL_C1(%d)\r\n",on);
  gStatus.bFanCtrl.c1=on;
  if(on)HAL_GPIO_WritePin(pFAN_F1__GPIO_Port, pFAN_F1__Pin,GPIO_PIN_SET);   
  else  HAL_GPIO_WritePin(pFAN_F1__GPIO_Port, pFAN_F1__Pin,GPIO_PIN_RESET);  
}

void CTRL_C2(uint8_t on)
{
  on=(on) ? 1:0;
  // printf("CTRL_C2(%d)\r\n",on);
  gStatus.bFanCtrl.c2=on;
  if(on)HAL_GPIO_WritePin(pFAN_F2__GPIO_Port, pFAN_F2__Pin,GPIO_PIN_SET);   
  else  HAL_GPIO_WritePin(pFAN_F2__GPIO_Port, pFAN_F2__Pin,GPIO_PIN_RESET);  
}

void CTRL_C3(uint8_t on)
{
  on=(on) ? 1:0;
  gStatus.bFanCtrl.c3=on;
  // printf("CTRL_C3(%d)\r\n",on);
  if(on)HAL_GPIO_WritePin(pFAN_F3__GPIO_Port, pFAN_F3__Pin,GPIO_PIN_SET);   
  else HAL_GPIO_WritePin(pFAN_F3__GPIO_Port, pFAN_F3__Pin,GPIO_PIN_RESET);  
}

void RELAY_Light(uint8_t on)
{
  on=(on) ? 1:0;
  gStatus.bRelayCtrl.ac_light=on;
  if(on){
    HAL_GPIO_WritePin(pAC_LIGHT_SET_GPIO_Port,   pAC_LIGHT_SET_Pin,  GPIO_PIN_SET); 
    HAL_GPIO_WritePin(pAC_LIGHT_RESET_GPIO_Port, pAC_LIGHT_RESET_Pin,GPIO_PIN_RESET); 	
  }
  else 
  {
    HAL_GPIO_WritePin(pAC_LIGHT_SET_GPIO_Port,   pAC_LIGHT_SET_Pin,  GPIO_PIN_RESET); 
    HAL_GPIO_WritePin(pAC_LIGHT_RESET_GPIO_Port, pAC_LIGHT_RESET_Pin,GPIO_PIN_SET); 	  
  }
  HAL_Delay(30); 
  HAL_GPIO_WritePin(pAC_LIGHT_SET_GPIO_Port,   pAC_LIGHT_SET_Pin,  GPIO_PIN_RESET);   
  HAL_GPIO_WritePin(pAC_LIGHT_RESET_GPIO_Port, pAC_LIGHT_RESET_Pin,GPIO_PIN_RESET);    
}

void RELAY_Heater1(uint8_t on)
{
  on=(on) ? 1:0;
   gStatus.bRelayCtrl.ac_heater1=on;
   if(on){
    HAL_GPIO_WritePin(GPIOE, pAC_HTR1_SET_Pin,  GPIO_PIN_SET); 
    HAL_GPIO_WritePin(GPIOE, pAC_HTR1_RESET_Pin,GPIO_PIN_RESET); 	
  }
  else 
  {
    HAL_GPIO_WritePin(GPIOE, pAC_HTR1_SET_Pin,  GPIO_PIN_RESET); 
    HAL_GPIO_WritePin(GPIOE, pAC_HTR1_RESET_Pin,GPIO_PIN_SET); 	  
  }
  HAL_Delay(30); 
  HAL_GPIO_WritePin(GPIOE, pAC_HTR1_SET_Pin,  GPIO_PIN_RESET);   
  HAL_GPIO_WritePin(GPIOE, pAC_HTR1_RESET_Pin,GPIO_PIN_RESET);    
}

void RELAY_Heater2(uint8_t on)
{
  on=(on) ? 1:0;
  gStatus.bRelayCtrl.ac_heater2=on;
   if(on){
    HAL_GPIO_WritePin(GPIOE, pAC_HTR2_SET_Pin,  GPIO_PIN_SET); 
    HAL_GPIO_WritePin(GPIOE, pAC_HTR2_RESET_Pin,GPIO_PIN_RESET); 	
  }
  else 
  {
    HAL_GPIO_WritePin(GPIOE, pAC_HTR2_SET_Pin,  GPIO_PIN_RESET); 
    HAL_GPIO_WritePin(GPIOE, pAC_HTR2_RESET_Pin,GPIO_PIN_SET); 	  
  }
  HAL_Delay(30); 
  HAL_GPIO_WritePin(GPIOE, pAC_HTR2_SET_Pin,  GPIO_PIN_RESET);   
  HAL_GPIO_WritePin(GPIOE, pAC_HTR2_RESET_Pin,GPIO_PIN_RESET); 
}

void RELAY_Heater3(uint8_t on)
{
  on=(on) ? 1:0;
  gStatus.bRelayCtrl.ac_heater3=on;
  if(on){
    HAL_GPIO_WritePin(GPIOE, pAC_HTR3_SET_Pin,  GPIO_PIN_SET); 
    HAL_GPIO_WritePin(GPIOE, pAC_HTR3_RESET_Pin,GPIO_PIN_RESET); 	
  }
  else 
  {
    HAL_GPIO_WritePin(GPIOE, pAC_HTR3_SET_Pin,  GPIO_PIN_RESET); 
    HAL_GPIO_WritePin(GPIOE, pAC_HTR3_RESET_Pin,GPIO_PIN_SET); 	  
  }
  HAL_Delay(30); 
  HAL_GPIO_WritePin(GPIOE, pAC_HTR3_SET_Pin,  GPIO_PIN_RESET);   
  HAL_GPIO_WritePin(GPIOE, pAC_HTR3_RESET_Pin,GPIO_PIN_RESET); 
}

void HVAC_Relay(uint8_t *data)
{
  uint8_t val,mask;
  val=data[1];
  mask= data[2];
  if(CHECK_BIT(mask,0))  RELAY_Light(CHECK_BIT(val,0));
  if(CHECK_BIT(mask,1))  RELAY_Heater1(CHECK_BIT(val,1));
  if(CHECK_BIT(mask,2))  RELAY_Heater2(CHECK_BIT(val,2));
  if(CHECK_BIT(mask,3))  RELAY_Heater3(CHECK_BIT(val,3));
}

void HVAC_Fan(uint8_t *data)
{
  uint8_t val,mask;
  val = data[1];
  mask= data[2];
  if(CHECK_BIT(mask,0))  CTRL_VFF(CHECK_BIT(val,0));
  if(CHECK_BIT(mask,1))  CTRL_VFR(CHECK_BIT(val,1));
  if(CHECK_BIT(mask,2))  CTRL_VFS(CHECK_BIT(val,2));
  if(CHECK_BIT(mask,3))  CTRL_C1(CHECK_BIT(val,3));
  if(CHECK_BIT(mask,4))  CTRL_C2(CHECK_BIT(val,4));
  if(CHECK_BIT(mask,5))  CTRL_C3(CHECK_BIT(val,5));
 }