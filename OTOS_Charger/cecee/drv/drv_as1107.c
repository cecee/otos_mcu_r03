#include "main.h"
#include "stm32f1xx_hal.h"
#include "cecee.h"
#include "drv_as1107.h"

/****************************************************************************
 *                                Defines
 *****************************************************************************/
#define AS_TRACE(...)    DBG(__VA_ARGS__)

#define NO_OP 0
#define DIGIT0 1
#define DIGIT1 2
#define DIGIT2 3
#define DIGIT3 4
#define DIGIT4 5
#define DIGIT5 6
#define DIGIT6 7
#define DIGIT7 8
#define DEC_MODE 9
#define INTENSITY  10
#define SCAN_LIMIT 11
#define SHUTDOWN 12
#define N_A    13
#define FEATURE  14
#define DISP_TEST  15

//SHUTDOWN
//Shutdown mode
#define SD_DEFAULT   0x00
#define SD_UNCHANGED 0x80
//Normal mode
#define DEFAULT   0x01
#define UNCHANGED  0x81

//DEC_MODE
#define NO_DEC_ALL  0x00
#define NO_DEC_17  0x01
#define NO_DEC_47  0x0F
#define DEC_ALL      0xFF

//DISP_TEST
#define NORMAL  0x00
#define TEST    0x01

//INTENSITY
#define DUTY_1    0x00
#define DUTY_3    0x01
#define DUTY_5    0x02
#define DUTY_7    0x03
#define DUTY_9    0x04
#define DUTY_11    0x05
#define DUTY_13    0x06
#define DUTY_15    0x07
#define DUTY_17    0x08
#define DUTY_19    0x09
#define DUTY_21    0x0A
#define DUTY_23    0x0B
#define DUTY_25    0x0C
#define DUTY_27    0x0D
#define DUTY_29    0x0E
#define DUTY_31    0x0F

//SCAN_LIMIT
#define DISP_00    0x00
#define DISP_01    0x01
#define DISP_02    0x02
#define DISP_03    0x03
#define DISP_04    0x04
#define DISP_05    0x05
#define DISP_06    0x06
#define DISP_07    0x07

//FEATURE
#define CLK_EN    0x00
#define REG_RES   0x01
#define DEC_SEL   0x02
#define SPI_EN    0x03
#define BLK_EN    0x04
#define BLK_FR    0x05
#define SYNC     0x06
#define BLK_START  0x07

//7Segment
#define SEG_DP  0x80
#define SEG_A   0x40
#define SEG_B   0x20
#define SEG_C   0x10
#define SEG_D   0x08
#define SEG_E   0x04
#define SEG_F   0x02
#define SEG_G   0x01

uint8_t seg[16]={0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b, 0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47};
uint8_t led_seg[8]={SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G, SEG_DP};

UI_FND ui;
//TIME_OUT_LED led_tick;
LED_MONO led_mono[8];
LED_RGB led_rgb;
LED_BLINK led_blink;

void Drv_as1107_init(void)
{
    //printf("init as1107.....\r\n");
    memset(&led_mono, 0, sizeof(led_mono));
    memset(&led_rgb, 0, sizeof(led_rgb));
    memset(&ui, 0, sizeof(UI_FND));
    writeRegister(SCAN_LIMIT, DISP_06);//set digit 0~6 7��
    writeRegister(INTENSITY, DUTY_3);
    writeRegister(DEC_MODE, NO_DEC_ALL);
    writeRegister(SHUTDOWN,DEFAULT);
    Drv_cat4109_init();
}

void Drv_cat4109_init(void)
{
  HAL_GPIO_WritePin(GPIOC, LED_OE_Pin,GPIO_PIN_SET); 
  TIM3->CCR1 = 0x80;
  TIM3->CCR2 = 0x00;
  TIM3->CCR3 = 0x80;
}

void Drv_SetRGB(uint8_t *data)
{
  uint32_t lrgba;//, grgba;
  uint8_t r,g,b,blink;
  
  r = data[1];
  g = data[2];
  b = data[3];
  blink = data[4];
  
  lrgba = r<<24 | g<<16 | b<<8 | blink;
  // grgba = gCBUP.rgb;
  //printf("Drv_SetRGBnew[%08x] old[%08x]\r\n",lrgba, grgba );
  //if(lrgba==grgba) return;
  led_rgb.blink=blink;
  led_rgb.r = r*4;
  led_rgb.g = g*4;
  led_rgb.b = b*4;
  TIM3->CCR1 = led_rgb.g;
  TIM3->CCR2 = led_rgb.r;
  TIM3->CCR3 = led_rgb.b;
  
  gCBUP.rgb= lrgba;
  
}

void Drv_RGB_Blink(uint8_t on)
{
  if(led_rgb.blink && on){
    TIM3->CCR1 = 0;
    TIM3->CCR2 = 0;
    TIM3->CCR3 = 0;  
  }
  else{
    TIM3->CCR1 = led_rgb.g;
    TIM3->CCR2 = led_rgb.r;
    TIM3->CCR3 = led_rgb.b;   
  }
}

void Drv_led_tick(void)//10ms loop
{
   uint8_t i;
   
   for(i=0;i<8;i++)
   {
    if(led_mono[i].tick>0 && led_mono[i].tick<0xff) led_mono[i].tick--;
   }

}

//timeout 0:  0xff:forever
void Drv_led_set(uint8_t led,  uint8_t timeout, uint8_t blink)
{
  led=led & 0x07;//led 0~7까지 8ea
  led_mono[led].tick=timeout;
  led_mono[led].blink=blink;
  ui.ledValue = (led_mono[led].tick) ? ui.ledValue|led_seg[led] : ui.ledValue&~led_seg[led];
  writeRegister(DIGIT6, ui.ledValue);
}

void Drv_led_release(void)
{
  uint8_t i;
  uint8_t tmp;
  
  Drv_led_tick();
  if(led_blink.blink){
    tmp=ui.ledValue;
    for(i=0;i<8;i++) if(led_mono[i].blink) tmp = tmp & ~led_seg[i];
    writeRegister(DIGIT6, tmp);
    return;
  }
  
  for(i=0;i<8;i++) if(led_mono[i].tick==0) ui.ledValue = ui.ledValue & ~led_seg[i];
  writeRegister(DIGIT6, ui.ledValue);
  
}


void Drv_display_fnd()
{
//printf("voltage [%d] current[%d]\r\n",ui.voltage, ui.current);	
#ifdef FUNCTION_MOTOR_TEST  
   // display_decfnd(LOCK_TRY_CNT, LOCK_ERR_CNT, 0);
    display_testfnd();
#else  
    display_decfnd(ui.voltage, ui.current, 1);
#endif    
}

void Drv_display_cid()
{
    uint8_t data[3];
    uint8_t fData[6];
    uint16_t value = cbd.id & 0xff;
    uint16_t fw = FW_VERSION;
   
    data[0]=value%10;
    data[1]=(value/10)%10;
    
    fData[0]=seg[data[0]];
    fData[1]=seg[data[1]];
#ifdef FUNCTION_MOTOR_TEST
    fw=0xAAAA;
    fData[2]=seg[fw & 0x0f]|SEG_DP;;
    fData[3]=seg[(fw>>4)& 0x0f];
    fData[4]=seg[(fw>>8)& 0x0f];
    fData[5]=seg[(fw>>12)& 0x0f];   
#else    
    fData[2]=seg[fw & 0x0f]|SEG_DP;;
    fData[3]=seg[(fw>>4)& 0x0f];
    fData[4]=seg[(fw>>8)& 0x0f];
    fData[5]=seg[(fw>>12)& 0x0f];
#endif   
    writeRegister(DIGIT0,fData[0]);
    writeRegister(DIGIT1,fData[1]);
    writeRegister(DIGIT2,fData[2]);
    
    writeRegister(DIGIT3,fData[3]);
    writeRegister(DIGIT4,fData[4]);
    writeRegister(DIGIT5,fData[5]);
 }


void display_decfnd(uint16_t lvalue, uint16_t rvalue, uint8_t dot)
{
    uint8_t data[6];
    uint8_t fData[6];
    uint8_t ldot = (dot) ? SEG_DP:0;
    
    lvalue %=1000;
    data[0]=lvalue%10;
    data[1]=(lvalue/10)%10;
    data[2]=(lvalue/100)%10;
  
    rvalue %=1000;
    data[3]=rvalue%10;
    data[4]=(rvalue/10)%10;
    data[5]=(rvalue/100)%10; 
    
    fData[0]=seg[data[0]];
    fData[1]=seg[data[1]]|ldot;
    fData[2]=seg[data[2]];
    fData[3]=seg[data[3]];
    fData[4]=seg[data[4]]|ldot;
    fData[5]=seg[data[5]];
    
    writeRegister(DIGIT3,fData[0]);
    writeRegister(DIGIT4,fData[1]);
    writeRegister(DIGIT5,fData[2]);
    
    writeRegister(DIGIT0,fData[3]);
    writeRegister(DIGIT1,fData[4]);
    writeRegister(DIGIT2,fData[5]);  
}

void display_testfnd()
{
    uint8_t data[6];
    uint8_t fData[6];
    
    LOCK_TRY_CNT %=1000;
    data[0]=LOCK_TRY_CNT%10;
    data[1]=(LOCK_TRY_CNT/10)%10;
    data[2]=(LOCK_TRY_CNT/100)%10;
  
    LOCK_ERR_CNT %=100;
    data[3]=LOCK_ERR_CNT%10;
    data[4]=(LOCK_ERR_CNT/10)%10;
    data[5]=LOCK_ERR_VAL&0x0f; 
    
    fData[0]=seg[data[0]];
    fData[1]=seg[data[1]];
    fData[2]=seg[data[2]];
    
    fData[3]=seg[data[3]];
    fData[4]=seg[data[4]];
    fData[5]=seg[data[5]];
    
    writeRegister(DIGIT3,fData[0]);
    writeRegister(DIGIT4,fData[1]);
    writeRegister(DIGIT5,fData[2]);
    
    writeRegister(DIGIT0,fData[3]);
    writeRegister(DIGIT1,fData[4]);
    writeRegister(DIGIT2,fData[5]);  
}
/*
void display_voltage(uint16_t value)
{
		uint8_t data[3];
		uint8_t fData[3];

		value %=1000;
		data[0]=value%10;
		data[1]=(value/10)%10;
		data[2]=(value/100)%10;

		fData[0]=seg[data[0]];
		fData[1]=seg[data[1]]|SEG_DP;
		fData[2]=seg[data[2]];
		writeRegister(DIGIT3,fData[0]);
		writeRegister(DIGIT4,fData[1]);
		writeRegister(DIGIT5,fData[2]);
}

void display_current(uint16_t value)
{
		uint8_t data[3];
		uint8_t fData[3];
		value %=1000;
		data[0]=value%10;
		data[1]=(value/10)%10;
		data[2]=(value/100)%10;

		fData[0]=seg[data[0]];
		fData[1]=seg[data[1]]|SEG_DP;
		fData[2]=seg[data[2]];
		writeRegister(DIGIT0,fData[0]);
		writeRegister(DIGIT1,fData[1]);
		writeRegister(DIGIT2,fData[2]);
}
*/
void displayTest(void)
{
		writeRegister(DISP_TEST, TEST);
}

void noDisplayTest(void)
{
		writeRegister(DISP_TEST, NORMAL);
}

void writeRegister(uint8_t addr, uint8_t value) {
		uint16_t data;
		uint8_t hiByte;
		uint8_t loByte;
		hiByte=addr&0x0f;
		loByte=value;
		data=BUILD_UINT16(loByte, hiByte);
		writeRegisters(data);
}

void writeRegisters(uint16_t value) {
	//Datasheet calls for 25ns between LOAD/#CS going low and the start of the
	//transfer, an Arduino running at 20MHz (4MHz faster than the Uno, mind you)
	//has a clock period of 50ns so no action needed.
	Hal_Spi2_Write(value);

}
