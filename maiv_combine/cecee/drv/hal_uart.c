/*
 * hal_uart.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "stm32f1xx_hal.h"
#include "usart.h"

#include "cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define HUART_TRACE(...)
#define HUART_TRACE(...)				DBG(__VA_ARGS__)

#define UART_U3_RX_IDLE	0
#define UART_U3_RX_DATA	1

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/
uint8_t bms_buf[128];
uint8_t bmsAddBuf[16];
uint8_t console_sbuf[32];
uint8_t console_buf[32];
uint8_t bms_data_cnt=0;

uint8_t BMS_RCV_FLAG=0;

uint8_t CONSOLE_RCV_FLAG=0;

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/
void Hal_Uart_DebugBufferInit(void);
//void Hal_Uart_ModemBufferInit(void);
//void Hal_Uart_GpsBufferInit(void);

/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Hal_Uart_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Uart_Init(void)
{
 	MX_USART_StartIT();
	//Hal_Uart_DebugInit();
  	memset(bms_buf,0,sizeof(bms_buf));
   	memset(console_buf,0,sizeof(console_buf));
 	return RET_OK;
}


/*****************************************************************************
*  Name:        Hal_Uart_SerialRxDataProcess()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned short Hal_Uart_SerialRxDataProcess(unsigned char rxdata)
{
 
  unsigned short ret = 0;
  static uint8_t console_in=0;
  static uint8_t console_data_cnt=0;
#if 0 
  
#ifdef CONSOL_DEBUG
  gConsole_serch_flag=1;
#else
  gConsole_serch_flag=0;
#endif
	printf("Hal_Uart_SerialRxDataProcess[%x]\n",rxdata);
	if(gConsole_serch_flag)
	{
		 if((rxdata=='!') && (console_in==0))
		{
			console_in=1;
			console_data_cnt=0;
			memset(console_sbuf,0,32);
		} 
		if(console_in) 
		{
			console_sbuf[console_data_cnt++]=rxdata;
			if(console_data_cnt>=8 && rxdata=='@')
			{
    			console_in=0;
    			console_data_cnt=0;
    			memcpy(console_buf,console_sbuf,16);
               CONSOLE_RCV_FLAG=1;
				//printf("Hal_Uart_SerialRxDataProcess[%x]\r\n",rxdata);
			}

		}
		if(console_data_cnt>16){
				console_in=0;
				console_data_cnt=0;
		}
		
  }
  else //debug mode
  {
    if(rxdata=='1'){
     printf("1-DISPLAY CBD SOL_B2_LOCK\r\n");
    // Drv_solenoid_set(0, _LOCK);
    }
    
     if(rxdata=='2'){
     printf("2-DISPLAY CBD SOL_B2_UNLOCK\r\n");
     //Drv_solenoid_set(0, _UNLOCK);
    }
  }
  
#endif 
	return ret;
}


int Hal_Uart_ConsoleTxData(unsigned char *txdata, unsigned short txlen)
{
	return MX_USART_Console_SendData(txdata, txlen);
}
