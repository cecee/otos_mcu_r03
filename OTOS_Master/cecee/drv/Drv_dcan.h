/*
Drv_dcan.h
*/
#ifndef DRV_DCAN_H_
#define DRV_DCAN_H_
#include <stdint.h>

typedef struct dbd_th11_tag_bit
{
	//signed integer
	int16_t temper;
	int16_t humidi;
}DHT11;

typedef struct dbd_relay_tag_bit
{
	uint8_t ac_light: 1;
	uint8_t ac_monitor: 1;	
	uint8_t ac_heaterA: 1;	
	uint8_t ac_heaterB: 1;	
}RELAY_BIT;

typedef struct dbd_fan_tag_bit
{
	uint8_t fan_f1: 1;
	uint8_t fan_f2: 1;	
	uint8_t fan_f3: 1;	
	uint8_t fan_f4: 1;	
  uint8_t fan_r1: 1;
	uint8_t fan_r2: 1;	
	uint8_t fan_r3: 1;	
	uint8_t fan_r4: 1;	
}FAN_BIT;


typedef struct dbd_ssensor_tag_bit
{
	uint16_t door: 1;	
	uint16_t slope1: 1;
	uint16_t slope2: 1;	
	uint16_t shock1: 1;	
	uint16_t shock2: 1;		
	uint16_t nfc_led: 1;	
	uint16_t mood: 1;
 	uint16_t dht1: 1;
	uint16_t dht2: 1;
	uint16_t dht3: 1; 
}SENSOR_BIT;

typedef struct dbd_pahalarm_tag_bit
{
	uint8_t ac_fail: 1;	
	uint8_t dc_fail: 1;
	uint8_t buckup_bat_fail: 1;	
	uint8_t ovc: 1;	
}PSU_ALARM_BIT;

typedef struct tagbdStatusport
{
	RELAY_BIT bRelay;
	FAN_BIT bFanCtrl;
  FAN_BIT bFanAlarm;
	SENSOR_BIT bSensor;
	PSU_ALARM_BIT bPsuAlarm;
	DHT11 dht[3];
	uint8_t buckRGB[6];
	int16_t tmp36_0;
	int16_t tmp36_1;
	uint16_t cb_exist;
	uint16_t cds;
	uint16_t dimmer;
	uint16_t main_V;
	uint16_t main_A;
	uint16_t main_W;
}D_STATUS;

//------------------------------
typedef struct tag_solBit{
	uint16_t typel_sol: 1;
	uint16_t ctrl_d1: 1;
	uint16_t ctrl_d2: 1;
	uint16_t ctrl_b1: 1;
	uint16_t ctrl_b2: 1;
	uint16_t ctrl_htr: 1;
	uint16_t ctrl_relay: 1;
	uint16_t ctrl_sd: 1;
	uint16_t ctrl_fan: 1;	
	uint16_t x9: 1;	
	uint16_t x10: 1;	
	uint16_t x11: 1;	
	uint16_t x12: 1;	
	uint16_t x13: 1;	
	uint16_t x14: 1;			
	uint16_t x15: 1;						
}BIT_CB_CTRL;

typedef union tag_solBit_union
{
	uint16_t value;
	BIT_CB_CTRL b;
}UNION_CB_CTRL;

/*
typedef struct tag_cbd_ctrll_cmd
{
	uint8_t cid;//1
	UNION_CB_CTRL CbCtrl;//2
	uint16_t MaxV;//2
	uint16_t MaxA;//2
	uint8_t x7;
}CBD_CTRL_CMD;

*/
//-----------------------------------------
typedef struct tag_bms_err_bit
{
	uint32_t uv_warning: 1;
	uint32_t uv_error: 1;
	uint32_t ov_warning: 1;		
	uint32_t ov_error: 1;
	uint32_t ot_warning: 1;
	uint32_t ot_error: 1;
	uint32_t ut_warning: 1;		
	uint32_t ut_error: 1;	
			
	uint32_t oc_warning_chg: 1;		
	uint32_t oc_error_chg: 1;     
	uint32_t oc_warning_dis: 1;
	uint32_t oc_error_dis: 1;	
	uint32_t comm_error: 1;	
	uint32_t by1bit5: 1;		
	uint32_t by1bit6: 1;		
	uint32_t by1bit7: 1;
		
	uint32_t pack_uv_warning: 1;		
	uint32_t pack_uv_protect: 1;     
	uint32_t pack_ov_warning: 1;
	uint32_t pack_ov_protect: 1;	
	uint32_t by2bit4: 1;	
	uint32_t by2bit5: 1;		
	uint32_t by2bit6: 1;		
	uint32_t by2bit7: 1;	
				
	uint32_t by3bit0: 1;		
	uint32_t by3bit1: 1;     
	uint32_t by3bit2: 1;
	uint32_t by3bit3: 1;	
	uint32_t by3bit4: 1;	
	uint32_t by3bit5: 1;		
	uint32_t warning: 1;		
	uint32_t error: 1;	
}BMS_ERR_BIT;

typedef union tag_bms_error_union
{
	uint8_t value[4];
	BMS_ERR_BIT b;
}BMS_ERR;

typedef union _CELL_BAL
{
	uint8_t value[4];
}CELL_BAL;

typedef union _USER_DATA
{
	uint8_t value[3];
}USER_DATA;

typedef union _PACK_COUNTER
{
	uint8_t value[6];
}PACK_COUNTER;

typedef struct tag_bms_value
{
  uint16_t cell_votage[20];//cell_votage[16];
  int16_t cell_temper[6];//"-"온도 있을수 있음
  uint16_t pack_voltage;
  int16_t pack_current; //"-"값 있음 전류방향에 따라서
  uint16_t pack_soc;//new: 2byte  old:1byte
  int16_t pack_temper;//(58)통신[60]MSB
  uint8_t pack_serial;//추가[62]
  uint8_t pack_parallel;//추가
  uint8_t pack_capacity;
  uint8_t pack_year;
  uint8_t pack_month;
  uint8_t pack_day;
  uint16_t pack_nation;
  uint16_t pack_serialNumber;//(68)통신[70-lsb][71-msb]
  uint16_t xxpack_cycle;//[72-lsb][73-msb]
  USER_DATA pack_userData;//[74-lsb][76-msb]	
  PACK_COUNTER pack_Counter;//[77]-[82]
  CELL_BAL pack_cellBancing;//83(MSB) .. 86(LSB)//추가	
  BMS_ERR pack_err; //[90-[93]
}BMS_VALUE;

typedef struct tag_status_bit
{
	//uint8_t pre_charging: 1;
	//uint8_t cc_charging: 1;
	//uint8_t cv_charging: 1;
    uint8_t charging: 1;
    uint8_t bms_err: 1;
    uint8_t chargingONOFF: 1;
    uint8_t batt_exist: 1;//x3
    uint8_t comm_fail: 1;
    uint8_t ac_fail: 1;
    uint8_t tmp_fail: 1;
    uint8_t fan_fail: 1;
    uint8_t dc_fail: 1;
    uint8_t load_balance: 1;	
    uint8_t relay: 1;	
    uint8_t dc_sd: 1;		
    uint8_t ctrl_fan: 1;//x12 //sps194 내부의 FAN		
    uint8_t pBattIn: 1;//x13		
    uint8_t pBattOut: 1;//x14		
    uint8_t bms_enable: 1;//x15			
}SPS_STATUS_BIT;

typedef struct tag_sps_value
{
	uint16_t voltage_setting;
	uint16_t voltage_outer;
	uint16_t voltage_sps;	
	uint16_t current_setting;
	uint16_t current_monitor;
	int16_t temperature_inner;//signed short
	int16_t rsev;//signed short
     uint8_t   cid;
     uint8_t   charging_seg;
     uint16_t  charging_time;
	uint16_t  cversion;
	SPS_STATUS_BIT b;
}SPS_VALUE;

typedef struct _BIT_MOTOR_STATUS{
	uint8_t D1_ctrl: 1;
	uint8_t D2_ctrl: 1;
	uint8_t B1_ctrl: 1;
	uint8_t B2_ctrl: 1;
 	uint8_t D1_err: 1;
	uint8_t D2_err: 1;
	uint8_t B1_err: 1;
	uint8_t B2_err: 1;
 	uint8_t D1_pos: 2;
	uint8_t D2_pos: 2;
	uint8_t B1_pos: 2;
	uint8_t B2_pos: 2;    
}BIT_MOTOR_STATUS;

typedef struct _CBD_DATA
{
	BMS_VALUE bms_value;
	SPS_VALUE chg_value;
     BIT_MOTOR_STATUS bLockStatus;
}CBD_DATA;



typedef struct tag_db_upload
{
	uint16_t sof;
  	uint16_t size;
	D_STATUS dStatus;
	CBD_DATA CbData;
	uint16_t crc;
	uint16_t eof;
}DB_UPLOAD;

//void Drv_CanSeqLoop(void);
//void Drv_CanRequest(uint8_t id, uint8_t read_type);
void Drv_SendmsgToCbd(uint32_t can_id, uint8_t *data);  
void Drv_CanRx(void);

#endif